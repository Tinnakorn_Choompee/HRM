<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Libraries\ColorLibrary\ColorLibrary;
use Libraries\ConductLibrary\ConductLibrary;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use App\Conduct;
use App\Employee;
use DB;
use Carbon\Carbon;

class ConductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $conduct = Conduct::all();
        $data     = ConductLibrary::Data();
        $conduct  = Conduct::with('employee')->orderBy('id','DESC')->paginate(12);
        $good     = Conduct::where('type',1)->count();
        $bad      = Conduct::where('type',2)->count();
        $employee = Employee::select(DB::raw("CONCAT(name,' ',surname) AS fullname, id"))->pluck('fullname','id'); 
        $month    = DateThaiLibrary::Month();
        $year     = $this->year();
        // $conduct = Conduct::with(['employee'])->get();
        // dd($users);
        // exit();
        // $article = \App\Models\Article::with(['user','category'])->first();
        // foreach ($conduct as $rs) :
        //     echo $rs->employee->code;
        //     echo $rs->detail;
        // endforeach;
        // exit();
        return view('conduct.index')->withConduct($conduct)->withData($data)->withGood($good)->withBad($bad)->withEmployee($employee)->withMonth($month)->withYear($year);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $position = Employee::orderBy('name')->pluck('id','name');
        $employee = Employee::select(DB::raw("CONCAT(name,' ',surname) AS fullname, id"))->pluck('fullname','id');
        return view('conduct.create')->withEmployee($employee);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['employee_id'=>'required', 'type'=>'required', 'title'=>'required', 'detail'=>'required']);
        $conduct = new Conduct;
        $conduct->employee_id = $request->employee_id;
        $conduct->type        = $request->type;
        $conduct->title       = $request->title;
        $conduct->detail      = $request->detail;
        $conduct->date        = now();
        $conduct->save();
        return redirect('/conduct')->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Conduct  $conduct
     * @return \Illuminate\Http\Response
     */
    public function show(Conduct $conduct)
    {
        $data    = ConductLibrary::Data();
        $conduct = Conduct::with('employee')->find($conduct->id);
        $conduct->employee->prename = $data['prename'][$conduct->employee->prename];
        $conduct->type = $data['type'][$conduct->type];
        $conduct->date = DateThaiLibrary::ThaiDate($conduct->date);
        return response($conduct);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Conduct  $conduct
     * @return \Illuminate\Http\Response
     */
    public function edit(Conduct $conduct)
    {
       $employee = Employee::select(DB::raw("CONCAT(name,' ',surname) AS fullname, id"))->pluck('fullname','id');
       return view('conduct.edit')->with(['conduct' => $conduct, 'employee' => $employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conduct  $conduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conduct $conduct)
    {
        $request->validate(['employee_id'=>'required','type'=>'required','title'=>'required','detail'=>'required','date'=>'required']);
        $conduct_e = $request->all();
        Conduct::updateOrCreate(['id' => $conduct->id] , $conduct_e);
        return redirect('/conduct')->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conduct  $conduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conduct $conduct)
    {
        // return response($conduct->id);
        // // exit();
        Conduct::destroy($conduct->id);
    }

    public function type($id)
    {
        $data    = ConductLibrary::Data();
        $conduct = Conduct::with('employee')->where('type',$id)->orderBy('id','DESC')->paginate(12);
        return view('conduct.type')->withConduct($conduct)->withData($data);
    }

    public function type_report($id)
    {
        $data    = ConductLibrary::Data();
        $conduct = Conduct::with('employee')->where('type',$id)->orderBy('id','DESC')->paginate(12);
        return view('conduct.type')->withConduct($conduct)->withData($data);
    }

    public function employee(Request $request)
    {
        return redirect()->route('conduct.profile', ['id' => $request->employee_id]);
    }

    public function profile($id)
    {
        $data     = ConductLibrary::Data();
        $conduct  = Conduct::with('employee')->where('employee_id', $id)->orderBy('id','DESC')->paginate(12);
        $employee = Employee::find($id);
        $good     = Conduct::where('employee_id', $id)->where('type',1)->count();
        $bad      = Conduct::where('employee_id', $id)->where('type',2)->count();
        return view('conduct.profile')->withConduct($conduct)->withData($data)->withEmployee($employee)->withGood($good)->withBad($bad);
    }

    public function profile_good($id)
    {
        $data     = ConductLibrary::Data();
        $type     = 1;
        $conduct  = Conduct::with('employee')->where('employee_id', $id)->where('type', $type)->orderBy('id','DESC')->paginate(12);
        $employee = Employee::find($id);
        return view('conduct.profile_type')->withConduct($conduct)->withData($data)->withEmployee($employee)->withType($type);
    }

    public function profile_bad($id)
    {
        $data     = ConductLibrary::Data();
        $type     = 2;
        $conduct  = Conduct::with('employee')->where('employee_id', $id)->where('type', $type)->orderBy('id','DESC')->paginate(12);
        $employee = Employee::find($id);
        return view('conduct.profile_type')->withConduct($conduct)->withData($data)->withEmployee($employee)->withType($type);
    }

    // public function employee_date(Request $request)
    // {
    //     return redirect()->route('conduct.profile', ['id' => $request->employee_id]);
    // }

    public function date(Request $request)
    {
        // $data     = ConductLibrary::Data();
        $conduct  = Conduct::with('employee')->whereBetween('date',  ['2017-12-17', '2017-12-19'])->orderBy('id','DESC')->paginate(12);
        // $employee = Employee::find($id);
        // $type     = $_REQUEST['TYPE'] == 1 ? "ดี" : "ไม่ดี";
        // return view('conduct.profile_type')->withConduct($conduct)->withData($data)->withType($type)->withEmployee($employee);
    }

    public function profile_date()
    {
        $data     = ConductLibrary::Data();
        $type     = 2;
        $conduct  = Conduct::with('employee')->where('employee_id', $id)->where('type', $type)->orderBy('id','DESC')->paginate(12);
        $employee = Employee::find($id);
        return view('conduct.profile_type')->withConduct($conduct)->withData($data)->withEmployee($employee)->withType($type);
    }

    public function report(Request $request)
    {
        $data     = ConductLibrary::Data();
        $color    = ColorLibrary::Color();
        $border   = ColorLibrary::Border();

        $total    = Conduct::count();
        $month    = DateThaiLibrary::Month();
        $employee = Employee::all(); 
        // $conduct  = Conduct::select('type', DB::raw('COUNT(*) as total'))->groupBy('type')->get();
        if($request->has('type')) :
            switch($request->type):
                case "0" : 
                    for ($i=1; $i <= 12; $i++) : 
                        $good[] = Conduct::where('type',1)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
                        $bad[]  = Conduct::where('type',2)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();  
                    endfor;
                    foreach($employee as $rs) :
                        $employee_good[$rs->id] = Conduct::where('type',1)->where('employee_id', $rs->id)->count();
                        $employee_bad[$rs->id]  = Conduct::where('type',2)->where('employee_id', $rs->id)->count();
                    endforeach;
                    $g = Conduct::where('type',1)->count();
                    $b = Conduct::where('type',2)->count();
                break;
                case "1" : 
                    for ($i=1; $i <= 12; $i++) : 
                        $good[] = Conduct::where('type',1)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
                        $bad[]  = 0;  
                    endfor;
                    foreach($employee as $rs) :
                        $employee_good[$rs->id] = Conduct::where('type',1)->where('employee_id', $rs->id)->count();
                        $employee_bad[$rs->id]  = 0;
                    endforeach;
                    $g = Conduct::where('type',1)->count();
                    $b = 0;
                break;
                case "2" :
                    for ($i=1; $i <= 12; $i++) : 
                        $good[] = 0;
                        $bad[]  = Conduct::where('type',2)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();  
                    endfor;
                    foreach($employee as $rs) :
                        $employee_good[$rs->id] = 0;
                        $employee_bad[$rs->id]  = Conduct::where('type',2)->where('employee_id', $rs->id)->count();
                    endforeach; 
                    $g = 0;
                    $b = Conduct::where('type',2)->count();
                break;
            endswitch;
        else :
            for ($i=1; $i <= 12; $i++) : 
                $good[] = Conduct::where('type',1)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
                $bad[]  = Conduct::where('type',2)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();  
            endfor;
            foreach($employee as $rs) :
                $employee_good[$rs->id] = Conduct::where('type',1)->where('employee_id', $rs->id)->count();
                $employee_bad[$rs->id]  = Conduct::where('type',2)->where('employee_id', $rs->id)->count();
            endforeach;
            $g = Conduct::where('type',1)->count();
            $b = Conduct::where('type',2)->count();
        endif;

        return view('conduct.report.index', compact(['color', 'border', 'g', 'b', 'total', 'month', 'good', 'bad', 'employee', 'data', 'employee_good', 'employee_bad']));
    }

    public function month($id)
    {
        $data     = ConductLibrary::Data();
        $conduct  = Conduct::with('employee')->whereMonth('date', $id)->whereYear('date', date('Y'))->orderBy('id','DESC')->paginate(12);
        $good     = Conduct::whereMonth('date', $id)->whereYear('date', date('Y'))->where('type',1)->count();
        $bad      = Conduct::whereMonth('date', $id)->whereYear('date', date('Y'))->where('type',2)->count();
        $id       = $id;
        $month    = DateThaiLibrary::Month(false);
        return view('conduct.profile')->withConduct($conduct)->withData($data)->withId($id)->withMonth($month)->withGood($good)->withBad($bad);
    }

    public function month_good($id)
    {
        $data     = ConductLibrary::Data();
        $type     = 1;
        $conduct  = Conduct::with('employee')->whereMonth('date', $id)->whereYear('date', date('Y'))->where('type', $type)->orderBy('id','DESC')->paginate(12);
        $id       = $id;
        $month    = DateThaiLibrary::Month(false);
        return view('conduct.profile_type')->withConduct($conduct)->withData($data)->withId($id)->withMonth($month)->withType($type);
    }

    public function month_bad($id)
    {
        $data     = ConductLibrary::Data();
        $type     = 2;
        $conduct  = Conduct::with('employee')->whereMonth('date', $id)->whereYear('date', date('Y'))->where('type', $type)->orderBy('id','DESC')->paginate(12);
        $id       = $id;
        $month    = DateThaiLibrary::Month(false);
        return view('conduct.profile_type')->withConduct($conduct)->withData($data)->withId($id)->withMonth($month)->withType($type);
    }

    public function profile_report($id)
    {
        $data     = ConductLibrary::Data();
        $conduct  = Conduct::with('employee')->where('employee_id', $id)->orderBy('id','DESC')->paginate(12);
        $employee = Employee::find($id);
        $good     = Conduct::where('employee_id', $id)->where('type',1)->count();
        $bad      = Conduct::where('employee_id', $id)->where('type',2)->count();
        return view('conduct.profile')->withConduct($conduct)->withData($data)->withEmployee($employee)->withGood($good)->withBad($bad);
    }

    public function year()
    {
      $d = Carbon::now()->year;
      $y = Carbon::now()->year;
      for($i = $d; $i <= $d+3; $i++) :
          $year[($y)+543] = ($y)+543;
          $y--;
      endfor;
      return $year;
    }

}
