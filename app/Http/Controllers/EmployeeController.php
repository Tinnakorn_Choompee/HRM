<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Position;
use App\Role;

// Ralationship
use App\Education;
use App\User;
use App\Work;
use App\Conduct;
use App\Leave;
use App\LeaveType;
use App\Holiday;
use App\Clocktime;

use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use Libraries\EmployeeLibrary\EmployeeLibrary;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Libraries\MenuLibrary\MenuLibrary;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Auth;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::all();
        $data     = EmployeeLibrary::Data();
        return view('employee.index', compact('data', 'employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data     = EmployeeLibrary::Data();
        $position = Position::orderBy('position_no')->pluck('position_name','position_name');
        $last_id  = Employee::max('code');
        $code     = sprintf('%04d',(++$last_id));
        return view('employee.create', compact('prename', 'data', 'code', 'position'));
        // return view('about', ['bell'=> $bell, 'naja' => $naja]);
        // return view('about')->withBell($bell)->withNaja($naja);
        // return view('about', compact('bell', 'naja', 'null'));
        // return view('about')->with(['' => $bell]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $employee = $request->all();
        if($request->hasFile('image'))
        {
            $file = $request->image;
            $name = str_random(5).$file->getClientOriginalName(); //time()
            // $extension = $file->extension();
            // $file->move("images/employee/" , $name);

            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(250, 250);
            $image_resize->save('images/employee/'.$name);
            $image_resize->save('images/user/'.$name);
            $employee['image'] = $name;
        }
        else
        {
            $employee['image'] = "user.png";
        }

        $email = $employee['email'];
        $education['graduate']  = $employee['graduate'];
        $education['study']     = isset($employee['study']) ? $employee['study'] : "-";
        $education['education'] = $employee['education'];
        $education['grade']     = isset($employee['grade']) ? $employee['grade'] : "-";

        unset($employee['email']);
        unset($employee['graduate']);
        unset($employee['study']);
        unset($employee['education']);
        unset($employee['grade']);

        $employee['clock_status'] = 0;

        $last = Employee::create($employee);

        $education['employee_id'] = $last->id;

        // Create Education
        Education::create($education);

        // Create user
        $user = new User();
        $user->name     = $last->name." ".$last->surname;
        $user->email    = $email;
        $user->username = $last->code;
        $user->password = bcrypt($last->code);
        $user->image    = $employee['image'];
        $user->save();
        $user->roles()->attach(Role::where('name', 'User')->first());

        return redirect('/employee')->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $employee['birthday'] = DateThaiLibrary::ThaiDate($employee['birthday']);
        $absence   = Work::where('employee_id', $employee->id)->where('status', 0)->count();
        $come      = Work::where('employee_id', $employee->id)->where('status', 1)->count();
        $late      = Work::where('employee_id', $employee->id)->where('status', 2)->count();
        $leave     = Work::where('employee_id', $employee->id)->where('status', 3)->count();
        $data      = EmployeeLibrary::Data();
        $education = Employee::find($employee->id)->Education;
        $user      = User::where('username',$employee->code)->first();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();
        return view('employee.show' ,compact('employee', 'data','education', 'absence', 'come', 'late', 'leave', 'user', 'leavetype', 'holiday'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $data      = EmployeeLibrary::Data();
        $position  = Position::orderBy('position_no')->pluck('position_name','position_name');
        // $education = Employee::find($employee->id)->Education;
        $employee  = Employee::where('employees.id', $employee->id)->leftJoin('education', 'education.employee_id', '=', 'employees.id')->first();
        $user      = User::where('username',$employee->code)->first();
        
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();

        // dd($employee);
        return view('employee.edit', compact('employee','data','position', 'user', 'leavetype', 'holiday'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {   
        $employee_e = $request->except('role');
        if($request->hasFile('image'))
        {
            // Old Image
            $request->edit_image == 'user.png' ? : File::delete('images/employee/'.$request->edit_image);
            $request->edit_image == 'user.png' ? : File::delete('images/user/'.$request->edit_image);

            $file = $request->image;
            $name = str_random(5).$file->getClientOriginalName(); //time()
            // $extension = $file->extension();
            // $file->move("images/employee/" , $name);
            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(250, 250);
            $image_resize->save('images/employee/'.$name);
            $image_resize->save('images/user/'.$name);
            $employee_e['image'] = $name;
            unset($employee_e['edit_image']);
        } else {
            $employee_e['image'] = $request->edit_image;
            unset($employee_e['edit_image']);
        }

        $email = $employee_e['email'];
        $education['graduate']  = $employee_e['graduate'];
        $education['study']     = isset($employee_e['study']) ? $employee_e['study'] : "-";
        $education['education'] = $employee_e['education'];
        $education['grade']     = isset($employee_e['grade']) ? $employee_e['grade'] : "-";

        unset($employee_e['user_id']);
        unset($employee_e['email']);
        unset($employee_e['graduate']);
        unset($employee_e['study']);
        unset($employee_e['education']);
        unset($employee_e['grade']);

        $last = Employee::updateOrCreate(['id' => $employee->id] , $employee_e);

        $user = User::where('username', $last->code)->first();
        $user->name  = $employee_e['name'];
        $user->email = $email;
        $user->image = $employee_e['image'];
        $user->save();

        Education::updateOrCreate(['employee_id' => $employee->id] , $education);
        return redirect('/employee')->with('update', 'Update Successfully!');
    }

    public function update_profile(EmployeeRequest $request, $id)
    {   
        $employee_e = $request->except('role');
        if($request->hasFile('image'))
        {
            // Old Image
            $request->edit_image == 'user.png' ? : File::delete('images/employee/'.$request->edit_image);
            $request->edit_image == 'user.png' ? : File::delete('images/user/'.$request->edit_image);

            $file = $request->image;
            $name = str_random(5).$file->getClientOriginalName(); //time()
            // $extension = $file->extension();
            // $file->move("images/employee/" , $name);
            $image_resize = Image::make($file->getRealPath());
            $image_resize->resize(250, 250);
            $image_resize->save('images/employee/'.$name);
            $image_resize->save('images/user/'.$name);
            $employee_e['image'] = $name;
            unset($employee_e['edit_image']);
        } else {
            $employee_e['image'] = $request->edit_image;
            unset($employee_e['edit_image']);
        }

        $email = $employee_e['email'];
        $education['graduate']  = $employee_e['graduate'];
        $education['study']     = isset($employee_e['study']) ? $employee_e['study'] : "-";
        $education['education'] = $employee_e['education'];
        $education['grade']     = isset($employee_e['grade']) ? $employee_e['grade'] : "-";

        unset($employee_e['user_id']);
        unset($employee_e['email']);
        unset($employee_e['graduate']);
        unset($employee_e['study']);
        unset($employee_e['education']);
        unset($employee_e['grade']);

        $last = Employee::updateOrCreate(['id' => $id] , $employee_e);

        $user = User::where('username', $last->code)->first();
        $user->name  = $employee_e['name'];
        $user->email = $email;
        $user->image = $employee_e['image'];
        $user->save();

        Education::updateOrCreate(['employee_id' => $id] , $education);
        return redirect('/employee/profile/'.$last->code)->with('update', 'Update Successfully!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        // dd($employee);
        $employee->image == 'user.png' ? : File::delete('images/employee/'.$employee->image);
        $employee->image == 'user.png' ? : File::delete('images/user/'.$employee->image);
        Employee::destroy($employee->id);

        Education::where('employee_id', $employee->id)->delete();
        User::where('username', $employee->code)->delete();
        Work::where('employee_id', $employee->id)->delete();
        Conduct::where('employee_id', $employee->id)->delete();
        Leave::where('employee_id', $employee->id)->delete();
        Clocktime::where('employee_id', $employee->id)->delete();

        return redirect('/employee')->with('delete', 'Delete Successfully !');
    }

    // public function destroy_all(Request $request)
    // {
    //     dd($request);
    // }

    public function profile($code)
    {
        if($code == Auth::user()->username):
            $employee  = Employee::where('code', $code)->first();
            $absence   = Work::where('employee_id', $employee->id)->where('status', 0)->count();
            $come      = Work::where('employee_id', $employee->id)->where('status', 1)->count();
            $late      = Work::where('employee_id', $employee->id)->where('status', 2)->count();
            $leave     = Work::where('employee_id', $employee->id)->where('status', 3)->count();
            $data      = EmployeeLibrary::Data();
            $education = Employee::find($employee->id)->Education;
            $user      = User::where('username',$employee->code)->first();
            $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
            $holiday   = Holiday::all();
            return view('employee.show' ,compact('employee', 'data','education', 'absence', 'come', 'late', 'leave', 'user', 'leavetype', 'holiday'));
        else:
            return view('404');
        endif;
    }

    public function edit_profile($code)
    {
        if($code == Auth::user()->username):
            $employee_e = Employee::where('code', $code)->first();

            $data       = EmployeeLibrary::Data();
            $position   = Position::orderBy('position_no')->pluck('position_name','position_name');
            $employee   = Employee::where('employees.id', $employee_e->id)->leftJoin('education', 'education.employee_id', '=', 'employees.id')->first();
            $user       = User::where('username',$employee_e->code)->first();

            foreach($position as $k => $v) :
                if($employee_e->position == $v) :
                    $position_e[$k] = $v;
                endif;
            endforeach;

            foreach($data['type'] as $k => $v) :
                if($employee_e->type == $k) :
                    $type[$k] = $v;
                endif;
            endforeach;

            $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
            $holiday   = Holiday::all();

            return view('employee.edit', compact('employee','data','position', 'user', 'leavetype', 'holiday', 'type', 'position_e'));

        else:
            return view('404');
        endif;
    }
}
