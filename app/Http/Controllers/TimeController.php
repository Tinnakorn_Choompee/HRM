<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Time;
use App\Work;
use App\Clocktime;
use Carbon\Carbon;
use Libraries\DatetimeLibrary\DatetimeLibrary;

class TimeController extends Controller
{
    public function index()
    {
        $start = Time::where('type', 'start')->first(); 
        $end   = Time::where('type', 'end')->first(); 
        $late  = Time::where('type', 'late')->first();
        $out   = Time::where('type', 'out')->first();   
        return view('time.index')->withStart($start)->withEnd($end)->withLate($late)->withOut($out);
    }

    public function update(Request $request)
    {
        $time = Time::where('type', $request->type)->first(); 
        $time->time = $request->time;
        $time->save();
        $request->type == "late" ? $this->checklate() : NULL;
        return redirect('time')->with('update', 'Update Successfully !');
    }

    public function checklate()
    {
        $clock = Clocktime::all();
        $late  = Time::where('type', 'late')->first();
        foreach($clock as $rs) :
            $c_in   = DatetimeLibrary::dateTime($rs->clock_in,  $late->time);
            $r_in   = Carbon::parse($rs->clock_in);
            $status = ($r_in > $c_in) ?  2 : 1; 
            $date   = date('Y-m-d', strtotime($rs->clock_in));
            $work   = Work::where('employee_id', $rs->employee_id)->whereDate('date', $date)->first();
            $work->status = $status;
            $work->save(); 
        endforeach; 
    }
}
