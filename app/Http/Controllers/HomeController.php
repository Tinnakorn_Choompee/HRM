<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Manager')) : 
        // สถานะการมาทำงาน   0 = ขาด  1 = มา   2 = สาย   3 = ลา 
        $absence = Work::where('status', 0)->count(); // ขาด
        $work    = Work::where('status', 1)->count(); // มา
        $late    = Work::where('status', 2)->count(); // สาย
        $leave   = Work::where('status', 3)->count(); // ลา
        return view('home', compact(['absence', 'work', 'late', 'leave']));
        else :
            return  redirect("employee/profile/".Auth::user()->username);
        endif;
    }
}
