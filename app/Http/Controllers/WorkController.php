<?php

namespace App\Http\Controllers;

use App\Work;
use App\Employee;
use App\Leavetype;
use App\Holiday;
use Illuminate\Http\Request;
use Libraries\ColorLibrary\ColorLibrary;
use Libraries\EmployeeLibrary\EmployeeLibrary;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use DB;
use Carbon\Carbon;
use Auth;

class WorkController extends Controller
{
    public function index()
    {
        $data    = EmployeeLibrary::Data();
        $come    = Work::where('status',1)->count();
        $late    = Work::where('status',2)->count();
        $absence = Work::where('status',0)->count();
        $leave   = Work::where('status',3)->count();
        // $work    = Work::with('employee')->orderBy('date','DESC')->get();
        
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();

        $work = Work::join('employees', 'works.employee_id', '=', 'employees.id')
            ->select('works.*', 'employees.prename', 'employees.name', 'employees.surname')
            ->orderBy('works.date','DESC')
            ->get();

        // $work = Work::select('date', DB::raw('count(*) as total'))->groupBy('date')->get();
        return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
    }

    public function employee($id)
    {
        if($id == Auth::user()->username):
            $employee  = Employee::where('code', $id)->first();
            $data      = EmployeeLibrary::Data();
            $work      = Work::with('employee')->where('employee_id',$employee->id)->orderBy('date','DESC')->get();
            $come      = Work::where('status',1)->where('employee_id',$employee->id)->count();
            $late      = Work::where('status',2)->where('employee_id',$employee->id)->count();
            $absence   = Work::where('status',0)->where('employee_id',$employee->id)->count();
            $leave     = Work::where('status',3)->where('employee_id',$employee->id)->count();

            $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
            $holiday   = Holiday::all();

            return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
        elseif (Auth::user()->hasRole('Manager') || Auth::user()->hasRole('Admin')) :
            $employee  = Employee::where('code', $id)->first();
            $data      = EmployeeLibrary::Data();
            $work      = Work::with('employee')->where('employee_id',$employee->id)->orderBy('date','DESC')->get();
            $come      = Work::where('status',1)->where('employee_id',$employee->id)->count();
            $late      = Work::where('status',2)->where('employee_id',$employee->id)->count();
            $absence   = Work::where('status',0)->where('employee_id',$employee->id)->count();
            $leave     = Work::where('status',3)->where('employee_id',$employee->id)->count();

            $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
            $holiday   = Holiday::all();

            return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
        else :
            return view('404');
        endif;
    }

    public function status($id)
    {
        $data    = EmployeeLibrary::Data();
        // $work    = Work::with('employee')->where('status',$id)->orderBy('date','DESC')->get();

        $work = Work::join('employees', 'works.employee_id', '=', 'employees.id')
        ->select('works.*', 'employees.prename', 'employees.name', 'employees.surname')
        ->where('works.status', $id)
        ->orderBy('works.date','DESC')
        ->get();

        $status  = TRUE;
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();
        switch ($id) :
            case 0: $absence = Work::where('status', $id)->count(); $come    = 0; $late = 0; $leave = 0; break;
            case 1: $come    = Work::where('status', $id)->count(); $absence = 0; $late = 0; $leave = 0; break;
            case 2: $late    = Work::where('status', $id)->count(); $absence = 0; $come = 0; $leave = 0; break;
            case 3: $leave   = Work::where('status', $id)->count(); $absence = 0; $late = 0; $come  = 0; break;
        endswitch;
        return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
    }

    public function year($id)
    {
        $data      = EmployeeLibrary::Data();
        $work      = Work::with('employee')->whereYear('date', $id - 543)->orderBy('date','DESC')->get();
        $come      = Work::where('status',1)->count();
        $late      = Work::where('status',2)->count();
        $absence   = Work::where('status',0)->count();
        $leave     = Work::where('status',3)->count();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();
        return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
    }

    public function month($id)
    {
        $data      = EmployeeLibrary::Data();
        $work      = Work::with('employee')->whereMonth('date',$id)->whereYear('date', Carbon::now()->year)->orderBy('date','DESC')->get();
        $come      = Work::where('status',1)->count();
        $late      = Work::where('status',2)->count();
        $absence   = Work::where('status',0)->count();
        $leave     = Work::where('status',3)->count();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();
        return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
    }

    public function date($id)
    {
        $data      = EmployeeLibrary::Data();
        $work      = Work::whereDay('date', $id)->whereMonth('date', Carbon::now()->month)->whereYear('date', Carbon::now()->year)->orderBy('date','DESC')->get();
        $come      = Work::where('status',1)->count();
        $late      = Work::where('status',2)->count();
        $absence   = Work::where('status',0)->count();
        $leave     = Work::where('status',3)->count();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();
        return view('work.index')->withWork($work)->withData($data)->withCome($come)->withLate($late)->withAbsence($absence)->withLeave($leave)->withLeavetype($leavetype)->withHoliday($holiday);
    }

    public function report()
    {
        $month = Carbon::now()->daysInMonth;
        // $day   = Carbon::create(2018, 1 ,16)->day; // ตัวทดสอบ
        $day   = Carbon::now()->day;
        $week  = Carbon::now()->day(15)->day;

        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        $holiday   = Holiday::all();

        if($day <= $week):
            for ($i=1; $i <= 15; $i++) :
                for($a=0; $a <= 3; $a++) :
                    $dayofweek[$i][$a] = Work::where('status',$a)->whereDay('date', $i)->whereMonth('date', Carbon::now()->month)->whereYear('date', Carbon::now()->year)->count();
                endfor;
            endfor;
            $daybetween = " 1 - 15 ";
        else:
            for ($i=16; $i <= $month; $i++) :
                for($a=0; $a <= 3; $a++) :
                    $dayofweek[$i][$a] = Work::where('status',$a)->whereDay('date', $i)->whereMonth('date', Carbon::now()->month)->whereYear('date', Carbon::now()->year)->count();
                endfor;
            endfor;
            $daybetween = " 16 - ".$month;
        endif;

        foreach($dayofweek as $k => $rs) :
            $absence_max[] = $rs[0];
            $come_max[]    = $rs[1];
            $late_max[]    = $rs[2];
            $leave_max[]   = $rs[3];
        endforeach;

        $name_month = DateThaiLibrary::GetMonth(Carbon::now());
        $max = max($absence_max)+max($come_max)+max($late_max)+max($leave_max) + 5;

        // dd($dayofweek);
        $label    = ['ขาด', 'มา', 'สาย', 'ลา'];
        $total    = Work::count();
        $month    = DateThaiLibrary::Month();
        $data     = EmployeeLibrary::Data();
        $employee = Employee::all();
        $work     = Work::select('status', DB::raw('COUNT(*) as total'))->groupBy('status')->get();

        for ($i=1; $i <= 12; $i++) :
            $absence[] = Work::where('status',0)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
            $come[]    = Work::where('status',1)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
            $late[]    = Work::where('status',2)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
            $leave[]   = Work::where('status',3)->whereMonth('date', $i)->whereYear('date', date('Y'))->count();
        endfor;

        $year = Carbon::now()->year;

        for($i = $year - 4; $i <= $year; $i++) :
            $yeary[]     = $i+543;
            $absence_y[$i] = Work::where('status',0)->whereYear('date', $i)->count();
            $come_y[$i]    = Work::where('status',1)->whereYear('date', $i)->count();
            $late_y[$i]    = Work::where('status',2)->whereYear('date', $i)->count();
            $leave_y[$i]   = Work::where('status',3)->whereYear('date', $i)->count();
        endfor;

        foreach($employee as $rs) :
            $employee_absence[$rs->id] = Work::where('status',0)->where('employee_id', $rs->id)->count();
            $employee_come[$rs->id]    = Work::where('status',1)->where('employee_id', $rs->id)->count();
            $employee_late[$rs->id]    = Work::where('status',2)->where('employee_id', $rs->id)->count();
            $employee_leave[$rs->id]   = Work::where('status',3)->where('employee_id', $rs->id)->count();
        endforeach;

        $color    = ColorLibrary::Color();
        $border   = ColorLibrary::Border();

        return view('work.report')
            ->withColor($color)
            ->withBorder($border)
            ->withLabel($label)
            ->withWork($work)
            ->withTotal($total)
            ->withMonth($month)
            ->withYear($year)
            ->withYeary($yeary)
            ->withAbsence($absence)
            ->withCome($come)
            ->withLate($late)
            ->withLeave($leave)
            ->withAbsencey($absence_y)
            ->withComey($come_y)
            ->withLatey($late_y)
            ->withLeavey($leave_y)
            ->withData($data)
            ->withEmployee($employee)
            ->withDayofweek($dayofweek)
            ->withMax($max)
            ->withDaybetween($daybetween)
            ->withNamemonth($name_month)
            ->withLeavetype($leavetype)
            ->withHoliday($holiday)
            ->withEmpabsence($employee_absence)
            ->withEmpcome($employee_come)
            ->withEmplate($employee_late)
            ->withEmpleave($employee_leave);
    }
}
