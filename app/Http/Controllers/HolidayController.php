<?php

namespace App\Http\Controllers;

use App\Holiday;
use Illuminate\Http\Request;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Carbon\Carbon;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $holiday = Holiday::orderBy('date')->get();
        foreach($holiday as $rs):
            if (date('Y') > date('Y', strtotime($rs->date))):
                $year = Holiday::find($rs->id);
                $year->date = Carbon::parse($year->date)->year(date('Y'));
                $year->save();
                return redirect()->back();
            endif;
        endforeach;
        return view('holiday.index')->withHoliday($holiday);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = ['unique' => 'วันหยุดซ้ำกัน'];
        $request->validate(['date'=>'required|date|unique:holidays,date,'.$request->date, 'name'=>'required'], $messages);
        $holiday = $request->all();
        Holiday::create($holiday);
        return redirect('/holiday')->with('success', 'Save Successfully!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $messages = ['unique' => 'มีวันหยุดแล้ว'];
        $request->validate(['date'=>'required|date|unique:holidays,date,'.$request->date, 'name'=>'required'], $messages);
        $holiday = $request->all();
        Holiday::updateOrCreate(['id' => $request->id] , $holiday);
        return redirect('/holiday')->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Holiday::destroy($request->id);
    }
}
