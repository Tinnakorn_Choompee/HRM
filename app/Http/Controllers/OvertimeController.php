<?php

namespace App\Http\Controllers;

use App\Overtime;
use App\Overtime_detail;
use App\Holiday;
use App\Employee;
use Carbon;
use Libraries\EmployeeLibrary\EmployeeLibrary;
use Illuminate\Http\Request;

class OvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $overtime = Overtime::orderBy('date', 'DESC')->get();
        return view('overtime.index')->withOvertime($overtime);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date     = Carbon::now()->toDateString();
        $holiday  = Holiday::all();
        $employee = Employee::all();
        $data     = EmployeeLibrary::Data();
        return view('overtime.create')->withHoliday($holiday)->withEmployee($employee)->withData($data)->withDate($date);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = ['date.unique' => 'วันที่ทำงานล่วงเวลาซ้ำ'];        
        $request->validate(['date' => 'required|unique:overtimes'] , $messages);

        $overtime = $request->except(['id']);
        $last     = Overtime::create($overtime);
        foreach($request->id as $id):
            $detail = new Overtime_detail;
            $detail->overtimes_id = $last->id;
            $detail->employee_id  = $id;
            $detail->save();
        endforeach;
        return redirect('/overtime')->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function show(Overtime $overtime)
    {
       $overtime = Overtime::with('overtime_detail')->find($overtime->id);
       $employee = Employee::all();
       $data     = EmployeeLibrary::Data();
       foreach($overtime->overtime_detail as $rs):
        $detail[] = $rs->employee_id; 
       endforeach;
       return view('overtime.show')->withOvertime($overtime)->withDetail($detail)->withData($data)->withEmployee($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function edit(Overtime $overtime)
    {
        $date     = Carbon::now()->toDateString();
        $holiday  = Holiday::all();
        $employee = Employee::all();
        $data     = EmployeeLibrary::Data();
        $overtime = Overtime::with('overtime_detail')->find($overtime->id);
        foreach($overtime->overtime_detail as $rs):
             $detail[] = $rs->employee_id; 
        endforeach;
        return view('overtime.edit')->with([ 
            'date'     => $date ,
            'employee' => $employee, 
            'data'     => $data, 
            'overtime' => $overtime, 
            'holiday'  => $holiday,
            'detail'   => $detail
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Overtime $overtime)
    {
        $messages = ['date.unique' => 'วันที่ทำงานล่วงเวลาซ้ำ'];        
        $request->validate(['date' => 'required|unique:overtimes,date,'.$overtime->id] , $messages);
        $over    = $request->except(['id']);
        Overtime::updateOrCreate(['id' => $overtime->id] , $over);
        Overtime_detail::where(['overtimes_id' => $overtime->id])->delete();
        foreach($request->id as $id):
            Overtime_detail::create(['overtimes_id' => $overtime->id , 'employee_id' => $id]);
        endforeach;
        return redirect('/overtime')->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Overtime $overtime)
    {
        Overtime::destroy($overtime->id);
        Overtime_detail::where(['overtimes_id' => $overtime->id])->delete();
    }
}
