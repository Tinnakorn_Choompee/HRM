<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    public function index()
    {
        return view('position.index')
               ->withPosition(Position::orderBy('position_no')->get())
               ->withMax(Position::max('position_no'))
               ->withMin(Position::min('position_no'));
    }

    public function store(Request $request)
    {
        $last_id = Position::max('position_no');
        $position['position_no']   = ++$last_id;
        $position['position_name'] = $request->position;
        // $position = new Flight;
        // $position->position = $request->position;
        // $position-> 
        // $position->save();
        Position::create($position);
    }

    public function update(Request $request)
    {
        $position['position_no']   = $request->no;
        $position['position_name'] = $request->position;
        Position::updateOrCreate(['id' => $request->id] , $position);
        // return response($request->position);
    }

    public function destroy(Request $request)
    {
        Position::destroy($request->id);
    }

    public function up(Request $request)
    {   
        $sequence_a = Position::where('position_no', $request->no)->first(); 
        $sequence_a->position_no = ++$sequence_a->position_no;

        $sequence_b = Position::where('position_no', ++$request->no)->first(); 
        $sequence_b->position_no = --$sequence_b->position_no;

        $sequence_b->save();

        $sequence_a->save();

    }

    public function down(Request $request)
    {
        $sequence_a = Position::where('position_no', $request->no)->first(); 
        $sequence_a->position_no = --$sequence_a->position_no;

        $sequence_b = Position::where('position_no', --$request->no)->first(); 
        $sequence_b->position_no = ++$sequence_b->position_no;

        $sequence_b->save();

        $sequence_a->save();

        return response($sequence_b->position_no);
    }
}
