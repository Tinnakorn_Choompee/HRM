<?php

namespace App\Http\Controllers;

use App\Salary;
use App\Employee;
use App\Work;
use App\Overtime;
use App\Overtime_detail;
use Illuminate\Http\Request;
use App\Http\Requests\SalaryRequest;
use Libraries\EmployeeLibrary\EmployeeLibrary;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Carbon\Carbon;
use PDF;

class SalaryController extends Controller
{
    public function index(Request $request)
    {
      if ($request->has('year')) {
          $currentyear = $request->year;
      } else {
          $currentyear = Carbon::now()->year + 543;
      }

      $data     = EmployeeLibrary::Data();
      $employee = Employee::orderBy('type')->get();
      $month    = DateThaiLibrary::Month(FALSE);
      $year     = $this->year();
      $now      = Carbon::now();

      // $day   = Carbon::now()->day;
      // $week  = Carbon::now()->day(15)->day;
      // $num_m = Carbon::now()->daysInMonth;

      // $week[] = Carbon::parse($rs->date)->year;
      // $week[] = Carbon::createFromDate($currentyear - 543, 1, 1)->year;

      $salary   = Salary::all();
      foreach ($month as $k => $m) :
          foreach ($employee as $em) :
            $week[$em->id][$k][1] = 0;
            $week[$em->id][$k][2] = 0;
            foreach ($salary as $rs) :
              if($em->id == $rs->employee_id):
                  if(Carbon::parse($rs->date)->year == Carbon::createFromDate($currentyear - 543)->year):
                      if(Carbon::parse($rs->date)->month == Carbon::createFromDate($currentyear - 543, $k)->month):
                           if($rs->week == 1):
                             $week[$em->id][$k][1] = $rs->id;
                           else:
                             $week[$em->id][$k][2] = $rs->id;
                           endif;
                      endif;
                  endif;
              endif;
            endforeach;
          endforeach;
      endforeach;

      // dd($week);

      for($i = 1;$i <= 12;$i++) :
        $num_m = Carbon::createFromDate(($currentyear - 543), $i)->daysInMonth;
        $daybetween[$i][1] = " 1 - 15 ";
        $daybetween[$i][2] = " 16 - ".$num_m;
      endfor;

      return view('salary.index')
              ->withYear($year)
              ->withCurrentyear($currentyear)
              ->withMonth($month)
              ->withEmployee($employee)
              ->withData($data)
              ->withNow($now)
              ->withDaybetween($daybetween)
              ->withSalary($salary)
              ->withWeek($week);
    }

    public function create(Request $request)
    {
      $employee = Employee::find($request->id);
      $week     = $request->week;
      $date     = $request->date; 

      $day   = Carbon::parse($date)->day;
      $week  = Carbon::parse($date)->day(15)->day;
      $num_m = Carbon::parse($date)->daysInMonth;

      $month = DateThaiLibrary::GetMonth($date);

      if($day < $week) :
        $daybetween = "งวดวันที่ ".$day." - ".$week." เดือน ".$month;
      else :
        $daybetween = "งวดวันที่ ".$week." - ".$num_m." เดือน ".$month;
      endif;

      $d = Carbon::parse($date)->day;
      $m = Carbon::parse($date)->month;
      $y = Carbon::parse($date)->year;

      if($day < $week) :
        $start = Carbon::createFromDate($y, $m, $d - 1);
        $end   = Carbon::createFromDate($y, $m, $week);
      else :
        $start = Carbon::createFromDate($y, $m, $d);
        $end   = Carbon::createFromDate($y, $m, $num_m);
      endif;

      $overtime = Overtime::whereBetween('date', [ $start , $end ])
      ->where('overtime_details.employee_id', $request->id)
      ->join('overtime_details', 'overtimes.id', '=', 'overtime_details.overtimes_id')
      ->get();

      foreach($overtime as $k => $rs):
        $time[] = $rs->time;
      endforeach;
      $come = Work::where('status',1)->whereBetween('date', [ $start , $end ])->where('employee_id',$request->id)->count();
      switch($employee->type) : 
        case 1 : 
          $salary    = $employee->salary; 
          if(empty($time)) {
            $ot_amount = 0;
          } else {
            $ot_time   = $this->Time($time);
            $time      = explode(":", $ot_time);
            $sum_time  = number_format( $time[0] + $time[1]/60 , 2, '.', '');
            $ot_amount = (($salary / 15) / 8 * 1.5) * $sum_time;
          }
        break;
        case 2 : 
          $salary    = $employee->salary * $come; 
          if(empty($time)) {
            $ot_amount = 0;
          } else {
            $ot_time   = empty($time) ? NULL : $this->Time($time);
            $time      = explode(":", $ot_time);
            $sum_time  = number_format( $time[0] + $time[1]/60 , 2, '.', '');
            $ot_amount = number_format( ($salary / 8 * 1.5) * $sum_time, 2, '.', '');
          }
        break;
      endswitch;

      switch($employee->type):
        case 1 : $insurance = ($employee->salary * 5) / 100; break;
        case 2 : $insurance = (($employee->salary * $come) * 5) / 100; break;
      endswitch;

      $data = EmployeeLibrary::data();
      $name = $data['prename'][$employee->prename]." ".$employee->name." ".$employee->surname;

      return view('salary.create')
      ->withDaybetween($daybetween)
      ->withName($name)
      ->withEmployee($employee)
      ->withOt($ot_amount)
      ->withInsurance($insurance)
      ->withDate($date)
      ->withWeek($week);
    }

    public function store(Request $request)
    {
      $salary = new Salary;
      $salary->employee_id  = $request->id;
      $salary->salary       = $request->salary;
      $salary->overtime     = $request->overtime;
      $salary->ar           = $request->ar;
      $salary->commission   = $request->commission;
      $salary->insurance    = $request->insurance;
      $salary->other        = $request->other;
      $salary->amount_other = $request->amount_other;
      $salary->week         = $request->week;
      $salary->date         = $request->date; 
      $salary->save();
      $m = Carbon::parse($salary->date)->month;
      return redirect('/salary')->with('success', $m);
    }

    public function edit($id)
    {
      $salary   = Salary::find($id);
      $data     = EmployeeLibrary::data();
      $employee = Employee::find($salary->employee_id);

      $name  = $data['prename'][$employee->prename]." ".$employee->name." ".$employee->surname;
      $day   = Carbon::parse($salary->date)->day;
      $week  = Carbon::parse($salary->date)->day(15)->day;
      $num_m = Carbon::parse($salary->date)->daysInMonth;

      $d = Carbon::parse($salary->date)->day;
      $m = Carbon::parse($salary->date)->month;
      $y = Carbon::parse($salary->date)->year;

      $month = DateThaiLibrary::GetMonth($salary->date);

      if($day < $week) :
        $daybetween = "งวดวันที่ ".$day." - ".$week." เดือน ".$month;
      else :
        $daybetween = "งวดวันที่ ".$week." - ".$num_m." เดือน ".$month;
      endif;

      switch($employee->type):
        case 1 : $salary->insurance = ($employee->salary * 5) / 100; break;
        case 2 : $salary->insurance = ($employee->salary * 5) / 100; break;
      endswitch;

      return view('salary.edit')->withSalary($salary)->withDaybetween($daybetween)->withMonth($month)->withName($name)->withEmployee($employee);
    }

    public function update(Request $request, $id)
    {
      $salary = $request->all();
      $last = Salary::updateOrCreate(['id' => $id] , $salary);
      $m = Carbon::parse($last->date)->month;
      return redirect('/salary')->with('success', $m);
    }

    public function show(Request $request)
    {
       
    }
    
    public function year()
    {
      $d = Carbon::now()->year;
      $y = Carbon::now()->year;
      for($i = $d; $i <= $d+3; $i++) :
          $year[($y)+543] = ($y)+543;
          $y--;
      endfor;
      return $year;
    }

    public function slip($id)
    {
      $salary   = Salary::with('employee')->find($id);
      $employee = Employee::find($salary->employee_id); 
      $data    = EmployeeLibrary::Data();
      $company = "ร้านรุ่งเรืองทรัพย์";
      $title   = "Pay Silp";
      // SELECT * FROM works WHERE status = 1 AND employee_id = 79 AND date BETWEEN "2018-02-01" AND "2018-02-15"

      $day   = Carbon::parse($salary->date)->day;
      $week  = Carbon::parse($salary->date)->day(15)->day;
      $num_m = Carbon::parse($salary->date)->daysInMonth;

      $d = Carbon::parse($salary->date)->day;
      $m = Carbon::parse($salary->date)->month;
      $y = Carbon::parse($salary->date)->year;

      if($day < $week) :
        $start = Carbon::createFromDate($y, $m, $d - 1);
        $end   = Carbon::createFromDate($y, $m, $week);
      else :
        $start = Carbon::createFromDate($y, $m, $d);
        $end   = Carbon::createFromDate($y, $m, $num_m);
      endif;

      $come    = Work::where('status',1)->whereBetween('date', [ $start , $end ])->where('employee_id',$salary->employee_id)->count();
      $late    = Work::where('status',2)->whereBetween('date', [ $start , $end ])->where('employee_id',$salary->employee_id)->count();
      $absence = Work::where('status',0)->whereBetween('date', [ $start , $end ])->where('employee_id',$salary->employee_id)->count();
      $leave   = Work::where('status',3)->whereBetween('date', [ $start , $end ])->where('employee_id',$salary->employee_id)->count();

      $overtime = Overtime::whereBetween('date', [ $start , $end ])
      ->where('overtime_details.employee_id', $salary->employee_id)
      ->join('overtime_details', 'overtimes.id', '=', 'overtime_details.overtimes_id')
      ->get();

      foreach($overtime as $k => $rs):
          $time[] = $rs->time;
      endforeach;
      // OT Time
      switch($salary->employee->type) : 

        case 1 : 
 
          $ot_time   = empty($time) ? NULL : $this->Time($time);
          if($ot_time) : 
            $time      = explode(":", $ot_time);
            $sum_time  = number_format( $time[0] + $time[1]/60 , 2, '.', '');
            $ot_amount = (($salary->salary / 15) / 8 * 1.5) * $sum_time;
          else: 
            $ot_amount = 0;
          endif;
          
        break;

        case 2 : 
          $salary->salary = $salary->salary * $come; 
          $ot_time   = empty($time) ? NULL : $this->Time($time);
          if($ot_time) : 
            $time      = explode(":", $ot_time);
            $sum_time  = number_format( $time[0] + $time[1]/60 , 2, '.', '');
            $ot_amount = number_format( ($employee->salary/ 8 * 1.5) * $sum_time, 2, '.', '');
          else: 
            $ot_amount = 0;
          endif;
        break;

      endswitch;

      $sum       = number_format($salary->salary + $ot_amount + $salary->ar + $salary->commission, 2, '.', '');
      $deduction = number_format((($salary->salary * 5) /100) + $salary->amount_other, 2, '.', '');
      $total     = number_format($sum - $deduction, 2, '.', '');
      $thai      = $this->Convert($total);
      
      $pdf  = PDF::loadView('salary.slip', 
      compact(  'company', 
                'title', 
                'data', 
                'salary', 
                'ot_time',
                'ot_amount',
                'come', 
                'late', 
                'absence', 
                'leave',
                'sum',
                'deduction',
                'total',
                'thai'
              ));

      // $pdf = PDF::loadView('saraly.silp');
      // return $pdf->download('invoice.pdf');
      // return $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->setPaper('a4', 'landscape')->stream('silp.pdf');
      return $pdf->stream('slip.pdf');
    }

    public function Convert($amount_number)
    {
        $amount_number = number_format($amount_number, 2, ".","");
        $pt = strpos($amount_number , ".");
        $number = $fraction = "";
        if ($pt === false) 
            $number = $amount_number;
        else
        {
            $number   = substr($amount_number, 0, $pt);
            $fraction = substr($amount_number, $pt + 1);
        }
        
        $ret = "";
        $baht = $this->ReadNumber ($number);
        if ($baht != "")
            $ret .= $baht . "บาท";
        
        $satang = $this->ReadNumber($fraction);
        if ($satang != "")
            $ret .=  $satang . "สตางค์";
        else 
            $ret .= "ถ้วน";
        return $ret;
    }

    public function ReadNumber($number)
    {
        $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
        $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
        $number = $number + 0;
        $ret = "";
        if ($number == 0) return $ret;
        if ($number > 1000000)
        {
            $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
            $number = intval(fmod($number, 1000000));
        }
        
        $divider = 100000;
        $pos = 0;
        while($number > 0)
        {
            $d = intval($number / $divider);
            $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
                ((($divider == 10) && ($d == 1)) ? "" :
                ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
            $ret .= ($d ? $position_call[$pos] : "");
            $number = $number % $divider;
            $divider = $divider / 10;
            $pos++;
        }
        return $ret;
    }

    public function Time($times) {
      $minutes=null;
      foreach ($times as $time) {
          list($hour, $minute) = explode(':', $time);
          $minutes += $hour * 60;
          $minutes += $minute;
      }
      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;
      return sprintf('%02d:%02d', $hours, $minutes);
    }
}
