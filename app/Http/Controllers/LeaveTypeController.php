<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveType;

class LeaveTypeController extends Controller
{
    public function index()
    {
        return view('leavetype.index')
               ->withPosition(LeaveType::orderBy('type_no')->get())
               ->withMax(LeaveType::max('type_no'))
               ->withMin(LeaveType::min('type_no'));
    }

    public function store(Request $request)
    {
        $last_id = LeaveType::max('type_no');
        $leave_type['type_no']   = ++$last_id;
        $leave_type['type_name'] = $request->type_name;
        // $position = new Flight;
        // $position->position = $request->position;
        // $position-> 
        // $position->save();
        LeaveType::create($leave_type);
    }

    public function update(Request $request)
    {
        $leave_type['type_no']   = $request->no;
        $leave_type['type_name'] = $request->type_name;
        LeaveType::updateOrCreate(['id' => $request->id] , $leave_type);
        // return response($request->position);
    }

    public function destroy(Request $request)
    {
        LeaveType::destroy($request->id);
    }

    public function up(Request $request)
    {   
        $sequence_a = LeaveType::where('type_no', $request->no)->first(); 
        $sequence_a->type_no = ++$sequence_a->type_no;

        $sequence_b = LeaveType::where('type_no', ++$request->no)->first(); 
        $sequence_b->type_no = --$sequence_b->type_no;

        $sequence_b->save();

        $sequence_a->save();

    }

    public function down(Request $request)
    {
        $sequence_a = LeaveType::where('type_no', $request->no)->first(); 
        $sequence_a->type_no = --$sequence_a->type_no;

        $sequence_b = LeaveType::where('type_no', --$request->no)->first(); 
        $sequence_b->type_no = ++$sequence_b->type_no;

        $sequence_b->save();

        $sequence_a->save();

    }
}
