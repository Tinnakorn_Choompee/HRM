<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Libraries\EmployeeLibrary\EmployeeLibrary;
use Libraries\DatetimeLibrary\DatetimeLibrary;
use App\Clocktime;
use App\Employee;
use App\Work;
use App\Holiday;
use App\Time;
use App\LeaveType;
use Carbon\Carbon;
use Auth;
// use Cookie;

class ClocktimeController extends Controller
{
    // Clock Time // ส่วนของการ ลงเวลางาน
    public function index()
    {
        $datetime = DateThaiLibrary::ThaiDate(now(), FALSE, TRUE);
        $data     = EmployeeLibrary::Data();

        $employee_month = Employee::where('type', 1)->get();

        $employee_day   = Employee::where('type', 2)->get();

        $employee = Employee::all();

        $start = Time::where('type', 'start')->first();
        $end   = Time::where('type', 'end')->first();
        $out   = Time::where('type', 'out')->first();

        $clock_in  = Clocktime::orderBY('clock_in','DESC')->whereDate('clock_in', now()->toDateString())->get();
        $clock_out = Carbon::createFromTime($end->hour, $end->minute, 0); // ลิมิตเวลาที่สามารถ ออกงานได้
        // $clock_out = Carbon::createFromTime(00, 01, 00); // ลิมิตเวลาที่สามารถ ออกงานได้

        $time = Carbon::now();
        // $time = Carbon::create(2017, 11, 26, 7, 45, 00);
        $set  = Carbon::createFromTime($start->hour, $start->minute, 00); // วันนี้  เวลาที่เซ็ตไว้

        $sample      = Carbon::create(2017, 11, 26, 7, 45, 00); // สมมุติเป็นเวลาเข้างาน // ตัวอย่าง 00.00.00 - 17.00.00
        $sample_set  = Carbon::create(2017, 11, 26, 10, 00, 00); // สมมุติเป็นเวลารีเซ็ต 8 หรือ 9 สายได้กี่นาที ว่าไปตามนั้น

        // เวลาทำงานเริ่มที่ 08.00 น
        // มาหลัง เวลาที่ เซ็ต เช่น เซ็ต 8 โมง หลัง 8 โมง มาล๊อกอิน ปุ่ม จะไม่ขึ้น
        // หลักการรอวันถัดไป แล้วรีเซ็ตใหม่ // ซับซ้อนนิดหน่อย แต่สามารถใช้งานได้ดี

        // เที่ยงคืนปุ๊บ ข้อความ บันทึก เวลาจะหายไป และเริ่มบันทึก ของอันใหม่

        // ตัดเวลาพักเที่ยง 1 ชั่วโมง

        $test  = Carbon::createFromTime(14, 00, 00); // วันนี้  เวลาที่เซ็ตไว้
        $check_work  = Work::whereDate('date', date('Y-m-d'))->first();
        // Carbon::now()->dayOfWeek;
        // สถานะการมาทำงาน   0 = ขาด  1 = มา   2 = สาย   3 = ลา

        // ประเภทการลา
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        // วันหยุด
        $holiday  = Holiday::all();
        $check_holiday  = Holiday::whereDate('date', now()->toDateString())->first();
        // dd($holiday);
        if (!(Carbon::now()->dayOfWeek === Carbon::SUNDAY) && (empty($check_holiday->name)))
        {
            foreach(Employee::all() as $em) {

                if(empty($check_work->date)) :
                    $work = new Work;
                    $work->employee_id = $em->id;
                    $work->status      = 0;
                    $work->date        = now();
                    $work->save();
                endif;

                // if($em->clock_status == 0)
                // {
                //     if($em->clock_status == 1)
                //     {
                //         $em->clock_status = 2;
                //         $em->save();
                //     }
                    
                // }

                if($em->clock_status == 2) // ออกงาน
                {
                    if ($time < $set)
                    {
                        $em->clock_status = 0;
                        $em->save();
                    }
                }
            } // endforeach
        } // endif

        return view('clocktime.index',
              compact('datetime',
                      'employee_month',
                      'employee_day',
                      'data',
                      'clock_in',
                      'time',
                      'clock_out',
                      'out',
                      'leavetype',
                      'holiday',
                      'check_holiday'));
    }

    public function clock_in(Request $request)
    {
        $clock_in = new Clocktime;
        $clock_in->employee_id  = $request->id;
        $clock_in->clock_in     = now();
        $clock_in->clock_out    = now();
        $clock_in->clock_status = 1;
        $clock_in->save();

        $employee = Employee::find($request->id);
        $employee->clock_status = 1;
        $employee->save();

        $late   = Time::where('type', 'late')->first();

        $time   = Carbon::now(); // เวลา ปัจจุบัน
        $set    = Carbon::createFromTime($late->hour, $late->minute, 00); // ตั้งเวลา มาสาย
        $status = $time > $set ?  2 : 1;

        $work = Work::where('employee_id', $request->id)->whereDate('date', date('Y-m-d'))->first();
        $work->status = $status;
        $work->save();
    }

    public function clock_out(Request $request)
    {
        $clock_out = Clocktime::where('employee_id', $request->id)->whereDate('clock_in', now()->toDateString())->first();
        $clock_out->clock_out = now();
        $clock_out->clock_status = 2;
        $clock_out->save();

        $employee = Employee::find($request->id);
        $employee->clock_status = 2;
        $employee->save();
        // Cookie::queue('clock_out', 'test', 1);
    }

    // Clock Time Report
    public function report($id = NULL)
    {
        $data      = EmployeeLibrary::Data();
        $holiday   = Holiday::all();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        if($id == Auth::user()->username):
            $employee  = Employee::where('code', $id)->first();
            $clocktime = Clocktime::join('employees',  'employees.id' , '=', 'clocktimes.employee_id')
            ->select('clocktimes.*', 'employees.prename', 'employees.name', 'employees.surname')
            ->orderBy('clocktimes.clock_in', 'DESC')
            ->where('employees.id', $employee->id)
            ->get();
        elseif (Auth::user()->hasRole('Admin')) :
            $clocktime = Clocktime::join('employees',  'employees.id' , '=', 'clocktimes.employee_id')
            ->select('clocktimes.*', 'employees.prename', 'employees.name', 'employees.surname')
            ->orderBy('clocktimes.clock_in', 'DESC')
            ->get();
        else :
            return view('404');
        endif;
        return view('clocktime.report.index')->withClocktime($clocktime)->withData($data)->withHoliday($holiday)->withLeavetype($leavetype);
    }

    public function edit(Request $request)
    {
        $clock  = Clocktime::find($request->id);
        $late   = Time::where('type', 'late')->first();
        $r_in   = DatetimeLibrary::dateTime($request->c_clock, $request->c_time);
        $c_in   = DatetimeLibrary::dateTime($clock->clock_in,  $late->time);
        switch ($request->clock):

            case "clock_in" :
                $clock->clock_in  = $request->c_clock." ".$request->c_time;
                $status = ($r_in > $c_in) ?  2 : 1;
                $date   = date('Y-m-d', strtotime($request->c_clock));
                $work   = Work::where('employee_id', $clock->employee_id)->whereDate('date', $date)->first();
                $work->status = $status;
                $work->save();
            break;

            case "clock_out" : $clock->clock_out = $request->c_clock." ".$request->c_time; break;

        endswitch;

        $clock->save();

        return back()->with('update', 'Update Successfully!');
        // dd($request);
        // แก้ไขเวลา เขา้งาน ถ้า หลัง 08.00 ให้ อัพเดต เป็น มาสาย
    }

    public function destroy(Request $request)
    {
        Work::where('employee_id', $request->employee)->whereDate('date', $request->date)->delete();
        Clocktime::destroy($request->id);
    }

    public function destroy_all(Request $request)
    {
        $clocktime = Clocktime::find($request->id);
        foreach ($clocktime as $rs) :
            Work::where('employee_id', $rs->employee_id)->whereDate('date', Carbon::parse($rs->clock_in)->format('Y-m-d'))->delete();
        endforeach;
        Clocktime::destroy($request->id);
        return redirect('report/clocktime')->with('delete', 'Delete Successfully !');
    }

    // OT  กดเปิด  ot  ก่อน
    // Absence  //  late 30 นาที  // 10 โมง // ลา วันที่ ล่วง // ADmin อนุมัติ  ขึ้น status ลาทันที

    // วิธีการ เพิ่ม employee ลงไปใน ตาราง work ก่อน ตามวัน --------

    // Conduct  //  เลือก พนักงาน  รายละเอียด   เลือก ประเภท ดี  ไม่ดี
    // Saraly Silp  // ใบ ข้อมูล show
    // รายการ ที่จะหัก ----

    // เลิกงาน หกโมง เข้างาน แปดโมง
    // รายงาน ขาด ลา มาสาย PDF
    // ดูย้อนหลัง ได้ ด้วย

    // Active OT ทีหลัง

    // Admin
    // Employee Password

    // ตัด week 1 - 15 และ 16 - 30/31

    // ขาดลา นับตาม week ตามปี ไม่นับ วันทิด

    // รายวัน / 8 * 1.5  ครึ่ง                        ชั่วโมง / 2   คิดโอที
    
    // การลา ถ้าออกก่อน ถือว่า ลา

    // ตอน ลบ เวลางาน ให้ ระวังหน่อย เพราะจะไปลบ ตาราง work ด้วย
}
