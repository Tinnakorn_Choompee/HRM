<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name'     => 'required|unique:users,name,'.$request->id,
            'username' => 'required|unique:users,username,'.$request->id,
            'email'    => 'required|unique:users,email,'.$request->id,
            'password' => 'required|min:6|max:12',
            'image'    => 'nullable|mimes:jpeg,png,jpg',
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'มีชื่อนี้แล้ว',
            'username.unique' => 'มีชื่อเข้าใช้นี้แล้ว',
            'password.min' => 'กรุณากรอกรหัสผ่าน 6 ตัวขึ้นไป',
            'password.max' => 'กรุณากรอกรหัสผ่านเท่ากับ 12 ตัว หรือ น้อยกว่า 12 ตัว',
            'email.unique'    => 'มีอีเมล์นี้แล้ว',
        ];
    }
}
