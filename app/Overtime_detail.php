<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overtime_detail extends Model
{
    protected $guarded = [];

    public function Overtime()
    {
        return $this->belongsTo(Overtime::class);
    }
}
