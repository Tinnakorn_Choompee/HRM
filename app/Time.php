<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Time extends Model
{
    public function getTimeAttribute($value)
    {
        return Carbon::parse($value)->format('H:i');
    }

    public function getHourAttribute()
    {
        return Carbon::parse($this->time)->format('G');
    }

    public function getMinuteAttribute()
    {
        return Carbon::parse($this->time)->format('i');
    }
}
