<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    // protected $fillable = ['first_name','last_name','gender_id'];
    protected $guarded = [];

    public function Education()
    {
        return $this->hasOne(Education::class);
    }

    public function Conduct()
    {
        return $this->hasMany(Conduct::class);
    }

    public function Clocktime()
    {
        return $this->hasMany(Clocktime::class);
    }

    public function Leave()
    {
        return $this->hasMany(Leave::class);
    }

    public function Work()
    {
        return $this->hasMany(Work::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function Salary()
    {
        return $this->hasMany(Salary::class);
    }


    // public function getFullNameAttribute()
    // {
    //     return $this->first_name.' '.$this->last_name;
    // }
}
