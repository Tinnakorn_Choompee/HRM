<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $guarded = [];

    public function Leave()
    {
        return $this->hasMany(Leave::class);
    }
}
