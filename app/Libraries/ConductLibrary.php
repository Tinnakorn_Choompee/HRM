<?php 
namespace Libraries\ConductLibrary;

class ConductLibrary 
{
    public static function Data()
    {
        return ['prename' => [ 1 => 'นาย', 2 => 'นาง', 3 =>'นางสาว'], 'type' => [ 1 => 'ดี', 2 => 'ไม่ดี' ]];
    }
}
