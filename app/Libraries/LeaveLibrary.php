<?php 
namespace Libraries\LeaveLibrary;

class LeaveLibrary 
{
    public static function Status()
    {
        return [    0 => ['status'=>'ไม่อนุมัติ'     , 'color'=> 'red'], 
                    1 => ['status'=>'รอการพิจารณา' , 'color'=> 'yellow'],
                    2 => ['status'=>'อนุมัติเรียบร้อย' , 'color'=> 'green'],
                ];
    }

    public static function Type($type)
    {
        switch ($type) :
            case 1 : return "ครึ่งเช้า"; break;
            case 2 : return "ครึ่งบ่าย"; break;
            case 3 : return "เต็มวัน";  break;
        endswitch;
    }
}
 // สถานะการลา  0 ไม่อนุมัติ  1 รอการพิจารณา  2 ทำการอนุมัติเรียบร้อย
