<?php 
namespace Libraries\DatetimeLibrary;
use Carbon\Carbon;

class DatetimeLibrary 
{
    public static function dateTime($date, $time)
    {
        $d = Carbon::parse($date);
        $t = Carbon::parse($time);
        return Carbon::create($d->year, $d->month, $d->day, $t->hour,  $t->minute, 00);
    }
    
}

