<?php
namespace Libraries\ClockLibrary;
use Carbon\Carbon;

class ClockLibrary
{
    public static function c_diff($in, $out)
    {
        // return gmdate('H:i', ClockLibrary::c_in($in)->diffInSeconds(ClockLibrary::out($out)));
        return gmdate('H:i', ClockLibrary::c_in($in)->diffInSeconds(ClockLibrary::out($out)));
    }

    public static function c_in($in)
    {
        return (Carbon::parse($in)  < Carbon::parse($in)->setTime(8, 00, 00) ? Carbon::parse($in)->setTime(8, 00, 00) : Carbon::parse($in));
    }

    public static function out($out)
    {
        return Carbon::parse($out)->setTime(12, 0, 0) < Carbon::parse($out) && Carbon::parse($out) < Carbon::parse($out)->setTime(13, 0, 0)
            ?  Carbon::parse($out)->setTime(12, 0, 0) : Carbon::parse($out)->setTime(12, 0, 0) == Carbon::parse($out)
            ?  Carbon::parse($out) : Carbon::parse($out)->subHour(1);
    }
}

// {{--  <span style="float:right"> ชม. {{  gmdate('H:i:s', Carbon::parse($c['clock_in'])->diffInSeconds(Carbon::parse($c['clock_out'])->subHour(1))) }}  </span> </h5>  --}}
