<?php
namespace Libraries\MenuLibrary;
use App\Leave;

class MenuLibrary
{
    public static function Menu()
    {
        return [

                1 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/home', 'icon' => 'ion-android-color-palette', 'name' => 'หน้าหลัก']
                    ]
                ],

                2 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/employee', 'icon' => 'ion-person-stalker', 'name' => 'พนักงาน']
                    ]
                ],

                3 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/clocktime', 'icon' => 'ion-clock', 'name' => 'ลงเวลางาน']
                    ]
                ],

                4 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/report/clocktime', 'icon' => 'ion-clipboard', 'name' => 'บันทึกเวลางาน']
                    ]
                ],

                5 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/work', 'icon' => 'ion-pricetags', 'name' => 'บันทึกขาดลามาสาย']
                    ]
                ],

                6 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/conduct', 'icon' => 'ion-ios-compose-outline', 'name' => 'ความประพฤติพนักงาน']
                    ]
                ],

                7 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/overtime', 'icon' => 'ion-android-time', 'name' => 'ทำงานล่วงเวลา']
                    ]
                ],

                8 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/leave', 'icon' => 'ion-ios-copy-outline', 'name' => 'ข้อมูลการลา', 'alert'=> Leave::where('status',1)->count()]
                    ]
                ],

                9 => ['menu' => ['submenu'=> FALSE ,
                        'list' => ['url' => '/salary', 'icon' => 'ion-social-usd', 'name' => 'ใบสลิปเงินเดือน']
                    ]
                ],

        ];
    }

    public static function Report()
    {
        return [

            1 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/report/conduct', 'icon' => 'ion-stats-bars', 'name' => 'รายงานความประพฤติ']
                ]
            ],

            2 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/report/work', 'icon' => 'ion-pie-graph', 'name' => 'รายงานขาดลามาสาย']
                ]
            ],

        ];
    }

    public static function Setting()
    {
        return [

            1 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/time', 'name' => 'ตั้งค่าเวลา']
                ]
            ],

            2 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/position' , 'name' => 'จัดการตำแหน่ง']
                ]
            ],

            3 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/holiday', 'name' => 'จัดการวันหยุด']
                ]
            ],

            4 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/leavetype', 'name' => 'ประเภทการลา']
                ]
            ],

            5 => ['menu' => ['submenu'=> FALSE ,
                    'list' => ['url' => '/user', 'name' => 'ผู้ใช้งานระบบ']
                ]
            ],

        ];
    }
}
