<?php 
namespace Libraries\ColorLibrary;

class ColorLibrary 
{
    public static function Color()
    {
        return [ 
                'rgba(54, 162, 235, 0.2)'  , 'rgba(255, 99, 132, 0.2)' , 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)' , 'rgba(230, 126, 34, 0.2)' , 'rgba(44, 62, 80, 0.2)'  , 'rgba(255, 159, 64, 0.2)',
                'rgba(155, 89, 182, 0.2)'  , 'rgba(22, 160, 133, 0.2)' , 'rgba(211, 84, 0, 0.2)'  , 'rgba(52, 73, 94, 0.2)'
            ]; 
    }

    public static function Border()
    {
        return [ 
                'rgba(54, 162, 235, 1)'  , 'rgba(255, 99, 132, 1)' , 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)' , 'rgba(230, 126, 34, 1)' , 'rgba(44, 62, 80, 1)'  , 'rgba(255, 159, 64, 1)',
                'rgba(155, 89, 182, 1)'  , 'rgba(22, 160, 133, 1)' , 'rgba(211, 84, 0, 1)'  , 'rgba(52, 73, 94, 1)'
            ];
    }

}