<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $guarded = [];

    public function Employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function LeaveType()
    {
        return $this->belongsTo(LeaveType::class);
    }
}
