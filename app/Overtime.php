<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overtime extends Model
{
    protected $guarded = [];

    public function Overtime_detail()
    {
        return $this->hasMany(Overtime_detail::class, 'overtimes_id');
    }
}
