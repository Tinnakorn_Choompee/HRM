$(document).ready(function(){

    function animationHover(element, animation){
        element = $(element);
        element.hover(
            function() {
                element.addClass('animated ' + animation);        
            },
            function(){
                window.setTimeout( function(){
                    element.removeClass('animated ' + animation);
                },1000);         
            });
    }

    function animationClick(element, animation){
        element = $(element);
        element.click(
            function() {
                element.addClass('animated ' + animation);        
                window.setTimeout( function(){
                    element.removeClass('animated ' + animation);
                }, 2000);         
      
            });
    }

    function animationShow(element, animation){
        element = $(element);
        element.show(
            function() {
                element.addClass('animated ' + animation);        
                window.setTimeout( function(){
                    element.removeClass('animated ' + animation);
                }, 1000);         
      
            });
    }
    
    $('#btn-Time').each(function() {
        animationHover(this, 'pulse');
    });

    $('#btn-Time').click(function() {
        animationShow('#employee_time', 'fadeInUp');
        animationShow('.employee', 'fadeInUp');
    });

    $('#home').each(function() {
        animationHover(this, 'rubberBand');
    });

    $('#time').each(function() {
        animationHover(this, 'rubberBand');
    });

    $('#btn-About').each(function() {
        animationHover(this, 'rubberBand');
    });

    $('#home').click(function() {
        animationShow('#1', 'fadeIn');
        animationShow('#2', 'fadeInDown');
        animationShow('#3', 'zoomIn');
    });

    $('#logo').click(function() {
        animationShow('#1', 'fadeIn');
        animationShow('#2', 'fadeInDown');
        animationShow('#3', 'zoomIn');
    });

    $('#time').click(function() {
        animationShow('#employee_time', 'fadeInUp');
        animationShow('.employee', 'fadeInUp');
    });


    $('#btn-About').click(function() {
        animationShow('.response-about', 'pulse');
    });

    (function () {
        function checkTime(i) {
            return (i < 10) ? "0" + i : i;
        }
        function startTime() {
            var today = new Date(),
                h = checkTime(today.getHours()),
                m = checkTime(today.getMinutes()),
                s = checkTime(today.getSeconds());
           $('#h').html(h);
           $('#m').html(m);
           $('#s').html(s);
            t = setTimeout(function () {
                startTime()
            }, 500);
        }
        startTime();
    })();
});







