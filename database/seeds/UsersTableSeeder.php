<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user    = Role::where('name', 'User')->first();
        $role_admin   = Role::where('name', 'Admin')->first();
        $role_manager = Role::where('name', 'Manager')->first();

        // $user = new User();
        // $user->name  = 'User';
        // $user->email = 'User@gmail.com';
        // $user->username = 'user';
        // $user->password = bcrypt('user');
        // $user->save();
        // $user->roles()->attach($role_user);

        $user = new User();
        $user->name  = 'ทินกร';
        $user->email = 'user1@gmail.com';
        $user->username = '0002';
        $user->password = bcrypt('0002');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name  = 'มานี';
        $user->email = 'user2@gmail.com';
        $user->username = '0003';
        $user->password = bcrypt('0003');
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name  = 'ทดสอบ';
        $user->email = 'user3@gmail.com';
        $user->username = '0004';
        $user->password = bcrypt('0004');
        $user->save();
        $user->roles()->attach($role_user);
    }
}
