<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // factory(App\Event::class,10)->create();
        $this->call(RoleTableSeeder::class);
        // $this->call(EventsTableSeeder::class);
    }
}

// php artisan db:seed --class=UsersTableSeeder
