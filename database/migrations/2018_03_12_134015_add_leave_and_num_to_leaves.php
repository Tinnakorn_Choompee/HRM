<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeaveAndNumToLeaves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaves', function($table) {
            $table->string('type_leave')->after('type_name');
            $table->float('num', 8, 1)->after('type_leave');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaves', function($table) {
            $table->dropColumn('type_leave');
            $table->dropColumn('num');
        });
    }
}
