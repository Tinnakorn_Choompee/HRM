<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->float('salary', 8, 2);
            $table->float('overtime', 8, 2);
            $table->float('ar', 8, 2);
            $table->float('commission', 8, 2);
            $table->float('insurance', 8, 2);
            $table->string('other', 150);
            $table->float('amount_other', 8, 2);
            $table->tinyInteger('week');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
