<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DorpClockStatusInClocktimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clocktimes', function (Blueprint $table) {
            $table->dropColumn('clock_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clocktimes', function (Blueprint $table) {
            $table->tinyInteger('clock_status')->after('clock_out');
        });
    }
}
