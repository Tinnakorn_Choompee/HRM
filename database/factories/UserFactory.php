<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Conduct::class, function (Faker $faker) {
    static $password;

    return [
        'employee_id' => $faker->numberBetween($min = 78, $max = 79),
        'type'        => $faker->numberBetween($min = 1, $max = 2),
        'title'       => $faker->name,
        'detail'      => $faker->address,
        'date'        => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(4),
        'start' => $faker->dateTimeThisMonth(),
        'end'   => $faker->dateTimeThisMonth(),
        'color' => $faker->hexcolor,
    ];
});

    // return [
    //     'code'        => $faker->numberBetween($min = 0001, $max = 1000),
    //     'prename'     => $faker->numberBetween($min = 1, $max = 3),
    //     'name'        => $faker->name,
    //     'birthday'    => $faker->date($format = 'Y-m-d', $max = 'now'),
    //     'age'         => $faker->numberBetween($min = 1, $max = 100),
    //     'blood'       => $faker->numberBetween($min = 1, $max = 3),
    //     'race'        => $faker->numberBetween($min = 1, $max = 3),
    //     'nationality' => $faker->numberBetween($min = 1, $max = 3),
    //     'religion'    => $faker->numberBetween($min = 1, $max = 3),
    //     'address'     => $faker->address,
    //     'card_id'     => $faker->numberBetween($min = 1, $max = 3),
    //     'tel'         => $faker->postcode,
    //     'type'        => $faker->numberBetween($min = 1, $max = 2),
    //     'position'    => $faker->numberBetween($min = 1, $max = 3),
    //     'salary'      => $faker->numberBetween($min = 10000, $max = 15000),
    //     'image'       => 'user.png',
    // ];
