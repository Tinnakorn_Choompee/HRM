<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('หน้าหลัก', route('home'));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::register('post', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('category', $post->category);
    $breadcrumbs->push($post->title, route('post', $post));
});

// Employee
Breadcrumbs::register('employee.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('จัดการพนักงาน', route('employee.index'));
});

Breadcrumbs::register('employee.create', function ($breadcrumbs) {
    $breadcrumbs->parent('employee.index');
    $breadcrumbs->push('เพิ่มพนักงาน', route('employee.create'));
});

Breadcrumbs::register('employee.show', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('employee.index');
    $breadcrumbs->push('ข้อมูลพนักงาน', route('employee.show', $employee->id));
});

Breadcrumbs::register('employee.edit', function ($breadcrumbs, $employee) {
    $breadcrumbs->parent('employee.index', $employee);
    $breadcrumbs->push('แก้ไขพนักงาน', route('employee.edit', $employee->id));
});

Breadcrumbs::register('employee.profile', function ($breadcrumbs, $id) {
    $breadcrumbs->push('ข้อมูลพนักงาน', route('employee.profile', $id));
});

Breadcrumbs::register('employee.edit_profile', function ($breadcrumbs, $id) {
    $breadcrumbs->push('แก้ไขพนักงาน', route('employee.edit_profile', $id));
});

// Position
Breadcrumbs::register('position.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('จัดการตำแหน่ง', route('position.index'));
});

// Clocktime
Breadcrumbs::register('clocktime.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ลงเวลางาน', route('clocktime.index'));
});

// Clocktime
Breadcrumbs::register('clocktime.report', function ($breadcrumbs, $id = NULL) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกเวลางาน', route('clocktime.report', $id = NULL));
});

// Conduct
Breadcrumbs::register('conduct.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกความประพฤติ', route('conduct.index'));
});

Breadcrumbs::register('conduct.create', function ($breadcrumbs) {
    $breadcrumbs->parent('conduct.index');
    $breadcrumbs->push('เพิ่มความประพฤติ', route('conduct.create'));
});

Breadcrumbs::register('conduct.edit', function ($breadcrumbs, $conduct) {
    $breadcrumbs->parent('conduct.index');
    $breadcrumbs->push('แก้ไขความประพฤติ', route('conduct.edit', $conduct->id));
});

Breadcrumbs::register('conduct.type', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.index');
    $breadcrumbs->push('ประเภทความประพฤติ', route('conduct.type', $id));
});

Breadcrumbs::register('conduct.profile', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.index');
    $breadcrumbs->push('ความประพฤติพนักงาน', route('conduct.profile', $id));
});

Breadcrumbs::register('conduct.profile_good', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.profile', $id);
    $breadcrumbs->push('ความประพฤติพนักงานตามประเภทดี', route('conduct.profile_good',  $id));
});

Breadcrumbs::register('conduct.profile_bad', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.profile', $id);
    $breadcrumbs->push('ความประพฤติพนักงานตามประเภทไม่ดี', route('conduct.profile_bad',  $id));
});

// Conduct Report
Breadcrumbs::register('conduct.report', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('รายงานความประพฤติ', route('conduct.report'));
});

Breadcrumbs::register('conduct.type_report', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.report');
    $breadcrumbs->push('ประเภทความประพฤติ', route('conduct.type_report', $id));
});

Breadcrumbs::register('conduct.month', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.report');
    $breadcrumbs->push('รายงานความประพฤติรายเดือน', route('conduct.month', $id));
});

Breadcrumbs::register('conduct.month_good', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.month', $id);
    $breadcrumbs->push('ความประพฤติพนักงานตามประเภทดี', route('conduct.month_good',  $id));
});

Breadcrumbs::register('conduct.month_bad', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.month', $id);
    $breadcrumbs->push('ความประพฤติพนักงานตามประเภทไม่ดี', route('conduct.month_bad',  $id));
});

Breadcrumbs::register('conduct.profile_report', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('conduct.report', $id);
    $breadcrumbs->push('ความประพฤติพนักงาน', route('conduct.profile_report',  $id));
});

// Holiday
Breadcrumbs::register('holiday.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('จัดการวันหยุด', route('holiday.index'));
});

// Time
Breadcrumbs::register('time.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ตั้งค่าเวลา', route('time.index'));
});

// Leave Type
Breadcrumbs::register('leavetype.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ประเภทการลา', route('leavetype.index'));
});

// Leave
Breadcrumbs::register('leave.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลการลา', route('leave.index'));
});

Breadcrumbs::register('leave.profile', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลการลา', route('leave.profile', $id));
});

// Work
Breadcrumbs::register('work.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกขาดลามาสาย', route('work.index'));
});

Breadcrumbs::register('work.employee', function ($breadcrumbs ,$id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกขาดลามาสาย', route('work.employee', $id));
});

Breadcrumbs::register('work.year', function ($breadcrumbs ,$id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกขาดลามาสาย', route('work.year', $id));
});

Breadcrumbs::register('work.month', function ($breadcrumbs ,$id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกขาดลามาสาย', route('work.month', $id));
});

Breadcrumbs::register('work.date', function ($breadcrumbs ,$id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกขาดลามาสาย', route('work.date', $id));
});

Breadcrumbs::register('work.status', function ($breadcrumbs ,$id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บันทึกขาดลามาสาย', route('work.status', $id));
});

Breadcrumbs::register('work.report', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('รายงานขาดลามาสาย', route('work.report'));
});

// User
Breadcrumbs::register('user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ผู้ใช้งานระบบ', route('user.index'));
});

Breadcrumbs::register('user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('เพิ่มผู้ใช้งานระบบ', route('user.create'));
});

Breadcrumbs::register('user.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('แก้ไขผู้ใช้งานระบบ', route('user.edit', $id));
});

Breadcrumbs::register('user.profile', function ($breadcrumbs, $id) {
    $breadcrumbs->push('ข้อมูลผู้ใช้งานระบบ', route('user.profile', $id));
});

// Event
Breadcrumbs::register('event.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การทำงานล่วงเวลา', route('event.index'));
});

// Saraly
Breadcrumbs::register('salary.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ใบสลิปเงินเดือน', route('salary.index'));
});
Breadcrumbs::register('salary.create', function ($breadcrumbs) {
    $breadcrumbs->parent('salary.index');
    $breadcrumbs->push('เพิ่มใบสลิปเงินเดือน', route('salary.create'));
});
Breadcrumbs::register('salary.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('salary.index');
    $breadcrumbs->push('แก้ไขใบสลิปเงินเดือน', route('salary.edit', $id));
});

// Overtime
Breadcrumbs::register('overtime.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การทำงานล่วงเวลา', route('overtime.index'));
});

Breadcrumbs::register('overtime.create', function ($breadcrumbs) {
    $breadcrumbs->parent('overtime.index');
    $breadcrumbs->push('เพิ่มการทำงานล่วงเวลา', route('overtime.create'));
});


Breadcrumbs::register('overtime.show', function ($breadcrumbs , $id) {
    $breadcrumbs->parent('overtime.index');
    $breadcrumbs->push('การทำงานล่วงเวลา', route('overtime.show', $id));
});

Breadcrumbs::register('overtime.edit', function ($breadcrumbs , $id) {
    $breadcrumbs->parent('overtime.index');
    $breadcrumbs->push('แก้ไขการทำงานล่วงเวลา', route('overtime.edit', $id));
});