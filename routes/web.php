<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $employee = App\Employee::all();
    $data     = Libraries\EmployeeLibrary\EmployeeLibrary::Data();
    return view('index', compact('employee', 'data'));
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    // Employee
    Route::group(['middleware' => ['roles'], 'roles' => ['Admin' , 'Manager']], function () {
        Route::resource('/employee', 'EmployeeController');
    });

     // Employee
     Route::group(['middleware' => ['roles'], 'roles' => ['User']], function () {
        Route::get('/employee/profile/{id}', 'EmployeeController@profile')->name('employee.profile');
        Route::get('/employee/edit/{id}', 'EmployeeController@edit_profile')->name('employee.edit_profile');
        Route::put('/employee/update/{id}', 'EmployeeController@update_profile');
    });

    // Position
    Route::get('/position', 'PositionController@index')->name('position.index');
    Route::post('/position', 'PositionController@store');
    Route::delete('/position', 'PositionController@destroy');
    Route::put('/position', 'PositionController@update');
    Route::put('/position/up', 'PositionController@up');
    Route::put('/position/down', 'PositionController@down');

    // Clocktime
    Route::get('/clocktime', 'ClocktimeController@index')->name('clocktime.index');
    Route::post('/clocktime', 'ClocktimeController@clock_in');
    Route::put('/clocktime', 'ClocktimeController@clock_out');

    // Clocktime Report
    Route::get('/report/clocktime/{id?}', 'ClocktimeController@report')->name('clocktime.report');
    Route::post('/clocktime/edit', 'ClocktimeController@edit');
    Route::delete('/clocktime', 'ClocktimeController@destroy');
    Route::delete('/clock_all', 'ClocktimeController@destroy_all');

    // Conduct
    Route::resource('/conduct', 'ConductController');
    Route::get('/conduct/type/{id}', 'ConductController@type')->name('conduct.type');
    Route::post('/conduct/employee',  'ConductController@employee');
    Route::post('/conduct/date', 'ConductController@date');
    Route::get('/conduct/profile/{id}', 'ConductController@profile')->name('conduct.profile');
    Route::get('/conduct/profile_good/{id}', 'ConductController@profile_good')->name('conduct.profile_good');
    Route::get('/conduct/profile_bad/{id}', 'ConductController@profile_bad')->name('conduct.profile_bad');

    // Conduct Report
    Route::get('/report/conduct', 'ConductController@report')->name('conduct.report');
    Route::get('/conduct/result/{id}', 'ConductController@result');
    Route::get('/conduct/report/{id}', 'ConductController@type_report')->name('conduct.type_report');
    Route::get('/conduct/month/{id}', 'ConductController@month')->name('conduct.month');
    Route::get('/conduct/month_good/{id}', 'ConductController@month_good')->name('conduct.month_good');
    Route::get('/conduct/month_bad/{id}', 'ConductController@month_bad')->name('conduct.month_bad');
    Route::get('/conduct/profile_report/{id}', 'ConductController@profile_report')->name('conduct.profile_report');

    // Holiday
    Route::get('/holiday', 'HolidayController@index')->name('holiday.index');
    Route::post('/holiday', 'HolidayController@store');
    Route::put('/holiday', 'HolidayController@update');
    Route::delete('/holiday', 'HolidayController@destroy');

    // Time
    Route::get('/time', 'TimeController@index')->name('time.index');
    Route::put('/time', 'TimeController@update');

    // Leave Type
    Route::get('/leavetype', 'LeaveTypeController@index')->name('leavetype.index');
    Route::post('/leavetype', 'LeaveTypeController@store');
    Route::put('/leavetype', 'LeaveTypeController@update');
    Route::delete('/leavetype', 'LeaveTypeController@destroy');
    Route::put('/leavetype/up', 'LeaveTypeController@up');
    Route::put('/leavetype/down', 'LeaveTypeController@down');

    // Leave
    Route::resource('/leave', 'LeaveController');
    Route::post('/leave/status', 'LeaveController@status');
    Route::get('/leave/profile/{id}', 'LeaveController@profile')->name('leave.profile');

    // Work
    Route::get('/work', 'WorkController@index')->name('work.index');
    Route::get('/work/{id}', 'WorkController@employee')->name('work.employee');
    Route::get('/work/year/{id}', 'WorkController@year')->name('work.year');
    Route::get('/work/month/{id}', 'WorkController@month')->name('work.month');
    Route::get('/work/date/{id}', 'WorkController@date')->name('work.date');
    Route::get('/work/status/{id}', 'WorkController@status')->name('work.status');
    Route::get('/report/work', 'WorkController@report')->name('work.report');

    // Change Password
    Route::put('/user/password', 'UserController@password');

    // Event
    Route::get('event', 'EventController@index')->name('event.index');
    Route::get('event/json', 'EventController@json')->name('event.json');

    //Saraly
    Route::get('/salary', 'SalaryController@index')->name('salary.index');
    Route::get('/salary/slip/{id}', 'SalaryController@slip')->name('salary.silp');
    Route::get('/salary/create', 'SalaryController@create')->name('salary.create');
    Route::post('/salary', 'SalaryController@store');
    Route::get('/salary/{id}/edit', 'SalaryController@edit')->name('salary.edit');
    Route::PATCH('/salary/{id}', 'SalaryController@update')->name('salary.edit');
    Route::get('/salary/show', 'SalaryController@show');

    //Overtime
    Route::resource('/overtime', 'OvertimeController');

    Route::group(['middleware' => ['roles'], 'roles' => ['Admin']], function () {
        // User
        Route::resource('/user', 'UserController');
    });
    
    Route::group(['middleware' => ['roles'], 'roles' => ['Admin', 'Manager']], function () {
        Route::get('/user/profile/{id}', 'UserController@profile')->name('user.profile');
    });
    
});




// Route::group(['middleware' => 'web'], function () {
//     Route::get('/', function () {
//         return view('index');
//     })->name('main');

//     Route::get('/author', [
//         'uses' => 'AppController@getAuthorPage',
//         'as' => 'author',
//         'middleware' => 'roles',
//         'roles' => ['Admin', 'Author']
//     ]);

//     Route::get('/author/generate-article', [
//         'uses' => 'AppController@getGenerateArticle',
//         'as' => 'author.article',
//         'middleware' => 'roles',
//         'roles' => ['Author', 'Admin']
//     ]);

//     Route::get('/admin', [
//         'uses' => 'AppController@getAdminPage',
//         'as' => 'admin',
//         'middleware' => 'roles',
//         'roles' => ['Admin']
//     ]);

//     Route::post('/admin/assign-roles', [
//         'uses' => 'AppController@postAdminAssignRoles',
//         'as' => 'admin.assign',
//         'middleware' => 'roles',
//         'roles' => ['Admin']
//     ]);

//     Route::get('/signup', [
//         'uses' => 'AuthController@getSignUpPage',
//         'as' => 'signup'
//     ]);

//     Route::post('/signup', [
//         'uses' => 'AuthController@postSignUp',
//         'as' => 'signup'
//     ]);

//     Route::get('/signin', [
//         'uses' => 'AuthController@getSignInPage',
//         'as' => 'signin'
//     ]);

//     Route::post('/signin', [
//         'uses' => 'AuthController@postSignIn',
//         'as' => 'signin'
//     ]);

//     Route::get('/logout', [
//         'uses' => 'AuthController@getLogout',
//         'as' => 'logout'
//     ]);
// });
