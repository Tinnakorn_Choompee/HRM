-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2018 at 07:55 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `clocktimes`
--

CREATE TABLE `clocktimes` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `clock_in` datetime NOT NULL,
  `clock_out` datetime NOT NULL,
  `clock_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clocktimes`
--

INSERT INTO `clocktimes` (`id`, `employee_id`, `clock_in`, `clock_out`, `clock_status`, `created_at`, `updated_at`) VALUES
(42, 78, '2018-01-03 07:34:25', '2018-01-03 17:10:00', 2, '2018-01-03 00:34:25', '2018-01-05 07:15:27'),
(54, 78, '2018-01-04 07:50:00', '2018-01-04 11:00:00', 2, '2018-01-04 05:29:04', '2018-01-04 05:36:36'),
(55, 79, '2018-01-04 07:45:00', '2018-01-04 13:58:04', 2, '2018-01-04 05:29:07', '2018-01-04 06:58:04'),
(56, 78, '2018-01-05 07:51:00', '2018-01-05 20:29:21', 2, '2018-01-05 04:45:40', '2018-01-05 13:29:21'),
(57, 79, '2018-01-05 08:03:00', '2018-01-05 20:29:24', 2, '2018-01-05 04:45:43', '2018-01-05 13:29:24'),
(58, 80, '2018-01-05 08:10:00', '2018-01-05 20:30:40', 2, '2018-01-05 04:50:32', '2018-01-05 13:30:40'),
(65, 78, '2018-01-06 08:10:00', '2018-01-06 18:06:01', 2, '2018-01-06 05:59:59', '2018-01-06 11:06:01'),
(66, 79, '2018-01-06 07:55:00', '2018-01-06 18:06:04', 2, '2018-01-06 06:00:02', '2018-01-06 11:06:04'),
(67, 80, '2018-01-06 07:49:00', '2018-01-06 18:06:09', 2, '2018-01-06 06:00:06', '2018-01-06 11:06:09'),
(68, 78, '2018-01-11 10:00:00', '2018-01-11 17:00:00', 2, '2018-01-11 03:02:49', '2018-01-11 16:40:24'),
(69, 79, '2018-01-11 08:00:00', '2018-01-11 13:00:00', 2, '2018-01-11 03:02:51', '2018-01-25 14:44:26'),
(70, 80, '2018-01-11 10:00:00', '2018-01-11 19:00:00', 2, '2018-01-11 03:02:54', '2018-01-11 16:39:20'),
(71, 78, '2018-01-25 07:55:00', '2018-01-25 17:30:00', 2, '2018-01-25 14:47:28', '2018-02-15 12:37:31'),
(72, 79, '2018-01-25 07:58:00', '2018-01-25 17:11:00', 2, '2018-01-25 14:50:42', '2018-02-15 12:38:02'),
(73, 80, '2018-01-25 07:45:00', '2018-01-25 17:15:00', 2, '2018-01-25 14:53:58', '2018-02-15 12:37:09'),
(74, 78, '2018-02-16 11:58:48', '2018-02-16 17:36:26', 2, '2018-02-16 04:58:48', '2018-02-16 10:36:26'),
(75, 79, '2018-02-16 11:58:56', '2018-02-16 17:36:30', 2, '2018-02-16 04:58:56', '2018-02-16 10:36:30'),
(76, 80, '2018-02-16 11:58:59', '2018-02-16 17:38:04', 2, '2018-02-16 04:58:59', '2018-02-16 10:38:04'),
(77, 84, '2018-02-16 11:59:02', '2018-02-16 17:38:18', 2, '2018-02-16 04:59:02', '2018-02-16 10:38:18'),
(78, 85, '2018-02-16 07:55:00', '2018-02-16 17:38:22', 2, '2018-02-16 04:59:06', '2018-02-16 10:42:57'),
(94, 78, '2018-03-12 16:33:10', '2018-03-12 16:39:19', 2, '2018-03-12 09:33:10', '2018-03-12 09:39:19'),
(95, 79, '2018-03-12 16:40:23', '2018-03-12 16:40:59', 2, '2018-03-12 09:40:23', '2018-03-12 09:40:59'),
(96, 80, '2018-03-12 23:43:07', '2018-03-12 23:43:10', 2, '2018-03-12 16:43:07', '2018-03-12 16:43:10'),
(97, 84, '2018-03-12 23:43:12', '2018-03-12 23:43:16', 2, '2018-03-12 16:43:12', '2018-03-12 16:43:16'),
(98, 86, '2018-03-12 23:43:18', '2018-03-12 23:43:22', 2, '2018-03-12 16:43:18', '2018-03-12 16:43:22'),
(99, 85, '2018-03-12 23:43:24', '2018-03-12 23:43:28', 2, '2018-03-12 16:43:24', '2018-03-12 16:43:28'),
(100, 78, '2018-03-16 18:38:29', '2018-03-16 18:43:47', 2, '2018-03-16 11:38:29', '2018-03-16 11:43:47'),
(105, 78, '2018-03-23 15:16:31', '2018-03-23 15:27:29', 2, '2018-03-23 08:16:31', '2018-03-23 08:27:29'),
(114, 78, '2018-04-16 08:04:00', '2018-04-16 18:04:00', 2, NULL, NULL),
(115, 79, '2018-04-16 08:04:29', '2018-04-16 18:04:21', 2, NULL, NULL),
(116, 80, '2018-04-16 07:26:20', '2018-04-16 18:04:06', 2, NULL, NULL),
(117, 84, '2018-04-16 08:02:00', '2018-04-16 18:01:00', 2, NULL, '2018-04-22 02:22:13'),
(118, 85, '2018-04-16 08:12:10', '2018-04-16 18:07:38', 2, NULL, NULL),
(119, 86, '2018-04-16 08:03:13', '2018-04-16 18:05:16', 2, NULL, NULL),
(120, 78, '2018-04-17 08:39:19', '2018-04-17 18:06:00', 2, NULL, NULL),
(121, 79, '2018-04-17 08:04:08', '2018-04-17 18:07:00', 2, NULL, NULL),
(122, 80, '2018-04-17 08:32:09', '2018-04-17 18:12:00', 2, NULL, NULL),
(123, 84, '2018-04-17 08:00:00', '2018-04-17 18:03:06', 2, NULL, NULL),
(124, 85, '2018-04-17 08:04:13', '2018-04-17 08:08:09', 2, NULL, NULL),
(125, 86, '2018-04-17 08:01:29', '2018-04-17 08:08:20', 2, NULL, NULL),
(126, 78, '2018-04-18 08:05:12', '2018-04-18 18:06:13', 2, NULL, NULL),
(127, 79, '2018-04-18 08:03:10', '2018-04-18 18:06:23', 2, NULL, NULL),
(128, 80, '2018-04-18 08:02:11', '2018-04-18 18:02:08', 2, NULL, NULL),
(129, 84, '2018-04-18 08:05:05', '2018-04-18 18:06:06', 2, NULL, NULL),
(130, 85, '2018-04-18 08:09:06', '2018-04-18 18:11:00', 2, NULL, NULL),
(131, 86, '2018-04-18 08:09:12', '2018-04-18 18:00:00', 2, NULL, NULL),
(132, 78, '2018-04-19 08:06:10', '2018-04-19 18:04:10', 2, NULL, NULL),
(133, 79, '2018-04-19 08:34:08', '2018-04-19 18:05:15', 2, NULL, NULL),
(134, 80, '2018-04-19 08:34:12', '2018-04-19 18:09:04', 2, NULL, NULL),
(135, 84, '2018-04-19 08:02:12', '2018-04-19 18:03:08', 2, NULL, NULL),
(137, 79, '2018-04-20 08:02:08', '2018-04-20 18:03:00', 2, NULL, '2018-04-22 03:19:06'),
(138, 80, '2018-04-20 08:09:06', '2018-04-20 18:05:00', 2, NULL, '2018-04-22 03:19:35'),
(139, 84, '2018-04-20 08:04:07', '2018-04-20 18:03:11', 2, NULL, NULL),
(140, 85, '2018-04-20 08:05:05', '2018-04-20 18:07:06', 2, NULL, NULL),
(141, 86, '2018-04-21 08:31:00', '2018-04-21 18:01:00', 2, NULL, '2018-04-22 03:18:50'),
(142, 78, '2018-04-21 08:04:07', '2018-04-21 18:02:15', 2, NULL, NULL),
(143, 79, '2018-04-21 08:02:10', '2018-04-21 18:03:00', 2, NULL, '2018-04-22 03:19:55'),
(144, 80, '2018-04-21 08:37:00', '2018-04-21 18:02:00', 2, NULL, '2018-04-22 03:19:44'),
(145, 84, '2018-04-21 08:40:08', '2018-04-21 18:01:15', 2, NULL, NULL),
(146, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(147, 78, '2018-04-23 07:24:35', '2018-04-23 22:43:54', 2, '2018-04-23 00:24:35', '2018-04-23 15:43:54'),
(148, 79, '2018-04-23 07:27:37', '2018-04-23 22:43:58', 2, '2018-04-23 00:27:37', '2018-04-23 15:43:58'),
(149, 80, '2018-04-23 07:27:38', '2018-04-23 22:44:02', 2, '2018-04-23 00:27:38', '2018-04-23 15:44:02'),
(150, 79, '2018-04-23 07:27:44', '2018-04-23 07:27:44', 1, '2018-04-23 00:27:44', '2018-04-23 00:27:44'),
(151, 79, '2018-04-23 07:27:47', '2018-04-23 07:27:47', 1, '2018-04-23 00:27:47', '2018-04-23 00:27:47'),
(152, 84, '2018-04-23 07:28:28', '2018-04-23 22:44:06', 2, '2018-04-23 00:28:28', '2018-04-23 15:44:06'),
(153, 86, '2018-04-23 22:44:10', '2018-04-23 22:44:10', 1, '2018-04-23 15:44:10', '2018-04-23 15:44:10'),
(154, 85, '2018-04-24 09:54:08', '2018-04-24 09:54:08', 1, '2018-04-24 02:54:08', '2018-04-24 02:54:08'),
(155, 78, '2018-04-24 09:55:32', '2018-04-24 09:55:32', 1, '2018-04-24 02:55:32', '2018-04-24 02:55:32'),
(156, 79, '2018-04-24 09:55:36', '2018-04-24 09:55:36', 1, '2018-04-24 02:55:36', '2018-04-24 02:55:36'),
(157, 80, '2018-04-24 09:55:39', '2018-04-24 09:55:39', 1, '2018-04-24 02:55:39', '2018-04-24 02:55:39'),
(158, 84, '2018-04-24 09:55:42', '2018-04-24 09:55:42', 1, '2018-04-24 02:55:42', '2018-04-24 02:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `conducts`
--

CREATE TABLE `conducts` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conducts`
--

INSERT INTO `conducts` (`id`, `employee_id`, `type`, `title`, `detail`, `date`, `created_at`, `updated_at`) VALUES
(10, 78, 1, 'เก็บขยะ', 'เก็บขยะบริเวณหน้าร้าน', '2017-12-12', '2017-12-18 12:05:59', '2018-04-22 03:27:28'),
(39, 80, 2, 'หลับในเวลางาน', 'แอบนอนหลับในเวลางาน', '2018-04-22', '2018-04-22 03:28:58', '2018-04-22 03:28:58'),
(40, 79, 2, 'กินขนม', 'กินขนมในเวลางาน', '2018-04-22', '2018-04-22 03:29:29', '2018-04-22 03:29:29'),
(41, 80, 1, 'ขยัน', 'ขยันทำงานขายของ', '2018-04-22', '2018-04-22 03:29:51', '2018-04-22 03:29:51'),
(42, 80, 1, 'ดดดด', 'กกกก', '2018-04-23', '2018-04-23 00:37:47', '2018-04-23 00:37:47');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `graduate` varchar(175) COLLATE utf8mb4_unicode_ci NOT NULL,
  `study` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` int(11) NOT NULL,
  `grade` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `employee_id`, `graduate`, `study`, `education`, `grade`, `created_at`, `updated_at`) VALUES
(6, 65, '132asdasdฟหกฟหกddd', '132', 1, '1', '2017-11-22 04:09:36', '2017-11-22 14:34:24'),
(7, 66, '132', '1321', 1, '1', '2017-11-22 07:49:27', '2017-11-22 07:49:27'),
(8, 67, '1', '1', 1, '1', '2017-11-22 07:53:24', '2017-11-22 07:53:24'),
(9, 68, '321', '321', 1, '2', '2017-11-22 11:43:16', '2017-11-22 11:43:16'),
(10, 69, '132', '132', 1, '1', '2017-11-23 09:08:19', '2017-11-23 09:08:19'),
(11, 70, '132', '132', 1, '1', '2017-11-23 09:25:43', '2017-11-23 09:25:43'),
(12, 71, '321', '321', 1, '2', '2017-11-23 09:38:24', '2017-11-23 09:38:24'),
(13, 72, '32', '123', 1, '1', '2017-11-23 15:31:01', '2017-11-23 15:31:01'),
(14, 73, '231', '23132', 1, '1', '2017-11-23 15:41:20', '2017-11-23 15:41:20'),
(15, 74, '321', '3213', 1, '3', '2017-11-23 16:55:27', '2017-11-23 16:55:27'),
(16, 75, '123', '132', 1, '4', '2017-11-23 16:55:43', '2017-11-23 16:55:43'),
(17, 76, '45', '45', 1, '4', '2017-11-23 16:55:57', '2017-11-23 16:55:57'),
(19, 78, 'โรงเรียนวัดขะจาว', 'คอม', 1, '4.00', '2017-11-24 04:19:15', '2018-03-23 08:03:46'),
(20, 79, 'โรงเรียน', 'คอม', 1, '1', '2017-11-24 10:54:32', '2018-04-22 08:11:16'),
(21, 80, 'มหาวัทยาลัยสันทราย', 'ธุรกิจ', 2, '3.80', '2017-12-19 05:01:33', '2018-03-23 08:07:59'),
(25, 84, 'โรงเรียนเจ๊เพย', 'หลังมอ', 3, '3.00', '2018-01-11 06:16:37', '2018-03-23 08:12:21'),
(26, 85, 'โรงเรียนสันป่าตอง', 'ท่องเที่ยว', 4, '2.50', '2018-01-11 17:57:57', '2018-03-23 08:10:18'),
(27, 86, 'โรงเรียนวัดดอน', 'คอม', 4, '3.00', '2018-03-04 02:05:38', '2018-03-23 08:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prename` tinyint(4) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `age` int(11) NOT NULL,
  `blood` tinyint(4) NOT NULL,
  `race` tinyint(4) NOT NULL,
  `nationality` tinyint(4) NOT NULL,
  `religion` tinyint(4) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_id` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `position` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clock_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `code`, `prename`, `name`, `surname`, `birthday`, `age`, `blood`, `race`, `nationality`, `religion`, `address`, `card_id`, `tel`, `type`, `position`, `salary`, `image`, `clock_status`, `created_at`, `updated_at`) VALUES
(78, '0002', 1, 'เอกวรพัทธ์', 'ไชยปัญญา', '1989-06-09', 28, 2, 99, 3, 1, '1322', '1100400491602', '0987722594', 1, 'ผู้จัดการ', 20000, 'user.png', 1, '2017-11-24 04:19:15', '2018-06-05 09:49:46'),
(79, '0003', 2, 'มานี', 'ดีใจ', '2012-11-13', 5, 1, 1, 1, 1, 'สันทราย', '1100444003940', '0984473625', 1, 'ผู้จัดการ', 13000, 'u87y6detailsquare3.jpg', 1, '2017-11-24 10:54:32', '2018-04-24 02:55:36'),
(80, '0004', 1, 'ทดสอบ', 'เทส', '1992-09-12', 25, 2, 1, 1, 1, '144 ซ.9 อ.สันทราย', '1500021312312', '0983384785', 1, 'รองผู้จัดการ', 8000, '9yhKVpexels-photo-568236.jpeg', 1, '2017-12-19 05:01:33', '2018-04-24 02:55:39'),
(84, '0005', 1, 'คิวคิว', 'อาร์อาร์', '1999-01-15', 19, 1, 1, 1, 1, 'asdasd', '123', '1231', 1, 'ผู้จัดการ', 4000, '1bshEavatar5.png', 1, '2018-01-11 06:16:37', '2018-04-24 02:55:42'),
(85, '0006', 1, 'รายวัน', 'ทดสอบ', '2006-01-11', 11, 1, 1, 1, 1, 'เชียงใหม่', '1500045544543', '0998847858', 2, 'พนักงาน', 300, 'user.png', 1, '2018-01-11 17:57:57', '2018-04-24 02:55:02'),
(86, '0007', 1, 'อออ่าง', 'กอไก่', '1958-03-05', 59, 1, 1, 1, 1, 'ตาก', '1100154584818', '0948584858', 1, 'พนักงาน', 5000, 'user.png', 1, '2018-03-04 02:05:38', '2018-04-23 15:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `date`, `name`, `created_at`, `updated_at`) VALUES
(1, '2018-01-01', 'วันขึ้นปีใหม่', '2018-01-02 05:45:56', '2018-01-02 08:55:32'),
(3, '2018-01-02', 'วันหยุดชดเชยวันปีใหม่', '2018-01-02 06:48:54', '2018-01-02 06:48:54'),
(5, '2018-03-01', 'วันมาฆบูชา', '2018-01-02 08:21:26', '2018-03-02 18:12:21'),
(6, '2018-04-11', 'วันหยุด', '2018-04-23 00:54:01', '2018-04-24 16:29:42');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_leave` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num` double(8,1) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`id`, `employee_id`, `type_name`, `type_leave`, `num`, `start`, `end`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 78, 'ลาป่วย', '3', 1.0, '2018-03-30', '2018-03-30', 'ฟหกฟหกฟหกฟหกฟหก', 2, '2018-03-30 13:21:52', '2018-04-04 01:44:41'),
(2, 78, 'ลาป่วย', '3', 1.0, '2018-04-20', '2018-04-20', 'เป็นไข้', 2, '2018-04-22 02:53:04', '2018-04-22 02:53:21'),
(5, 78, 'ลากิจ', '3', 2.0, '2018-04-23', '2018-04-24', 'ไปธุระ', 2, '2018-04-22 09:17:40', '2018-04-22 09:18:32'),
(6, 78, 'ลากิจ', '3', 2.0, '2018-04-23', '2018-04-24', 'ไปธุระ', 1, '2018-04-22 09:19:33', '2018-04-22 09:19:33'),
(7, 78, 'ลาป่วย', '3', 10.0, '2018-04-09', '2018-04-18', 'aaaaa', 1, '2018-04-24 07:13:13', '2018-04-24 07:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE `leave_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_no` int(11) NOT NULL,
  `type_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`id`, `type_no`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ลาป่วย', '2018-01-02 17:00:00', '2018-01-02 19:02:50'),
(2, 2, 'ลากิจ', '2018-01-02 17:00:00', '2018-01-03 16:42:29'),
(3, 3, 'ลาพักผ่อน', '2018-01-02 17:00:00', '2018-01-02 19:01:52'),
(5, 4, 'ลาคลอดบุตร', '2018-01-02 18:55:35', '2018-01-02 19:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(23, '2014_10_12_000000_create_users_table', 1),
(24, '2014_10_12_100000_create_password_resets_table', 1),
(25, '2017_11_16_143519_add_username_in_users', 1),
(27, '2017_11_18_141907_create_employees_table', 2),
(28, '2017_11_20_170523_create_positions_table', 3),
(30, '2017_11_21_123732_create_education_table', 4),
(31, '2017_11_21_140253_add_surname_in_employee', 5),
(32, '2017_11_21_140958_rename_employee_code_in_education', 6),
(34, '2017_11_22_114343_create_clocktimes_table', 7),
(35, '2017_11_23_171531_add_clock_status_in_employees', 8),
(38, '2017_11_23_174504_add_clock_status_in_clocktime', 9),
(40, '2017_11_23_171803_dorp_clock_status_in_clocktimes', 10),
(41, '2017_11_23_232517_drop_table_clocktimes', 10),
(42, '2017_11_23_232632_create_table_clocktimes', 11),
(44, '2017_12_17_160331_create_conducts_table', 12),
(45, '2017_12_21_145031_create_works_table', 13),
(46, '2018_01_02_105254_create_holidays_table', 14),
(47, '2018_01_02_204933_create_times_table', 15),
(48, '2018_01_03_012504_create_leave_types_table', 16),
(52, '2018_01_03_140817_create_leaves_table', 17),
(53, '2018_01_07_233948_create_roles_table', 18),
(55, '2018_01_07_234240_create_user_role_table', 19),
(56, '2018_01_11_120515_add_image_user', 20),
(60, '2018_02_12_144544_create_events_table', 21),
(62, '2018_02_14_224228_create_salaries_table', 22),
(67, '2018_03_10_175111_create_overtimes_table', 23),
(68, '2018_03_10_180324_create_overtime_details_table', 23),
(71, '2018_03_12_134015_add_leave_and_num_to_leaves', 24);

-- --------------------------------------------------------

--
-- Table structure for table `overtimes`
--

CREATE TABLE `overtimes` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `overtimes`
--

INSERT INTO `overtimes` (`id`, `date`, `time`, `created_at`, `updated_at`) VALUES
(8, '2018-03-12', '00:30:00', '2018-03-11 07:46:48', '2018-03-13 06:13:23'),
(9, '2018-03-09', '01:00:00', '2018-03-13 04:36:59', '2018-03-13 04:36:59'),
(10, '2018-03-08', '00:30:00', '2018-03-13 05:10:53', '2018-03-13 05:10:53'),
(11, '2018-03-07', '02:00:00', '2018-03-13 05:53:44', '2018-03-13 05:53:44'),
(12, '2018-04-21', '03:00:00', '2018-04-23 00:40:52', '2018-04-23 00:43:59');

-- --------------------------------------------------------

--
-- Table structure for table `overtime_details`
--

CREATE TABLE `overtime_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `overtimes_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `overtime_details`
--

INSERT INTO `overtime_details` (`id`, `overtimes_id`, `employee_id`, `created_at`, `updated_at`) VALUES
(26, 9, 78, '2018-03-13 04:36:59', '2018-03-13 04:36:59'),
(27, 9, 79, '2018-03-13 04:36:59', '2018-03-13 04:36:59'),
(28, 9, 80, '2018-03-13 04:36:59', '2018-03-13 04:36:59'),
(29, 10, 78, '2018-03-13 05:10:53', '2018-03-13 05:10:53'),
(30, 10, 79, '2018-03-13 05:10:53', '2018-03-13 05:10:53'),
(31, 11, 85, '2018-03-13 05:53:44', '2018-03-13 05:53:44'),
(35, 8, 78, '2018-03-13 06:13:23', '2018-03-13 06:13:23'),
(36, 8, 79, '2018-03-13 06:13:23', '2018-03-13 06:13:23'),
(37, 8, 85, '2018-03-13 06:13:23', '2018-03-13 06:13:23'),
(44, 12, 78, '2018-04-23 00:44:00', '2018-04-23 00:44:00'),
(45, 12, 79, '2018-04-23 00:44:00', '2018-04-23 00:44:00'),
(46, 12, 80, '2018-04-23 00:44:00', '2018-04-23 00:44:00'),
(47, 12, 84, '2018-04-23 00:44:00', '2018-04-23 00:44:00'),
(48, 12, 85, '2018-04-23 00:44:00', '2018-04-23 00:44:00'),
(49, 12, 86, '2018-04-23 00:44:00', '2018-04-23 00:44:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `position_no` int(11) NOT NULL,
  `position_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `position_no`, `position_name`, `created_at`, `updated_at`) VALUES
(45, 4, 'ผู้จัดการ', '2017-11-21 16:26:08', '2018-03-23 08:29:37'),
(46, 5, 'รองผู้จัดการ', '2017-11-21 16:28:24', '2018-03-23 08:29:37'),
(47, 6, 'พนักงาน', '2018-04-24 02:54:43', '2018-04-24 02:54:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'User', 'ผู้ใช้งาน', '2018-01-07 17:24:22', '2018-01-07 17:24:22'),
(2, 'Admin', 'ผู้ดูแลระบบ', '2018-01-07 17:24:22', '2018-01-07 17:24:22'),
(3, 'Manager', 'ผู้บริหาร', '2018-01-07 17:24:22', '2018-01-07 17:24:22');

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `salary` double(8,2) NOT NULL,
  `overtime` double(8,2) DEFAULT NULL,
  `ar` double(8,2) DEFAULT NULL,
  `commission` double(8,2) DEFAULT NULL,
  `insurance` double(8,2) DEFAULT NULL,
  `other` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_other` double(8,2) DEFAULT NULL,
  `week` tinyint(4) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salaries`
--

INSERT INTO `salaries` (`id`, `employee_id`, `salary`, `overtime`, `ar`, `commission`, `insurance`, `other`, `amount_other`, `week`, `date`, `created_at`, `updated_at`) VALUES
(2, 78, 13.00, 123.00, 123.00, 123.00, 432.00, 'wettrt', 234.00, 1, '2018-01-01', '2018-02-25 16:05:47', '2018-02-25 16:05:47'),
(3, 78, 132.00, 1.00, 1.00, 1.00, 5.00, 'asdd', 5.00, 1, '2018-02-01', '2018-02-25 16:06:25', '2018-03-13 03:14:16'),
(4, 79, 132.00, 234.00, 123.00, 54.00, 234.00, 'qwer', 2344.00, 2, '2018-01-16', '2018-02-25 16:38:05', '2018-02-25 16:38:05'),
(5, 78, 132.00, 12.00, 12.00, 12.00, 12.00, 'qweqwe', 12.00, 2, '2018-01-16', '2018-03-02 16:38:34', '2018-03-02 16:38:34'),
(6, 79, 13.00, 12.00, 11.00, 12.00, 1212.00, 'asdda', 12.00, 1, '2018-02-01', '2018-03-13 02:19:31', '2018-03-13 02:19:31'),
(7, 79, 13.00, 121.00, 121.00, 121.00, 121.00, '1212', 12.00, 2, '2018-02-16', '2018-03-13 02:57:53', '2018-03-13 02:57:53'),
(9, 78, 10001.00, 187.50, 122.00, 122.00, 250.03, '1222', 12.00, 1, '2018-03-01', '2018-03-13 04:01:23', '2018-03-25 11:43:25'),
(11, 79, 13.00, 0.00, 123.00, 123.00, 123.00, '123', 123.00, 1, '2018-03-01', '2018-03-16 12:30:17', '2018-03-16 12:30:17'),
(12, 78, 7500.00, 0.00, 0.00, 0.00, 0.00, '0', 0.00, 2, '2018-03-16', '2018-03-18 08:18:46', '2018-03-18 08:18:46'),
(13, 79, 13.00, 0.00, NULL, NULL, NULL, NULL, NULL, 2, '2018-03-16', '2018-03-18 08:32:35', '2018-03-18 08:32:35'),
(14, 84, 4000.00, 0.00, NULL, NULL, 200.00, NULL, NULL, 1, '2018-03-01', '2018-03-18 08:33:06', '2018-04-22 18:11:13'),
(15, 86, 500.00, NULL, 0.00, 0.00, 0.00, '0', 0.00, 2, '2018-03-16', '2018-03-18 08:41:04', '2018-03-18 09:15:14'),
(16, 84, 123.00, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2018-03-16', '2018-03-18 08:43:37', '2018-03-18 08:43:37'),
(17, 84, 123.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-01', '2018-03-18 08:55:08', '2018-03-18 08:55:08'),
(18, 79, 13.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-01', '2018-03-18 08:57:08', '2018-03-18 08:57:08'),
(19, 86, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2018-01-16', '2018-03-18 09:02:37', '2018-03-18 09:02:37'),
(20, 84, 123.00, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2018-01-16', '2018-03-18 09:03:00', '2018-03-18 09:03:00'),
(21, 86, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-01', '2018-03-18 09:04:04', '2018-03-18 09:04:04'),
(23, 86, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-03-01', '2018-03-18 09:07:58', '2018-03-18 09:07:58'),
(25, 84, 123.00, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2018-02-16', '2018-03-18 09:08:50', '2018-03-18 09:08:50'),
(27, 86, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2018-02-16', '2018-03-18 09:11:05', '2018-03-18 09:11:05'),
(28, 86, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-02-01', '2018-03-18 09:12:04', '2018-03-18 09:12:04'),
(29, 84, 123.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-02-01', '2018-03-18 09:13:12', '2018-03-18 09:13:12'),
(31, 80, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-03-01', '2018-03-18 09:16:49', '2018-03-18 09:16:49'),
(32, 80, 7500.00, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2018-03-16', '2018-03-23 08:23:52', '2018-03-23 08:23:52'),
(33, 80, 7500.00, 0.00, NULL, NULL, 187.50, NULL, NULL, 1, '2018-02-01', '2018-03-25 15:41:54', '2018-03-25 15:41:54'),
(34, 80, 7500.00, 0.00, NULL, NULL, 187.50, NULL, NULL, 1, '2018-01-01', '2018-03-25 15:42:07', '2018-03-25 15:42:07'),
(35, 85, 300.00, 281.25, NULL, NULL, 15.00, NULL, NULL, 1, '2018-03-01', '2018-03-25 16:05:14', '2018-03-25 16:05:14'),
(36, 85, 300.00, 0.00, NULL, NULL, 0.00, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:22:22', '2018-04-22 03:22:22'),
(37, 85, 300.00, 0.00, NULL, NULL, 0.00, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:22:53', '2018-04-22 03:22:53'),
(38, 85, 300.00, 0.00, NULL, NULL, 0.00, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:23:11', '2018-04-22 03:23:11'),
(39, 86, 5000.00, 0.00, NULL, NULL, 125.00, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:23:48', '2018-04-22 03:23:48'),
(40, 85, 300.00, 0.00, NULL, NULL, 0.00, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:24:08', '2018-04-22 03:24:08'),
(41, 85, 300.00, 0.00, NULL, NULL, 0.00, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:24:51', '2018-04-22 03:24:51'),
(42, 78, 10001.00, 0.00, NULL, NULL, 250.03, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:25:00', '2018-04-22 03:25:00'),
(43, 78, 10001.00, 0.00, NULL, NULL, 250.03, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:25:15', '2018-04-22 03:25:15'),
(44, 79, 13.00, 0.00, NULL, NULL, 0.32, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:46:38', '2018-04-22 03:46:38'),
(45, 79, 13.00, 0.00, NULL, NULL, 0.32, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:46:51', '2018-04-22 03:46:51'),
(46, 80, 7500.00, 0.00, NULL, NULL, 187.50, NULL, NULL, 1, '2018-04-01', '2018-04-22 03:47:45', '2018-04-22 03:47:45'),
(47, 80, 7500.00, 0.00, NULL, NULL, 187.50, NULL, NULL, 2, '2018-04-16', '2018-04-22 03:51:42', '2018-04-22 03:51:42'),
(48, 85, 300.00, 0.00, NULL, NULL, 30.00, NULL, NULL, 2, '2018-04-16', '2018-04-22 03:51:55', '2018-04-22 03:53:42'),
(49, 78, 10001.00, 375.04, 500.00, NULL, 500.05, NULL, NULL, 2, '2018-04-16', '2018-04-23 00:46:05', '2018-04-23 00:46:05'),
(50, 80, 7500.00, 0.00, NULL, NULL, 400.00, NULL, NULL, 2, '2018-01-16', '2018-04-22 18:14:34', '2018-04-22 18:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE `times` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `times`
--

INSERT INTO `times` (`id`, `type`, `time`, `created_at`, `updated_at`) VALUES
(1, 'start', '12:00:00', '2018-01-01 17:00:00', '2018-03-23 05:13:21'),
(2, 'end', '13:00:00', '2018-01-01 17:00:00', '2018-03-23 08:26:36'),
(3, 'late', '08:15:00', '2018-01-02 17:00:00', '2018-02-15 11:08:44'),
(4, 'out', '18:00:00', '2018-01-02 17:00:00', '2018-03-16 11:43:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin', '$2y$10$rYvr4U8Y3W6rXtUWl1KMl.JjEKOjx4Zt.RQxeR9o0lFymGVAD70oS', '6Ef1Cpexels-photo-568236.jpeg', 'TElH24JGPtqJrjURq3pLFWIYDTqvx4RC9aPbSjyYbneIeYEucnc5tRnIrcLK', '2018-01-10 14:54:48', '2018-04-01 06:37:18'),
(2, 'Manager', 'manager@gmail.com', 'manager', '$2y$10$gMYYGnqRpnwVa.vcvvd.9ORB7ThkMDaj8mYhwHToxOfxzDHN3VwVC', 'user.png', 'LxtbaP7rbm15lY1xgQGaOQAoOh3uOpwMbZfN3nrRe5t3TaeMSdjOt2MGWGvJ', '2018-01-10 14:54:48', '2018-01-10 14:54:48'),
(4, 'แอดมิน', 'admin2@example.com', 'admin2', '$2y$10$41o0/O8y2HsvzgorQb3DouzyPhwtvqcND.yMyAeJ6Viqq2RCwkoJG', 'zjSzmdetailsquare3.jpg', 'fwI6pNXlmA7GWm81rZ6GOuKEFAZPAmFNP3NlsPyeRiKbrYOCvZXrs5aaedvw', '2018-01-10 15:46:23', '2018-03-23 08:32:58'),
(5, 'เอกวรพัทธ์', 'user1@gmail.com', '0002', '$2y$10$Mb/c7cPyY4q4WTPtfDyKEOR75eAN1EfSRTsc4G3pPrjaCBBaNIi.m', 'user.png', 'GNmGVDMqZc0oEJM3kfbR4J8Bywhs4gGdzzwWtfeiK9sfX1HJETvZoRNWjQdK', '2018-01-11 03:01:39', '2018-03-23 08:53:40'),
(6, 'มานี', 'user2@gmail.com', '0003', '$2y$10$CYBOkyQuWxyJigX.SAusQObFQ8aQaUfDS1k.SoaeuT1BXY2TKMO62', 'u87y6detailsquare3.jpg', 'bRNZEq8Zd4NU8fHQVhmVaF5IgZfhcV1oPDnyZEUsY01FZncTMgBoMl8gRslq', '2018-01-11 03:01:39', '2018-04-22 08:11:16'),
(7, 'ทดสอบ', 'user4@gmail.com', '0004', '$2y$10$jcShjqSeC6lWvl69nRAM6OdgMLG8PDqz6FMjeI8pc.fQv8ZC7RF2u', '9yhKVpexels-photo-568236.jpeg', 'JQcS1AiGl7pVQnlSBStZoblH6wXGS6xDoIxOt5JOrRmG1s2hRWr2GgokacsS', '2018-01-11 03:01:39', '2018-03-23 08:07:59'),
(17, 'คิวคิว', 'visitord@user.com', '0005', '$2y$10$ZVW/V5x7jR4fSXgjdgvAluS6UJpNDTFZkBbPbJa3UCvNciYztjhxW', '1bshEavatar5.png', '7Nn2yGwQzatvn90KC5KDoEXEtxCNbnXmLVfbmXEWSQBndEuvcbsPLK4H2CAI', '2018-01-11 06:16:37', '2018-03-23 08:12:21'),
(18, 'รายวัน', 'day@gmail.com', '0006', '$2y$10$mLIN2xIenOdCPJcrQM3X0uUkWzp2WSHwcfSgdGKsiH/VV6n/wE6wa', 'user.png', 'KIeHwA573HH4Hl1wNrNz9F6zhVUICF4OJdXNpo3sKxVR6uWezhS9LvLN2wbF', '2018-01-11 17:57:57', '2018-03-23 08:10:18'),
(19, 'อออ่าง', 'wefw@ggggg.cc', '0007', '$2y$10$IrQEZXwqZVVcd4X1Hq/pF.lQfV/lrslA1YAAQORCzZB32no.CcrUW', 'user.png', NULL, '2018-03-04 02:05:38', '2018-03-23 08:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, NULL, NULL),
(2, 2, 3, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 4, 2, NULL, NULL),
(5, 5, 1, NULL, NULL),
(6, 6, 1, NULL, NULL),
(7, 7, 1, NULL, NULL),
(8, 8, 1, NULL, NULL),
(9, 9, 2, NULL, NULL),
(10, 14, 3, NULL, NULL),
(11, 15, 2, NULL, NULL),
(12, 4, 2, NULL, NULL),
(13, 4, 2, NULL, NULL),
(14, 4, 2, NULL, NULL),
(15, 4, 2, NULL, NULL),
(16, 4, 2, NULL, NULL),
(17, 16, 1, NULL, NULL),
(18, 17, 1, NULL, NULL),
(19, 1, 2, NULL, NULL),
(20, 18, 1, NULL, NULL),
(21, 19, 1, NULL, NULL),
(22, 4, 2, NULL, NULL),
(23, 1, 2, NULL, NULL),
(24, 20, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `works`
--

CREATE TABLE `works` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `works`
--

INSERT INTO `works` (`id`, `employee_id`, `status`, `date`, `created_at`, `updated_at`) VALUES
(25, 78, 1, '2018-01-03', '2018-01-02 18:10:31', '2018-01-25 14:46:26'),
(28, 78, 1, '2018-01-04', '2018-01-03 17:00:03', '2018-01-05 08:13:43'),
(29, 79, 1, '2018-01-04', '2018-01-03 17:00:03', '2018-01-05 19:10:03'),
(30, 80, 0, '2018-01-04', '2018-01-03 17:00:03', '2018-01-04 05:17:37'),
(31, 78, 1, '2018-01-05', '2018-01-05 04:44:55', '2018-01-05 08:25:57'),
(32, 79, 1, '2018-01-05', '2018-01-05 04:44:55', '2018-01-25 14:46:26'),
(33, 80, 1, '2018-01-05', '2018-01-05 04:44:55', '2018-01-25 14:46:26'),
(40, 78, 1, '2018-01-06', '2018-01-06 05:59:27', '2018-01-25 14:46:26'),
(41, 79, 1, '2018-01-06', '2018-01-06 05:59:27', '2018-01-06 06:00:34'),
(42, 80, 1, '2018-01-06', '2018-01-06 05:59:27', '2018-01-06 06:00:43'),
(43, 78, 0, '2018-01-08', '2018-01-08 10:14:20', '2018-01-08 10:14:20'),
(44, 79, 0, '2018-01-08', '2018-01-08 10:14:20', '2018-01-08 10:14:20'),
(45, 80, 0, '2018-01-08', '2018-01-08 10:14:20', '2018-01-08 10:14:20'),
(46, 78, 0, '2018-01-10', '2018-01-10 14:08:42', '2018-01-10 14:08:42'),
(47, 79, 0, '2018-01-10', '2018-01-10 14:08:42', '2018-01-10 14:08:42'),
(48, 80, 0, '2018-01-10', '2018-01-10 14:08:42', '2018-01-10 14:08:42'),
(49, 78, 2, '2018-01-11', '2018-01-11 03:02:23', '2018-02-15 11:08:44'),
(50, 79, 1, '2018-01-11', '2018-01-11 03:02:23', '2018-01-25 14:44:00'),
(51, 80, 2, '2018-01-11', '2018-01-11 03:02:23', '2018-02-15 11:08:44'),
(52, 78, 0, '2018-01-22', '2018-01-22 12:12:41', '2018-01-22 12:12:41'),
(53, 79, 0, '2018-01-22', '2018-01-22 12:12:41', '2018-01-22 12:12:41'),
(54, 80, 0, '2018-01-22', '2018-01-22 12:12:41', '2018-01-22 12:12:41'),
(55, 85, 1, '2018-01-04', '2018-01-22 12:12:41', '2018-01-22 12:12:41'),
(56, 85, 1, '2018-01-03', '2018-01-22 12:12:41', '2018-01-22 12:12:41'),
(57, 78, 1, '2018-01-25', '2018-01-25 13:30:24', '2018-02-15 12:37:20'),
(58, 79, 1, '2018-01-25', '2018-01-25 13:30:24', '2018-02-15 12:37:45'),
(59, 80, 1, '2018-01-25', '2018-01-25 13:30:24', '2018-02-15 12:36:56'),
(60, 84, 0, '2018-01-25', '2018-01-25 13:30:24', '2018-01-25 13:30:24'),
(61, 85, 0, '2018-01-25', '2018-01-25 13:30:24', '2018-01-25 13:30:24'),
(77, 78, 0, '2018-02-15', '2018-02-15 11:11:41', '2018-02-15 11:11:41'),
(78, 79, 0, '2018-02-15', '2018-02-15 11:11:41', '2018-02-15 11:11:41'),
(79, 80, 0, '2018-02-15', '2018-02-15 11:11:41', '2018-02-15 11:11:41'),
(80, 84, 0, '2018-02-15', '2018-02-15 11:11:41', '2018-02-15 11:11:41'),
(81, 85, 0, '2018-02-15', '2018-02-15 11:11:41', '2018-02-15 11:11:41'),
(82, 78, 2, '2018-02-16', '2018-02-16 04:58:21', '2018-02-16 04:58:48'),
(83, 79, 2, '2018-02-16', '2018-02-16 04:58:21', '2018-02-16 04:58:56'),
(84, 80, 2, '2018-02-16', '2018-02-16 04:58:21', '2018-02-16 04:58:59'),
(85, 84, 2, '2018-02-16', '2018-02-16 04:58:21', '2018-02-16 04:59:02'),
(86, 85, 1, '2018-02-16', '2018-02-16 04:58:21', '2018-02-16 10:42:57'),
(97, 78, 0, '2018-02-24', '2018-02-24 15:05:34', '2018-02-24 15:05:34'),
(98, 79, 0, '2018-02-24', '2018-02-24 15:05:35', '2018-02-24 15:05:35'),
(99, 80, 0, '2018-02-24', '2018-02-24 15:05:35', '2018-02-24 15:05:35'),
(100, 84, 0, '2018-02-24', '2018-02-24 15:05:35', '2018-02-24 15:05:35'),
(101, 85, 0, '2018-02-24', '2018-02-24 15:05:35', '2018-02-24 15:05:35'),
(122, 78, 0, '2018-03-03', '2018-03-02 18:10:51', '2018-03-02 18:10:51'),
(123, 79, 0, '2018-03-03', '2018-03-02 18:10:51', '2018-03-02 18:10:51'),
(124, 80, 0, '2018-03-03', '2018-03-02 18:10:51', '2018-03-02 18:10:51'),
(125, 84, 0, '2018-03-03', '2018-03-02 18:10:52', '2018-03-02 18:10:52'),
(126, 85, 1, '2018-03-07', '2018-03-02 18:10:52', '2018-03-02 18:10:52'),
(130, 79, 1, '2018-02-01', NULL, NULL),
(131, 79, 1, '2018-02-02', NULL, NULL),
(132, 79, 0, '2018-02-03', NULL, NULL),
(133, 79, 1, '2018-02-05', NULL, NULL),
(134, 79, 1, '2018-02-06', NULL, NULL),
(135, 79, 1, '2018-02-07', NULL, NULL),
(136, 79, 0, '2018-02-08', NULL, NULL),
(137, 79, 1, '2018-02-09', NULL, NULL),
(138, 79, 1, '2018-02-10', NULL, NULL),
(139, 79, 1, '2018-02-12', NULL, NULL),
(140, 79, 1, '2018-02-13', NULL, NULL),
(141, 79, 1, '2018-02-14', NULL, NULL),
(142, 79, 1, '2018-02-15', NULL, NULL),
(162, 78, 1, '2018-03-12', '2018-03-12 09:42:44', '2018-03-12 09:42:44'),
(163, 79, 1, '2018-03-12', '2018-03-12 09:42:44', '2018-03-12 09:42:44'),
(164, 80, 2, '2018-03-12', '2018-03-12 09:42:44', '2018-03-12 16:43:07'),
(165, 84, 2, '2018-03-12', '2018-03-12 09:42:44', '2018-03-12 16:43:12'),
(166, 85, 1, '2018-03-12', '2018-03-12 09:42:44', '2018-03-12 16:43:24'),
(167, 86, 2, '2018-03-12', '2018-03-12 09:42:44', '2018-03-12 16:43:18'),
(170, 78, 2, '2018-03-16', '2018-03-16 11:28:07', '2018-03-16 11:38:29'),
(172, 80, 0, '2018-03-16', '2018-03-16 11:28:07', '2018-03-16 11:28:07'),
(173, 84, 0, '2018-03-16', '2018-03-16 11:28:07', '2018-03-16 11:28:07'),
(174, 85, 0, '2018-03-16', '2018-03-16 11:28:07', '2018-03-16 11:28:07'),
(175, 86, 0, '2018-03-16', '2018-03-16 11:28:07', '2018-03-16 11:28:07'),
(176, 78, 0, '2018-03-20', '2018-03-20 08:07:49', '2018-03-20 08:07:49'),
(177, 79, 0, '2018-03-20', '2018-03-20 08:07:49', '2018-03-20 08:07:49'),
(180, 85, 0, '2018-03-20', '2018-03-20 08:07:49', '2018-03-20 08:07:49'),
(188, 78, 0, '2018-03-26', '2018-03-25 19:33:18', '2018-03-25 19:33:18'),
(189, 79, 0, '2018-03-26', '2018-03-25 19:33:19', '2018-03-25 19:33:19'),
(190, 80, 0, '2018-03-26', '2018-03-25 19:33:19', '2018-03-25 19:33:19'),
(191, 84, 0, '2018-03-26', '2018-03-25 19:33:19', '2018-03-25 19:33:19'),
(192, 85, 0, '2018-03-26', '2018-03-25 19:33:19', '2018-03-25 19:33:19'),
(193, 86, 0, '2018-03-26', '2018-03-25 19:33:19', '2018-03-25 19:33:19'),
(194, 78, 3, '2018-03-30', '2018-03-30 13:21:52', '2018-04-04 01:44:40'),
(201, 78, 1, '2018-04-02', NULL, NULL),
(202, 78, 1, '2018-04-16', NULL, NULL),
(203, 79, 1, '2018-04-16', NULL, NULL),
(204, 80, 1, '2018-04-16', NULL, NULL),
(205, 84, 1, '2018-04-16', NULL, NULL),
(206, 85, 1, '2018-04-16', NULL, NULL),
(207, 86, 1, '2018-04-16', NULL, NULL),
(208, 78, 2, '2018-04-17', NULL, NULL),
(209, 79, 1, '2018-04-17', NULL, NULL),
(210, 80, 2, '2018-04-17', NULL, NULL),
(211, 84, 1, '2018-04-17', NULL, NULL),
(212, 85, 1, '2018-04-17', NULL, NULL),
(213, 86, 1, '2018-04-17', NULL, NULL),
(214, 78, 1, '2018-04-18', NULL, NULL),
(215, 79, 1, '2018-04-18', NULL, NULL),
(216, 80, 1, '2018-04-18', NULL, NULL),
(217, 84, 1, '2018-04-18', NULL, NULL),
(218, 85, 1, '2018-04-18', NULL, NULL),
(219, 86, 1, '2018-04-18', NULL, NULL),
(220, 78, 1, '2018-04-19', NULL, NULL),
(221, 79, 2, '2018-04-19', NULL, NULL),
(222, 80, 2, '2018-04-19', NULL, NULL),
(223, 84, 1, '2018-04-19', NULL, NULL),
(224, 85, 0, '2018-04-19', NULL, NULL),
(225, 86, 0, '2018-04-19', NULL, NULL),
(226, 78, 3, '2018-04-20', '2018-04-22 02:53:04', '2018-04-22 02:53:04'),
(227, 0, 0, '0000-00-00', NULL, NULL),
(228, 79, 1, '2018-04-20', NULL, NULL),
(229, 80, 1, '2018-04-20', NULL, NULL),
(230, 84, 1, '2018-04-20', NULL, NULL),
(231, 85, 1, '2018-04-20', NULL, NULL),
(232, 86, 2, '2018-04-20', NULL, NULL),
(233, 78, 1, '2018-04-21', NULL, NULL),
(234, 79, 1, '2018-04-21', NULL, NULL),
(235, 80, 1, '2018-04-21', NULL, '2018-04-22 03:19:44'),
(236, 84, 2, '2018-04-21', NULL, NULL),
(237, 85, 0, '2018-04-21', NULL, NULL),
(238, 86, 1, '2018-04-21', NULL, '2018-04-22 03:18:37'),
(241, 78, 1, '2018-04-23', '2018-04-22 09:17:41', '2018-04-23 00:24:35'),
(242, 78, 3, '2018-04-23', '2018-04-22 09:19:33', '2018-04-22 09:19:33'),
(243, 78, 2, '2018-04-24', '2018-04-24 02:54:02', '2018-04-24 02:55:33'),
(244, 79, 2, '2018-04-24', '2018-04-24 02:54:02', '2018-04-24 02:55:36'),
(245, 80, 2, '2018-04-24', '2018-04-24 02:54:03', '2018-04-24 02:55:39'),
(246, 84, 2, '2018-04-24', '2018-04-24 02:54:03', '2018-04-24 02:55:42'),
(247, 85, 2, '2018-04-24', '2018-04-24 02:54:03', '2018-04-24 02:54:08'),
(248, 86, 0, '2018-04-24', '2018-04-24 02:54:03', '2018-04-24 02:54:03'),
(249, 78, 3, '2018-04-09', '2018-04-24 07:13:13', '2018-04-24 07:13:13'),
(250, 78, 0, '2018-04-25', '2018-04-25 10:13:53', '2018-04-25 10:13:53'),
(251, 79, 0, '2018-04-25', '2018-04-25 10:13:53', '2018-04-25 10:13:53'),
(252, 80, 0, '2018-04-25', '2018-04-25 10:13:53', '2018-04-25 10:13:53'),
(253, 84, 0, '2018-04-25', '2018-04-25 10:13:53', '2018-04-25 10:13:53'),
(254, 85, 0, '2018-04-25', '2018-04-25 10:13:53', '2018-04-25 10:13:53'),
(255, 86, 0, '2018-04-25', '2018-04-25 10:13:53', '2018-04-25 10:13:53'),
(256, 78, 0, '2018-04-27', '2018-04-27 06:54:35', '2018-04-27 06:54:35'),
(257, 79, 0, '2018-04-27', '2018-04-27 06:54:35', '2018-04-27 06:54:35'),
(258, 80, 0, '2018-04-27', '2018-04-27 06:54:35', '2018-04-27 06:54:35'),
(259, 84, 0, '2018-04-27', '2018-04-27 06:54:35', '2018-04-27 06:54:35'),
(260, 85, 0, '2018-04-27', '2018-04-27 06:54:35', '2018-04-27 06:54:35'),
(261, 86, 0, '2018-04-27', '2018-04-27 06:54:35', '2018-04-27 06:54:35'),
(262, 78, 0, '2018-06-05', '2018-06-05 09:50:23', '2018-06-05 09:50:23'),
(263, 79, 0, '2018-06-05', '2018-06-05 09:50:23', '2018-06-05 09:50:23'),
(264, 80, 0, '2018-06-05', '2018-06-05 09:50:23', '2018-06-05 09:50:23'),
(265, 84, 0, '2018-06-05', '2018-06-05 09:50:23', '2018-06-05 09:50:23'),
(266, 85, 0, '2018-06-05', '2018-06-05 09:50:23', '2018-06-05 09:50:23'),
(267, 86, 0, '2018-06-05', '2018-06-05 09:50:23', '2018-06-05 09:50:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clocktimes`
--
ALTER TABLE `clocktimes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conducts`
--
ALTER TABLE `conducts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtimes`
--
ALTER TABLE `overtimes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overtime_details`
--
ALTER TABLE `overtime_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `works`
--
ALTER TABLE `works`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clocktimes`
--
ALTER TABLE `clocktimes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `conducts`
--
ALTER TABLE `conducts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `leave_types`
--
ALTER TABLE `leave_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `overtimes`
--
ALTER TABLE `overtimes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `overtime_details`
--
ALTER TABLE `overtime_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `works`
--
ALTER TABLE `works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
