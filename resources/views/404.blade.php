@extends('layouts.app') 
@section('title', '404 Page') 
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'404 Page'])
        <!-- Main content -->
        <section class="content">
            <div class="error-page">
              <h2 class="headline text-yellow"> 404 </h2>
      
              <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
      
                <p class="font" style="font-size:50px">
                    คุณไม่สิทธิ์ในการใช้งานหน้านี้ 
                </p>
      
                <button onclick="goBack()" class="btn btn-warning btn-lg"><span class="ion ion-arrow-left-a"></span> Go Back </button>
              </div>
              <!-- /.error-content -->
            </div>
            <!-- /.error-page -->
          </section>
          <!-- /.content -->
    </div>
@endsection 
@push('scripts')
<script>
  function goBack() { window.history.back() }
</script>
@endpush