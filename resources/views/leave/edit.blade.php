<!-- Modal -->
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top:10%">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <i class="fa fa-file-text-o fa-2x"></i>
        <span class="modal-title font" style="font-size:22px;margin-left:10px"> </span>
      </div>
      <div class="modal-body">
        {!! Form::open(['url' => ['/leave', "leave"], 'class'=> 'form-horizontal', 'method'=>'PUT']) !!}
        {!! Form::hidden('id', NULL, ['id'=>'leave']) !!}
          <div class="row">
            <div class="form-group">
              {!! Form::label('type', 'ประเภทการลา', ['class'=>'label_font col-sm-3 text-right']) !!}
              <div class="col-sm-7">
                {!! Form::select('type_name', $leavetype , NULL, ['class'=>'form-control', 'required', 'id'=>'type_name']) !!}
              </div>
            </div>
             
            <div class="form-group">
                {!! Form::label('type', 'ชนิดการลา', ['class'=>'label_font col-sm-3 text-right']) !!}
                <div class="col-sm-7">
                  <div class="radio">
                    {{--  <label style="margin-right:5px"> 
                        {!! Form::radio('type_leave', 1, NULL, ['class'=> 'radio-half', 'id'=>'type_leave_1']) !!} ครึ่งเช้า 
                    </label>
                    <label style="margin-right:5px"> 
                        {!! Form::radio('type_leave', 2, NULL , ['class'=> 'radio-half', 'id'=>'type_leave_2']) !!} ครึ่งบ่าย
                     </label>  --}}
                     <label style="margin-right:5px"> 
                        {!! Form::radio('type_leave', 3, NULL, ['class'=> 'radio-full' ,'id'=>'type_leave_3']) !!} เต็มวัน
                   </label>
                  </div>
                </div>
              </div>

              <div class="half" style="display:none">
                  <div class="form-group">
                    {!! Form::label('type', 'วันที่', ['class'=>'label_font col-sm-3 text-right']) !!}
                    <div class="col-sm-7">
                      {!! Form::date('start', NULL , ['class'=>'form-control c_clock', 'required']) !!}
                      {!! Form::hidden('end', date('Y-m-d')) !!}
                    </div>
                  </div> 
                </div> 
    
                <div class="full">
                  <div class="form-group">
                    {!! Form::label('type', 'ช่วงเวลา', ['class'=>'label_font col-sm-3 text-right']) !!}
                    <div class="col-sm-3">
                      {!! Form::date('start', NULL , ['class'=>'form-control c_clock', 'required' , 'id'=>'start']) !!}
                      {!! Form::date('start', NULL , ['class'=>'form-control c_clock_b', 'required']) !!}
                    </div>
                    {!! Form::label('type', 'ถึง', ['class'=>'label_font col-sm-1 text-center']) !!}
                    <div class="col-sm-3">
                      {!! Form::date('end', NULL , ['class'=>'form-control c_clock',  'required', 'id'=>'end']) !!}
                      {!! Form::date('end', NULL , ['class'=>'form-control c_clock_b',  'required']) !!}
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  {!! Form::label('detail', 'รายละเอียด', ['class'=>'label_font col-sm-3 text-right']) !!}
                  <div class="col-sm-7">
                    {!! Form::textarea('detail', NULL, ['class'=>'form-control', 'rows'=> 3, 'required' , 'id'=>'detail_edit']) !!}
                  </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
