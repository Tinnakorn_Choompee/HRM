<!-- Modal Show-->
<div id="show" class="modal fade" role="dialog">
	<div class="modal-dialog" style="margin-top:10%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<i class="fa fa-file-text-o fa-2x"></i>
				<span class="modal-title font" style="font-size:25px;margin-left:10px"></span>
				<span class="modal-title font" id="status" style="font-size:25px;margin-right:30px;float:right;"></span>
			</div>
            <hr>
			<div class="modal-body">
				<dl class="font" style="font-size:20px">
					<dt>ประเภทการลา</dt>
					<dd id="type"></dd>
					<br>
					<div class="row">
						<div class="col-md-6">
							<dt>วันที่เริ่ม</dt>
							<dd class="message" id="start"></dd>
						</div>
						<div class="col-md-6">
							<dt>วันที่สิ้นสุด</dt>
							<dd class="message" id="end"></dd>
						</div>
					</div>
					<br>
					<dt>รวมระยะเวลา</dt>
					<dd class="message" id="num"></dd>
					<br>
					<dt>รายละเอียด</dt>
					<dd class="message" id="detail"></dd>
				</dl>
			</div>
			<div class="modal-footer">
				@if(Auth::user()->hasRole('Manager'))
				{!! Form::open(['url' => 'leave/status', 'class'=>'form-inline']) !!}
					{!! Form::hidden('status', 2) !!}
					{!! Form::hidden('id', NULL,['class'=>'id']) !!}
					{!! Form::submit('อนุมัติ',['class'=> 'btn btn-success','style'=> 'float:left']); !!}
				{!! Form::close() !!}

				{!! Form::open(['url' => 'leave/status', 'class'=>'form-inline']) !!}
					{!! Form::hidden('status', 0) !!}
					{!! Form::hidden('id', NULL, ['class'=>'id']) !!}
					{!! Form::submit('ไม่อนุมัติ',['class'=> 'btn btn-danger','style'=> 'float:left']); !!}
				{!! Form::close() !!}
				@endif		
				<button type="button" class="btn btn-default" style="margin-top:10px" data-dismiss="modal">Close</button>
			</div>
			<br>
		</div>
    </div>
</div>