@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Libraries\ClockLibrary\ClockLibrary;
use Libraries\LeaveLibrary\LeaveLibrary;
@endphp
@extends('layouts.app')
@section('title', 'ข้อมูลการลา')
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }}
    <style>
        #consider { animation-duration: 0.5s; }
    </style>
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'ข้อมูลการลา'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                  {{ Form::open(['url' => 'clock_all', 'method'=>'DELETE', 'id'=>'clock_all']) }}
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >ตารางข้อมูลการลา</h2>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="report" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr>
                                        <th width="4%">#</th>
                                        <th width="20%">รายชื่อพนักงาน</th>
                                        <th width="10%">ประเภทการลา</th>
                                        <th width="7%">วันที่เริ่ม</th>
                                        <th width="7%">วันที่สิ้นสุด</th>
                                        <th width="10%">ชนิดการลา</th>
                                        {{--  <th width="10%">รวมระยะเวลา</th>  --}}
                                        <th width="10%">สถานะการลา</th>
                                        <th width="30%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:20px">
                                @foreach($leave as $k => $rs)
                                    <tr class="text-center">
                                        <td>{{ ++$k }}</td>
                                        <td class="text-left">{{ $data['prename'][$rs->employee->prename] }} {{ $rs->employee->name }} {{ $rs->employee->surname }}</td>
                                        <td>{{ $rs->type_name }}</td>
                                        <td>{!! DateThaiLibrary::ThaiDate($rs->start) !!}</td>
                                        <td>{!! DateThaiLibrary::ThaiDate($rs->end) !!}</td>
                                        <td>{!! LeaveLibrary::Type($rs->type_leave) !!}</td>
                                        {{--  <td>{{ $rs->num  }}</td>  --}}
                                        <td>
                                            @if($rs->status == 1)
                                            <p class="consider text-{!! $status[$rs->status]['color'] !!}"> {!! $status[$rs->status]['status'] !!} </p>
                                            @else
                                            <p class="text-{!! $status[$rs->status]['color'] !!}"> {!! $status[$rs->status]['status'] !!} </p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($rs->status == 2)        
                                                <span data-toggle="modal" data-target="#show" data-id="{{ $rs->id }}">
                                                    <button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="รายละเอียด"><i class="fa fa-eye"></i></button>
                                                </span>
                                            @else
                                                <span data-toggle="modal" data-target="#show" data-id="{{ $rs->id }}">
                                                    <button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="รายละเอียด"><i class="fa fa-eye"></i></button>
                                                </span>
                                                @if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('User'))
                                                <span data-toggle="modal" data-target="#edit" data-id="{{ $rs->id }}">
                                                    <button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></button>
                                                </span>
                                                <button class="btn btn-danger btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="4%">#</th>
                                        <th width="20%">รายชื่อพนักงาน</th>
                                        <th width="10%">ประเภทการลา</th>
                                        <th width="7%">วันที่เริ่ม</th>
                                        <th width="7%">วันที่สิ้นสุด</th>
                                        <th width="10%">ชนิดการลา</th>
                                        {{--  <th width="10%">รวมระยะเวลา</th>  --}}
                                        <th width="10%">สถานะการลา</th>
                                        <th width="30%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                            @if(Auth::user()->hasRole('Manager'))
                            <p class="text-red font" style="font-size:20px;margin-top:25px"> * หมายเหตุ กรุณากด <i class="fa fa-eye text-green" style="margin:0px 5px 0px 5px"></i> เพื่อพิจารณาอนุมัติการลา </p>
                            @endif
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  {{ Form::close() }}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@include('leave.show')
@include('leave.edit', ['leavetype' => $leavetype])
@endsection
@push('scripts')
    <!-- flatpickr -->
    {{ Html::script('plugins/flatpickr/flatpickr.min.js') }}
    {{ Html::script('plugins/flatpickr/rtl/th.js') }}
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}

    @if (session('success'))
    <script>
        swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
        <script>
            swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('approve'))
        <script>
            swal("Approved !", "อนุมัติการลาเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('not'))
        <script>
            swal("Not Approved!", "ไม่อนุมัติการลา", "error");
        </script>
    @endif

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

     $( ".radio-half" ).click(function() {
        $( ".full" ).hide();
        $( ".half" ).show();
    });

     $( ".radio-full" ).click(function() {
        $( ".full" ).show();
        $( ".half" ).hide();
    });

    $('#create').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id     = button.data('id')
        var page   = "leave";
        var title  = "การขอลางาน";
        $(this).find('.modal-title').html(title)
        $(this).find('.modal-body .id').val(id)
        $(this).find('.modal-body .page').val(page)
    });

   var date = new Date();
    const c_clock = flatpickr(".c_clock", {
        locale : 'th',
        defaultDate	: date,
        disable: [
            @foreach($holiday as $rs)
                '{{ $rs->date }}',
             @endforeach
            ,
            function(date) {
                return (date.getDay() === 0)
            },
            {
                from: "2000-01-01",
                to: date
            }
        ]
    });
    
    $('.consider').addClass('animated infinite pulse');

    $('#show').on('show.bs.modal', function(event) {
        var button   = $(event.relatedTarget)
        var id       = button.data('id')
        $.ajax({
            type: "GET",
            url: "/leave/"+id,
            success: function(rs){
                $('.modal-title').html(rs.employee.prename + " " + rs.employee.name + " " + rs.employee.surname)
                switch(rs.status) {
                    case "ไม่อนุมัติ" :     var color = "text-red";    break;
                    case "รอการพิจารณา" : var color = "text-yellow"; break;
                    case "อนุมัติเรียบร้อย" : var color = "text-green";  break;
                }
                $('#status').html(rs.status).addClass(color);
                $('#type').html(rs.type_name)
                $('#type_leave').html(rs.type_  )
                $('#start').html(rs.start)
                $('#end').html(rs.end)
                $('#num').html(rs.num+" วัน")
                $('#detail').html(rs.detail)
                $('.id').val(rs.id)
			}
		});
    });

    $('#edit').on('show.bs.modal', function(event) {
        var button   = $(event.relatedTarget)
        var id       = button.data('id')
        $.ajax({
            type: "GET",
            url: "/leave/"+id+"/edit",
            success: function(rs){
                $('.modal-title').html(rs.employee.prename + " " + rs.employee.name + " " + rs.employee.surname)
                $('#leave').val(rs.id)
                switch(rs.type_leave) {
                    case  "1" :  $("#type_leave_1").attr('checked', 'checked'); $( "#type_leave_1" ).trigger( "click" ); $( ".full" ).hide(); $( ".half" ).show(); break; 
                    case  "2" :  $("#type_leave_2").attr('checked', 'checked'); $( "#type_leave_2" ).trigger( "click" ); $( ".full" ).hide(); $( ".half" ).show(); break; 
                    case  "3" :  $("#type_leave_3").attr('checked', 'checked'); $( "#type_leave_3" ).trigger( "click" ); $( ".full" ).show(); $( ".half" ).hide(); break; 
                }
                $('#type_name').val(rs.type_name)
                $.fn.Start(rs.start);
                $.fn.End(rs.end);
                $('#detail_edit').val(rs.detail)
			}
		});
    });

    var date = new Date();
    $.fn.Start = function(time){
        const start = flatpickr("#start", {
            locale : 'th',
            defaultDate : time,
            disable: [
                @foreach($holiday as $rs)
                    '{{ $rs->date }}',
                 @endforeach
                ,
                function(date) {
                    return (date.getDay() === 0)
                }
            ]
        });
    }
    $.fn.End = function(time){
        const end = flatpickr("#end", {
            locale : 'th',
            defaultDate : time,
            disable: [
                @foreach($holiday as $rs)
                    '{{ $rs->date }}',
                 @endforeach
                ,
                function(date) {
                    return (date.getDay() === 0)
                }
            ]
        });
    }

    $('.btn-del').on('click',function(e){
       e.preventDefault();
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ ข้อมูลการลา นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    url: "/leave/"+id,
                    success: function(msg){
                        console.log(msg);
                        swal("Success!", "ทำการลบข้อมูลการลาเรียบร้อยแล้ว", "success",{button:false});
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
				    }
			    });
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(function () {
            $("#example1").DataTable();
            $('#report').DataTable({ "dom": '<"right"f> rt <"right"p> <"clear">'});
    });

    </script>
@endpush
