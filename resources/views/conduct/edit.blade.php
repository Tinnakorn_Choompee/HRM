@extends('layouts.app')
@section('title', 'เพิ่มความประพฤติ')
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }}
    <!-- Select2 -->
    {{ Html::style('plugins/select2/select2.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'เพิ่มความประพฤติ'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        <h3 class="box-title font">ข้อมูลความประพฤติ</h3>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::model($conduct, ['url' =>  ['conduct', $conduct->id], 'method' => 'PUT']) !!}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('employee_id', 'รายชื่อพนักงาน', ['class'=>'label_font']) !!}
                                            {!! Form::select('employee_id', $employee , $conduct->employee_id, ['class'=>'form-control select2', 'required', 'style'=>'width:100%;','placeholder' => '-- เลือกพนักงาน --']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="radio">
                                                <label> {!! Form::radio('type', '1', true) !!} ความประพฤติดี </label>
                                            </div>
                                            <div class="radio">
                                                <label> {!! Form::radio('type', '2', false) !!} ความประพฤติไม่ดี </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('title', 'หัวข้อ', ['class'=>'label_font']) !!}
                                            {!! Form::text('title', old('title'), ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('detail', 'รายละเอียด', ['class'=>'label_font']) !!}
                                            {!! Form::textarea('detail', old('detail'), ['class'=>'form-control', 'rows' => 3, 'required']) !!}
                                        </div>
                                    </div>
                                </div>
								 <div class="row">
                                    <div class="col-md-offset-4 col-md-4 col-xs-12">
                                        <div class="form-group font" style="font-size:16px">
                                            {!! Form::label('date', 'วันที่', ['class'=>'label_font']) !!}
                                            {!! Form::date('date', old('date'), ['class'=>'form-control date']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary label_font"> บันทึก </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
            </div><!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
    <!-- flatpickr -->
    {{ Html::script('plugins/flatpickr/flatpickr.min.js') }}
    {{ Html::script('plugins/flatpickr/rtl/th.js') }}
    <!-- Select2 -->
    {{ Html::script('plugins/select2/select2.full.min.js') }}
   <script>
    const date = flatpickr(".date", { locale : 'th' });
    $(".select2").select2();
   </script>
@endpush
