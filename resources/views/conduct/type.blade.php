@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app')
@section('title', 'บันทึกความประพฤติ')
@section('styles')
    <!-- DataTables -->
    {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'บันทึกความประพฤติ'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >บันทึกความประพฤติ</h2>
                            <a href="/conduct/create" class="btn btn-lg btn btn-info btn_employee font" style="font-size:22px">
                                <i class="ion ion-ios-compose" style="margin-right:10px;"></i> เพิ่มความประพฤติ
                            </a>
                            <span class="font pull-right title" style="margin:15px 10px 10px 10px;"> รวม {{ $conduct->total() }} ครั้ง</span>
                        </div>
                        <div class="box-body">
                        <div class="row">
                         @foreach($conduct as $k => $rs)
                            <div class="col-md-4 col-xs-12">
                                @if($rs->type == 1)
                                    <div class="box box-success box-solid">
                                @else
                                    <div class="box box-danger box-solid">
                                @endif
                                <div class="box-header with-border">
                                    <i class="fa fa-text-width"></i>
                                    <p class="box-title font" style="font-size:21.5px !important;margin-top:10px"> {{ $rs->title }}</p>
                                </div>
                                    <!-- /.box-header -->
                                    <div class="box-body font" style="font-size:20px">
                                    <dl>
                                        <dt>พนักงาน</dt>
                                        <dd> {{ $data['prename'][$rs->employee->prename] }} {{ $rs->employee->name }} {{ $rs->employee->surname }} </dd>
                                        <dt>ประเภท</dt>
                                        <dd> {{ $data['type'][$rs->type] }} </dd>
                                        <dt>รายละเอียด</dt>
                                        <dd> {{ str_limit($rs->detail,20) }} </dd>
                                        <dt>จัดการ</dt>
                                        <dd>
                                          <span style="text-align:center;">
                                            <div class="row">
                                              <div class="col-md-4">
                                                  <button class="btn btn-block btn-primary btn-show" data-toggle="modal" data-target="#show" data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ดูข้อมูล"><i class="fa fa-eye"></i></button>
                                              </div>
                                              <div class="col-md-4">
                                                <a href="/conduct/{{$rs->id}}/edit" class="btn btn-block btn-warning btn-edit" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>
                                              </div>
                                              <div class="col-md-4">
                                                <button class="btn btn-block btn-danger btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                              </div>
                                            </div>
                                          </span>
                                        </dd>
                                    </dl>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- ./col -->
                         @endforeach
                        </div>
                        <!-- /.box-body -->
                        <center> {{ $conduct->links() }} </center>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@include('conduct.show')

@endsection
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
        <script>
            swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-del').on('click',function() {
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ ความประพฤติ นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    url: "/conduct/"+id,
                    success: function(msg){
                        console.log(msg);
                        swal("Success!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success",{button:false});
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
				    }
			    });
            }
        });
    });

    $('#show').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id     = button.data('id')
        $.ajax({
            type: "GET",
            url: "/conduct/"+id,
            success: function(rs){
            $('.modal-title').text(rs.title)
            $('#employee').text(rs.employee.prename + " " + rs.employee.name + " " + rs.employee.surname)
            $('#type').text(rs.type)
            $('#detail').html(rs.detail)
            $('#date').text(rs.date)
			}
		});
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
