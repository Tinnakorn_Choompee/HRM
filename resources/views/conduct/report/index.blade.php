@extends('layouts.app') @section('title', 'รายงานความประพฤติ') 
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	@include('layouts.Backend.breadcrumb', ['title'=>'รายงานความประพฤติ'])
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="box">
					<div class="box-header">
						<h2 class="box-title font" style="margin-top:19px;">รายงานความประพฤติ</h2>
						<span class="btn_employee" style="margin-top:-20px;">
							{!! Form::open(['url' => '/report/conduct', 'method'=> 'GET', 'id' => 'types']) !!}
							<div class="form-control">
							  {!! Form::select('type', ['ทั้งหมด', 'ดี', 'ไม่ดี'] , NULL, ['class'=>'form-control select_type', 'id'=> 'type', 'required']) !!}
							</div>
							{!! Form::close() !!}
						  </span>
						  <span class="pull-right" style="padding:25px 10px 0px 0px">
							ความประพฤติ 
						  </span>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- DONUT CHART -->
								<div class="box box-danger">
										<div class="box-header with-border">
											<h3 class="box-title font">กราฟความประพฤติ รายเดือน ปี {{ date('Y')+543 }}</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div style="margin:auto;width:95%;">
												<canvas id="ChartMonth" width="450" height="350"></canvas>
											</div>
										</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (LEFT) -->
							<div class="col-md-6">
								<!-- AREA CHART -->
								<div class="box box-primary">
										<div class="box-header with-border">
											<h3 class="box-title font">กราฟความประพฤติ</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div style="margin:auto;width:95%;">
												<canvas id="ChartConduct" width="450" height="350"></canvas>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
									<!-- /.box -->
							</div>
							<!-- /.col (RIGHT) -->
							<div class="col-md-12">
									<!-- AREA CHART -->
									<div class="box box-primary">
											<div class="box-header with-border">
												<h3 class="box-title font">กราฟความประพฤติของพนักงาน</h3>
												<div class="box-tools pull-right">
													<button type="button" class="btn btn-box-tool" data-widget="collapse">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</div>
											<div class="box-body">
												<div style="margin:auto;width:50%;">
													<canvas id="ChartEmployee" width="450" height="350"></canvas>
												</div>
											</div>
										<!-- /.box-body -->
									</div>
									<!-- /.box -->
								</div>
								<!-- /.col (RIGHT) -->
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
	</section>
	<!-- /.content -->
	</div>

	@endsection 
	@push('scripts')
	<!-- ChartJS -->
	{{ Html::script('plugins/chartjs/Chart.min.js') }}
	<script>
		$('.select_type').on('change',function(){
        	$( "#types" ).submit();
    	});
	</script>
	<script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});		
	</script>
	<!----------------------------------------------------------  ChartSetting  -------------------------------------------------------------->
	<script>
		// Global Options:
		Chart.defaults.global.defaultFontColor = 'black';
		Chart.defaults.global.defaultFontSize = 22;
		Chart.defaults.global.defaultFontFamily = 'DBHelvethaica';
	</script>
	<!----------------------------------------------------------  ChartConduct  -------------------------------------------------------------->
	<script>
		var data = {
			datasets: [{
				data: [
					'{{ $g }}', '{{ $b }}'
				],
				backgroundColor: [
					@foreach($color as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderColor: [
					@foreach($border as $rs)
						'{{ $rs }}',
					@endforeach
				],
				borderWidth: 1,
				label: 'My dataset'
				}],
			labels: ["ดี", "ไม่ดี"],
			id: [
				1, 2
			] 
		};
			var pieOptions = {
			responsive: true,
				legend: {
				position: 'bottom',
			},
			hover: {
				mode: 'label'
			},
			title: {
				display: true,
				text: 'ความประพฤติทั้งหมด {{ $total }} ครั้ง',
				fontSize : 22,
				fontFamily : 'DBHelvethaica',
				fontStyle : 'normal',
		}, 
		animation: {
			duration: 500,
			animateScale: true,
			animateRotate: true,
			easing: "easeOutQuart",
			}
		};
		
		var ctx = document.getElementById("ChartConduct").getContext("2d");
		var conduct = new Chart(ctx,  {
			type: 'doughnut', // or doughnut
			data: data,
			options: pieOptions
		});
		
		document.getElementById("ChartConduct").onclick = function(evt)
		{   
			var activePoints = conduct.getElementsAtEvent(evt);
			var modal = this;
			if(activePoints.length > 0)
			{
				var clickedElementindex = activePoints[0]["_index"];
				var id = conduct.data.id[clickedElementindex];
				window.location.href = "/conduct/report/"+id;
				{{--  window.open("/conduct/type/"+id);  --}}
			}
		}
	</script>
	<!----------------------------------------------------------  ChartMonth  ---------------------------------------------------------------->
	<script>
			var ctx = document.getElementById("ChartMonth");
			document.getElementById("ChartMonth").onclick = function(evt)
			{   
				var activePoints = month.getElementsAtEvent(evt);
				var modal = this;
				if(activePoints.length > 0)
				{
					var clickedElementindex = activePoints[0]["_index"];
					var id = month.data.id[clickedElementindex];
					window.location.href = "/conduct/month/"+id;
					{{--  window.open("/conduct/month/"+id);  --}}
				}
			}
			var month = new Chart(ctx, {
			  type: 'bar',
			  data: {
				labels: [
					@foreach($month as $rs)
						'{{ $rs }}',
					@endforeach
				],
				id : [
					@foreach($month as $k => $rs)
						'{{ $k }}',
					@endforeach
				],
				datasets: [{
					type: "bar",
					label: 'ดี',
					data: [
						@foreach($good as $rs)
							{{ $rs }},
						@endforeach
					],
					backgroundColor: [
						@foreach($good as $rs)
							'rgba(54, 162, 235, 0.2)', 
						@endforeach
					],
					borderColor: [
						@foreach($good as $rs)
					   		'rgba(54, 162, 235, 1)',
					    @endforeach
					],
					borderWidth: 1,
				  },
				  {
					type : "bar",
					label: 'ไม่ดี',
					data: [
						@foreach($bad as $rs)
							{{ $rs }},
						@endforeach
					],
					backgroundColor: [
						@foreach($bad as $rs)
							'rgba(255, 99, 132, 0.2)',
						@endforeach
					],
					borderColor: [
						@foreach($bad as $rs)
							'rgba(255, 99, 132, 1)', 
						@endforeach
					],
					borderWidth: 1
				  }
				]
			  },
			  options: {
				animation: {
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.textAlign = "center";
                        ctx.textBaseline = "middle";
                        var chart    = this;
                        var datasets = this.config.data.datasets;
                        datasets.forEach(function (dataset, i) {
                            ctx.font = "22px DBHelvethaica";
                            switch (dataset.type) {
                                case "lines":
                                    ctx.fillStyle = "Black";
                                    chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                        ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
                                    });
                                    break;
                                case "bars":
                                    ctx.fillStyle = "Black";
                                    chart.getDatasetMeta(i).data.forEach(function (p, j) {
										if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
                                    });
                                    break;
                            }
                        });
                    }
                },
				scales: {
				  responsive: true,
				  yAxes: [{
					stacked: true,
					ticks: {
					  	beginAtZero: true,
					  	steps: 10,
                        stepValue: 5,
                        max : {{ max($good)+max($bad)+2 }}
					}
				  }],
				  xAxes: [{
					stacked: true,
					ticks: {
					  beginAtZero: true,
					  fontSize : 22,
					  fontFamily : 'DBHelvethaica',
					  fontStyle : 'normal',
					}
				  }]
				}
			  }
			});
	</script>
	<!----------------------------------------------------------  ChartEmployee  -------------------------------------------------------------->
	<script>
			var canvas = document.getElementById("ChartEmployee");
			var ctx = canvas.getContext('2d');
			var data = {
			  labels: [
				@foreach($employee as $rs)
					"{{ $data['prename'][$rs->prename].' '.$rs->name.' '.$rs->surname}}",
				@endforeach
			  ],
			  id : [
				@foreach($employee as $rs)
					"{{ $rs->id }}",
				@endforeach
			  ],
			  datasets: [{
				  label: "ดี",
				  type : "horizontalBar",
				  backgroundColor: 'rgba(54, 162, 235, 0.2)',
				  borderColor: 'rgba(54, 162, 235, 1)',
				  borderWidth: 1,
				  data: [
					@foreach($employee_good as $rs)
						{{ $rs }},
					@endforeach
				  	],
				  conduct : [
					  @foreach($employee as $rs)
					  	"{{ 1 }}",
					  @endforeach
				  	]
				}
				,
				{
				  label: "ไม่ดี",
				  type : "horizontalBar",
				  backgroundColor: 'rgba(255, 99, 132, 0.2)',
				  borderColor: 'rgba(255, 99, 132, 1)',
				  borderWidth: 1,
				  data: [
					@foreach($employee_bad as $rs)
						{{ $rs }},
					@endforeach
				  ],
				}
			  ]
			};
			var options = {
				animation: {
					onComplete: function () {
						var ctx = this.chart.ctx;
						ctx.textAlign = "center";
						ctx.textBaseline = "middle";
						var chart = this;
						var datasets = this.config.data.datasets;
						datasets.forEach(function (dataset, i) {
							ctx.font = "15px Arial";
							switch (dataset.type) {
								case "horizontalBars":
									ctx.fillStyle = "Black";
									chart.getDatasetMeta(i).data.forEach(function (p, j) {
										if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x + 20, p._model.y); }
									});
									break;
							}
						});
					}
				},
			  scales: {
						xAxes: [{
							ticks: {
								beginAtZero:true,
								steps: 10,
								stepValue: 5,
								max : {{  max($employee_good) > max($employee_bad) ? max($employee_good) + 2 : max($employee_bad) + 2   }}
							},
							scaleLabel: {
								 display: true,
								 fontSize: 20 
							  }
						}]            
					}  
			};
			// ChartEmployee
			var employee = new Chart(ctx, {
			  type: 'horizontalBar',
			  data: data,
			  options: options
			});

			document.getElementById("ChartEmployee").onclick = function(evt)
			{   
				var activePoints = employee.getElementsAtEvent(evt);
				var modal = this;
				if(activePoints.length > 0)
				{
					var clickedElementindex = activePoints[0]["_index"];
					var id = employee.data.id[clickedElementindex];
					window.location.href = "/conduct/profile_report/"+id;
					{{--  window.open("/conduct/month/"+id);  --}}
				}
			}
	</script>
	@endpush

{{--  $.ajax({
	type: "POST",
	url: "/conduct/result",
	data  : {id : id},
	success: function(rs) {
		$("#result").append('<tr><th>ลำดับ</th></tr>');
		$.each(rs, function(i, result) {
			$("#result").append('<tr><td>'+ result.title + "</td></tr>");
		});
	}
});
$('#StudentModal').modal('show');
$('#StudentModal').on('hidden.bs.modal', function () {
	$("#result").empty();
})  --}}
