@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app')
@section('title', 'บันทึกความประพฤติ')
@section('styles')
<style>
    select {
  text-align: center;
  text-align-last: center;
  /* webkit*/
}
</style>
    <!-- DataTables -->
    {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
        <!-- Select2 -->
    {{ Html::style('plugins/select2/select2.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'บันทึกความประพฤติ'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h2 class="box-title font" style="margin-top:19px;" >บันทึกความประพฤติ</h2>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                        <a href="/conduct/create" class="btn btn-lg btn btn-info btn_employee font" style="font-size:22px">
                                            <i class="ion ion-ios-compose" style="margin-right:10px;"></i> เพิ่มความประพฤติ
                                        </a>
                                    <span class="font pull-right title" style="margin:15px 10px 10px 10px;"> รวม {{ $conduct->total() }} ครั้ง</span>
                                    <span class="font title pull-right"> ประเภท
                                        <a href="/conduct/type/{{ 1 }}" class="btn btn-success"> <span class="text-green title" style="padding:5px 5px 5px 5px"> ดี {{ $good }} </span> </a>
                                        <a href="/conduct/type/{{ 2 }}" class="btn btn-danger"> <span class="text-red title"   style="padding:5px 5px 5px 5px"> ไม่ดี {{ $bad }} </span> </a>
                                    </span>
                                </div>

                                {{-- <div class="col-md-6 col-xs-12" style="margin-top:-22px;">
                                {!! Form::open(['url' => 'conduct/employee', 'class'=>'form-horizontal']) !!}
                                   <div class="box-body">
                                       <div class="form-group">
                                           {!! Form::label('month', 'เดือน', ['class'=>' col-md-1 col-sm-12 label_font']) !!}
                                           <div class="col-md-2 col-sm-12">
                                               {!! Form::select('month', $month , NULL, ['class'=>'form-control']) !!}
                                           </div>
                                           {!! Form::label('year', 'ปี', ['class'=>' col-md-1 col-sm-12 label_font']) !!}
                                            <div class="col-md-2 col-sm-12">
                                                {!! Form::select('year', $year , NULL, ['class'=>'form-control', 'style'=>' text-align: center;']) !!}
                                            </div>
                                       </div>
                                   </div>
                                {!! Form::close() !!}
                               </div> --}}
                               <div class="col-md-12 col-xs-12" style="margin-top:-22px;">
                                {!! Form::open(['url' => 'conduct/employee', 'class'=>'form-horizontal conduct_employee']) !!}
                                   <div class="box-body">
                                       <div class="form-group">
                                           {!! Form::label('employee_id', 'พนักงาน', ['class'=>'col-md-offset-4 col-md-4 col-sm-12 label_font']) !!}
                                           <div class="col-md-4 col-sm-12">
                                               {!! Form::select('employee_id', $employee , NULL, ['class'=>'form-control select2', 'required', 'style'=>'width:100%;','placeholder' => '-- เลือกพนักงาน --']) !!}
                                           </div>
                                       </div>
                                   </div>
                                {!! Form::close() !!}
                               </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        {{--  <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="10%">รหัสพนักงาน</th>
                                        <th>พนักงาน</th>
                                        <th>ประเภท</th>
                                        <th>รายละเอียด</th>
                                        <th>วันที่</th>
                                        <th>ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($conduct as $k => $rs)
                                    <tr>
                                        <td>{{ ++$k }}</td>
                                        <td class="text-center">{{ $rs->employee->code }}</td>
                                        <td width="22%">{{ $data['prename'][$rs->employee->prename] }} {{ $rs->employee->name }} {{ $rs->employee->surname }}</td>
                                        <td class="text-center">{{ $data['type'][$rs->type] }}</td>
                                        <td>{{ $rs->detail }}</td>
                                        <td class="text-center">{{ DateThaiLibrary::ThaiDate($rs->date) }}</td>
                                        <td width="15.1%">
                                            <a href="/conduct/{{$rs->id}}"      data-toggle="tooltip" data-placement="bottom" class="btn btn-primary left" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            <a href="/conduct/{{$rs->id}}/edit" data-toggle="tooltip" data-placement="bottom" class="btn btn-warning left" title="แก้ไขข้อมูล"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
                                            {{ Form::open(['url' => ['conduct', $rs->id], 'method'=>'DELETE', 'class' => 'left', 'id' => 'frm-delete'.$rs->id]) }}
                                                {{ Form::button('<i class="fa fa-trash"></i>',
                                                    ['type' => 'button', 'class'=>'btn btn-danger btn-del',
                                                    'data-id' => $rs->id,
                                                    'data-toggle' => 'tooltip',
                                                    'data-placement' => 'bottom',
                                                    'title'=>'ลบข้อมูล'])
                                                }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="10%">รหัสพนักงาน</th>
                                        <th>พนักงาน</th>
                                        <th>ประเภท</th>
                                        <th>รายละเอียด</th>
                                        <th>วันที่</th>
                                        <th>ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>  --}}
                        <div class="box-body">
                        <div class="row">
                         @foreach($conduct as $k => $rs)
                            <div class="col-md-4 col-xs-12">
                                @if($rs->type == 1)
                                    <div class="box box-success box-solid">
                                @else
                                    <div class="box box-danger box-solid">
                                @endif
                                <div class="box-header with-border">
                                    <i class="fa fa-text-width"></i>
                                    <p class="box-title font" style="font-size:21.5px !important;margin-top:10px"> {{ $rs->title }}</p>

                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body font" style="font-size:20px">
                                    <dl>
                                        <dt>พนักงาน</dt>
                                        <dd> {{ $data['prename'][$rs->employee->prename] }} {{ $rs->employee->name }} {{ $rs->employee->surname }} </dd>
                                        <dt>ประเภท</dt>
                                        <dd> {{ $data['type'][$rs->type] }} </dd>
                                        <dt>จัดการ</dt>
                                        <dd>
                                          <span style="text-align:center;">
                                            <div class="row">
                                              <div class="col-md-4">
                                                  <button class="btn btn-block btn-primary btn-show" data-toggle="modal" data-target="#show" data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ดูข้อมูล"><i class="fa fa-eye"></i></button>
                                              </div>
                                              <div class="col-md-4">
                                                <a href="/conduct/{{$rs->id}}/edit" class="btn btn-block btn-warning btn-edit" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>
                                              </div>
                                              <div class="col-md-4">
                                                <button class="btn btn-block btn-danger btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                              </div>
                                            </div>
                                          </span>
                                        </dd>
                                        {{-- <dt>รายละเอียด</dt>
                                        <dd> {{ str_limit($rs->detail,20) }} </dd>
                                        <dt>วันที่</dt>
                                        <dd>{{ DateThaiLibrary::ThaiDate($rs->date) }} </dd> --}}
                                    </dl>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- ./col -->
                         @endforeach
                        </div>
                        <!-- /.box-body -->
                        <center> {{ $conduct->links() }} </center>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@include('conduct.show')

@endsection
@push('scripts')
    <!-- Select2 -->
    {{ Html::script('plugins/select2/select2.full.min.js') }}
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
        <script>
            swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".select2").select2();

    $('.select2').change(function() {
        $('.conduct_employee').submit();
    });

    $('.btn-del').on('click',function() {
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ ความประพฤติ นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    url: "/conduct/"+id,
                    success: function(msg){
                        console.log(msg);
                        swal("Success!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success",{button:false});
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
				    }
			    });
            }
        });
    });

    $('#show').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id     = button.data('id')
        $.ajax({
            type: "GET",
            url: "/conduct/"+id,
            success: function(rs){
            $('.modal-title').text(rs.title)
            $('#employee').text(rs.employee.prename + " " + rs.employee.name + " " + rs.employee.surname)
            $('#type').text(rs.type)
            $('#detail').html(rs.detail)
            $('#date').text(rs.date)
			}
		});
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
