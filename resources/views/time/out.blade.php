<!-- Modal -->
<div id="out" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top:10%">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title font" style="font-size:22px"> เวลาเลิกงานปัจจุบัน </h4>
      </div>
      <div class="modal-body">
      {!! Form::open(['url' => '/time', 'method'=>'PUT']) !!}
      {!! Form::hidden('type', 'out') !!}
      {!! Form::hidden('id', NULL, ['id'=>'id']) !!}
      {!! Form::time('time', NULL, ['class'=>'form-control c_time',  'id'=> 'time']) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>