@extends('layouts.app') @section('title', 'ตั้งค่าเวลา') @section('styles')
<!-- flatpickr -->
{{ Html::style('plugins/flatpickr/flatpickr.min.css') }} @endsection @section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	@include('layouts.Backend.breadcrumb', ['title'=>'ตั้งค่าเวลา'])
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="box">
					<div class="box-header">
						<h2 class="box-title font" style="margin-top:19px;">ตั้งค่าเวลาในระบบ</h2>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<!-- left column -->
						<div class="col-md-6">
							<!-- Horizontal Form -->
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title font">เวลาเริ่มต้นรีเซ็ต</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-3 label_font text-right">เวลา</label>
										<div class="col-sm-6">
											<p class="form-control text-center">{{ $start->time }}</p>
										</div>
										<label class="col-sm-2">
											<button class="btn btn-info font text-left" data-id="{{ $start->id }}" data-time="{{ $start->time }}" data-toggle="modal"
											 data-target="#start" title="แก้ไขเวลา" style="font-size:22px;bottom: 17px;">
												<i class="ion ion-android-create"></i>
											</button>
										</label>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
                            <!-- /.box -->
                            <!-- Horizontal Form -->
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title font">เวลาที่สามารถมาสายได้</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-3 label_font text-right">เวลา</label>
										<div class="col-sm-6">
											<p class="form-control text-center">{{ $late->time }}</p>
										</div>
										<label class="col-sm-2">
											<button class="btn btn-info font text-left" data-id="{{ $late->id }}" data-time="{{ $late->time }}" data-toggle="modal"
											 data-target="#late" title="แก้ไขเวลา" style="font-size:22px;bottom: 17px;">
												<i class="ion ion-android-create"></i>
											</button>
										</label>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!--/.col (left) -->
						<!-- right column -->
						<div class="col-md-6">
							<!-- Horizontal Form -->
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title font">เวลาที่สามารถออกงานได้</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-3 label_font text-right">เวลา</label>
										<div class="col-sm-6">
											<p class="form-control text-center">{{ $end->time }}</p>
										</div>
										<label class="col-sm-2">
											<button class="btn btn-info font text-left" data-id="{{ $end->id }}" data-time="{{ $end->time }}" data-toggle="modal" data-target="#end"
											 data-toggle="tooltip" title="แก้ไขเวลา" style="font-size:22px;bottom: 17px;">
												<i class="ion ion-android-create"></i>
											</button>
										</label>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
							<!-- Horizontal Form -->
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title font">เวลาเลิกงานปัจจุบัน</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-3 label_font text-right">เวลา</label>
										<div class="col-sm-6">
											<p class="form-control text-center">{{ $out->time }}</p>
										</div>
										<label class="col-sm-2">
											<button class="btn btn-info font text-left" data-id="{{ $out->id }}" data-time="{{ $out->time }}" data-toggle="modal" data-target="#out" data-toggle="tooltip" title="แก้ไขเวลา" style="font-size:22px;bottom: 17px;">
												<i class="ion ion-android-create"></i>
											</button>
										</label>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!--/.col (right) -->
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
@include('time.start') 
@include('time.end') 
@include('time.late')
@include('time.out')  
@endsection 
@push('scripts')
<!-- flatpickr -->
{{ Html::script('plugins/flatpickr/flatpickr.min.js') }} {{ Html::script('plugins/flatpickr/rtl/th.js') }} @if (session('update'))
<script>
	swal("Success!", "ทำการแก้ไขเรียบร้อยแล้ว", "success");
</script>
@endif

<script>
		$('#start').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget) 
            var id      = button.data('id') 
            var time    = button.data('time') 
            $(this).find('.modal-body #id').val(id)
            $(this).find('.modal-body #time').val(time)
        });

        $('#end').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget) 
            var id      = button.data('id') 
            var time    = button.data('time') 
            $(this).find('.modal-body #id').val(id)
            $(this).find('.modal-body #time').val(time)
        });

        $('#late').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget) 
            var id      = button.data('id') 
            var time    = button.data('time') 
            $(this).find('.modal-body #id').val(id)
            $(this).find('.modal-body #time').val(time)
		});
		
		$('#out').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget) 
            var id      = button.data('id') 
            var time    = button.data('time') 
            $(this).find('.modal-body #id').val(id)
            $(this).find('.modal-body #time').val(time)
        });

        const c_time  = flatpickr(".c_time", {
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            minDate: "00:00",
            time_24hr: true    
        });
</script>
@endpush