@extends('layouts.app')
@section('title', 'การทำงานล่วงเวลา')
@section('styles')
     <!-- Fullcalendar -->
    {{ Html::style('plugins/fullcalendar/fullcalendar.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'การทำงานล่วงเวลา'])
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-12 col-xs-12">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <div class="box-header">
                      <h2 class="box-title font" style="margin-top:19px;" >ตารางตำแหน่งงาน</h2>
                      <button class="btn btn-lg btn-add btn btn-info btn_employee font" style="font-size:22px">
                          <i class="ion ion-person-stalker" style="margin-right:10px;"></i> เพิ่มตำแหน่งงาน
                      </button>
                  </div>
                  <!-- THE CALENDAR -->
                  <div style="padding:0px 20px 0px 20px">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="box box-primary">
                          <div class="box-body no-padding">
                            <div id="calendar"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


                </div>
                <!-- /.box-body -->
              </div>
              <!-- /. box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
        <!-- /.content -->
    </div>
@endsection

@push('scripts')
    <!-- Fullcalendar -->
    {{-- {{ Html::script('plugins/fullcalendar/gcal.min.js') }} --}}
    {{ Html::script('plugins/fullcalendar/lib/jquery-ui.min.js') }}
    {{ Html::script('plugins/fullcalendar/lib/moment.min.js') }}
    {{ Html::script('plugins/fullcalendar/fullcalendar.min.js') }}

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {

      $('#calendar').fullCalendar({
        header: {
          left: 'today',
          center: 'title',
          right: 'prev,next'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: 'event/json'
      });

    });
    </script>

@endpush
