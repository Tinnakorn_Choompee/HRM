@extends('layouts.app')
@section('title', 'รายงานขาดลามาสาย')
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	@include('layouts.Backend.breadcrumb', ['title'=>'รายงานขาดลามาสาย'])
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="box">
					<div class="box-header">
						<h2 class="box-title font" style="margin-top:19px;">รายงานขาดลามาสาย</h2>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- DONUT CHART -->
								<div class="box box-danger">
									<div class="box-header with-border">
										<h3 class="box-title font">กราฟขาดลามาสายระหว่าง วันที่ {{ $daybetween }} {{ $namemonth }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<div class="box-body">
										<div style="margin:auto;width:95%;">
											<canvas id="ChartWeek" width="450" height="350"></canvas>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (LEFT) -->
							<div class="col-md-6">
								<!-- AREA CHART -->
								<div class="box box-primary">
									<div class="box-header with-border">
										{{-- @foreach($dayofweek as $k => $rs) {{ $k." ขาด".$rs[0] }}
										<br> @endforeach --}}
										<h3 class="box-title font">กราฟขาดลามาสาย รายเดือน ปี {{ date('Y')+543 }}</h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<div class="box-body">
										<div style="margin:auto;width:95%;">
											<canvas id="ChartMonth" width="450" height="350"></canvas>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (RIGHT) -->
							<div class="col-md-6">
								<!-- AREA CHART -->
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title font">กราฟขาดลามาสาย รายปี {{ $yeary[0] }} - {{ end($yeary) }} </h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-box-tool" data-widget="collapse">
												<i class="fa fa-minus"></i>
											</button>
										</div>
									</div>
									<div class="box-body">
											<div style="margin:auto;width:95%;">
												<canvas id="ChartYear" width="450" height="350"></canvas>
											</div>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (RIGHT) -->
							<div class="col-md-6">
									<!-- AREA CHART -->
									<div class="box box-primary">
										<div class="box-header with-border">
											<h3 class="box-title font">กราฟขาดลามาสาย</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
												<div style="margin:auto;width:95%;">
												<canvas id="ChartWork" width="450" height="350"></canvas>
											</div>
										</div>
										<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (RIGHT) -->
							<div class="col-md-12">
									<!-- AREA CHART -->
									<div class="box box-primary">
										<div class="box-header with-border">
											<h3 class="box-title font">กราฟขาดลามาสายของพนักงาน</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool" data-widget="collapse">
													<i class="fa fa-minus"></i>
												</button>
											</div>
										</div>
										<div class="box-body">
											<div style="margin:auto;width:50%;">
												<canvas id="ChartEmployee" width="450" height="350"></canvas>
											</div>
										</div>
										<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (RIGHT) -->
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
	</section>
	<!-- /.content -->
	</div>
	@endsection
	@push('scripts')
	<!-- ChartJS -->
	{{ Html::script('plugins/chartjs/Chart.min.js') }}
	<script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	</script>
	<!----------------------------------------------------------  ChartSetting  -------------------------------------------------------------->
	<script>
		// Global Options:
		Chart.defaults.global.defaultFontColor = 'black';
		Chart.defaults.global.defaultFontSize = 22;
		Chart.defaults.global.defaultFontFamily = 'DBHelvethaica';
	</script>
	<!----------------------------------------------------------  ChartWork  -------------------------------------------------------------->
	<script>
		var data = {
			datasets: [{
				data: [
					@foreach($work as $rs)
						'{{ $rs->total }}',
					@endforeach
				],
				backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(153, 51, 0, 0.2)',
				],
				borderColor: [
					'rgba(255, 99, 132, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(153, 51, 0, 1)',
				],
				borderWidth: 1,
				label: 'My dataset'
				}],
			labels: [
				@foreach($label as $rs)
						'{{ $rs }}',
				@endforeach
			],
			id: [
				@foreach($work as $rs)
					'{{ $rs->status }}',
				@endforeach
			]
		};
		var pieOptions = {
			responsive: true,
				legend: {
				position: 'bottom',
			},
			hover: {
				mode: 'label'
			},
			title: {
				display: true,
				text: 'การขาดลามาสายทั้งหมด {{ $total }} ครั้ง',
				fontSize : 22,
				fontFamily : 'DBHelvethaica',
				fontStyle : 'normal',
		},
		animation: {
			duration: 500,
			animateScale: true,
			animateRotate: true,
			easing: "easeOutQuart",
			onComplete: function () {
			var ctx = this.chart.ctx;
			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';
			this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
						total = dataset._meta[Object.keys(dataset._meta)[0]].total,
						mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
						start_angle = model.startAngle,
						end_angle = model.endAngle,
						mid_angle = start_angle + (end_angle - start_angle)/2;

				var x = mid_radius * Math.cos(mid_angle);
				var y = mid_radius * Math.sin(mid_angle);

					ctx.fillStyle = '#000';
					ctx.font = "22px DBHelvethaica";
					if (i == 3){
						ctx.fillStyle = '#444';
					}
					var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
					ctx.fillText(dataset.data[i], model.x + x, model.y + y);
					ctx.fillText(percent, model.x + x, model.y + y + 15);
					}
				});
				}
			}
		};

		var ctx = document.getElementById("ChartWork").getContext("2d");
		var work = new Chart(ctx,  {
			type: 'doughnut', // or doughnut
			data: data,
			options: pieOptions
		});

		document.getElementById("ChartWork").onclick = function(evt)
		{
			var activePoints = work.getElementsAtEvent(evt);
			var modal = this;
			if(activePoints.length > 0)
			{
				var clickedElementindex = activePoints[0]["_index"];
				var id = work.data.id[clickedElementindex];
				window.location.href = "/work/status/"+id;
			}
		}
	</script>
	<!----------------------------------------------------------  ChartWeek  ---------------------------------------------------------------->
	<script>
		var ctx = document.getElementById("ChartWeek");
		document.getElementById("ChartWeek").onclick = function(evt)
		{
			var activePoints = week.getElementsAtEvent(evt);
			var modal = this;
			if(activePoints.length > 0)
			{
				var clickedElementindex = activePoints[0]["_index"];
				var id = week.data.id[clickedElementindex];
				window.location.href = "/work/date/"+id;
			}
		}
		var week = new Chart(ctx, {
		  type: 'bar',
		  data: {
			labels: [
				@foreach($dayofweek as $k => $rs)
					'{{ $k }}',
				@endforeach
			],
			id : [
				@foreach($dayofweek as $k => $rs)
					'{{ $k }}',
				@endforeach
			],
			datasets: [
				{
					type: "bar",
					label: 'ขาด',
					data: [
						@foreach($dayofweek as $k => $rs)
							'{{ $rs[0] }}',
						@endforeach
					],
					backgroundColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(255, 99, 132, 0.2)',
						@endforeach
					],
					borderColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(255, 99, 132, 1)',
						@endforeach
					],
					borderWidth: 1,
				  },
				  {
					type : "bar",
					label: 'มา',
					data: [
						@foreach($dayofweek as $k => $rs)
							'{{ $rs[1] }}',
						@endforeach
					],
					backgroundColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(75, 192, 192, 0.2)',
						@endforeach
					],
					borderColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(75, 192, 192, 1)',
						@endforeach
					],
					borderWidth: 1
				},
				{
					type : "bar",
					label: 'สาย',
					data: [
						@foreach($dayofweek as $k => $rs)
							'{{ $rs[2] }}',
						@endforeach
					],
					backgroundColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(255, 206, 86, 0.2)',
						@endforeach
					],
					borderColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(255, 206, 86, 1)',
						@endforeach
					],
					borderWidth: 1
				},
				{
					type : "bar",
					label: 'ลา',
					data: [
						@foreach($dayofweek as $k => $rs)
							'{{ $rs[3] }}',
						@endforeach
					],
					backgroundColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(153, 51, 0, 0.2)',
						@endforeach
					],
					borderColor: [
						@foreach($dayofweek as $k => $rs)
							'rgba(153, 51, 0, 1)',
						@endforeach
					],
					borderWidth: 1
				  }
			]
		  },
		  options: {
			animation: {
				onComplete: function () {
					var ctx = this.chart.ctx;
					ctx.textAlign = "center";
					ctx.textBaseline = "middle";
					var chart    = this;
					var datasets = this.config.data.datasets;
					datasets.forEach(function (dataset, i) {
						ctx.font = "22px DBHelvethaica";
						switch (dataset.type) {
							case "lines":
								ctx.fillStyle = "Black";
								chart.getDatasetMeta(i).data.forEach(function (p, j) {
									ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
								});
								break;
							case "bars":
								ctx.fillStyle = "Black";
								chart.getDatasetMeta(i).data.forEach(function (p, j) {
									if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
								});
								break;
						}
					});
				}
			},
			scales: {
			  responsive: true,
			  yAxes: [{
				stacked: true,
				ticks: {
					  beginAtZero: true,
					  steps: 10,
					stepValue: 5,
					max : {{ $max }}
				}
			  }],
			  xAxes: [{
				stacked: true,
				ticks: {
				  beginAtZero: true,
				  fontSize : 22,
				  fontFamily : 'DBHelvethaica',
				  fontStyle : 'normal',
				}
			  }]
			}
		  }
		});
	</script>
	<!----------------------------------------------------------  ChartMonth  ---------------------------------------------------------------->
	<script>
		var ctx = document.getElementById("ChartMonth");
			document.getElementById("ChartMonth").onclick = function(evt)
			{
				var activePoints = month.getElementsAtEvent(evt);
				var modal = this;
				if(activePoints.length > 0)
				{
					var clickedElementindex = activePoints[0]["_index"];
					var id = month.data.id[clickedElementindex];
					window.location.href = "/work/month/"+id;
				}
			}
			var month = new Chart(ctx, {
			  type: 'bar',
			  data: {
				labels: [
					@foreach($month as $rs)
						'{{ $rs }}',
					@endforeach
				],
				id : [
					@foreach($month as $k => $rs)
						'{{ $k }}',
					@endforeach
				],
				datasets: [
					{
						type: "bar",
						label: 'ขาด',
						data: [
							@foreach($absence as $rs)
								{{ $rs }},
							@endforeach
						],
						backgroundColor: [
							@foreach($absence as $rs)
								'rgba(255, 99, 132, 0.2)',
							@endforeach
						],
						borderColor: [
							@foreach($absence as $rs)
								'rgba(255, 99, 132, 1)',
							@endforeach
						],
						borderWidth: 1,
				  	},
				  	{
						type : "bar",
						label: 'มา',
						data: [
							@foreach($come as $rs)
								{{ $rs }},
							@endforeach
						],
						backgroundColor: [
							@foreach($come as $rs)
								'rgba(75, 192, 192, 0.2)',
							@endforeach
						],
						borderColor: [
							@foreach($come as $rs)
								'rgba(75, 192, 192, 1)',
							@endforeach
						],
						borderWidth: 1
					},
					{
						type : "bar",
						label: 'สาย',
						data: [
							@foreach($late as $rs)
								{{ $rs }},
							@endforeach
						],
						backgroundColor: [
							@foreach($late as $rs)
								'rgba(255, 206, 86, 0.2)',
							@endforeach
						],
						borderColor: [
							@foreach($late as $rs)
								'rgba(255, 206, 86, 1)',
							@endforeach
						],
						borderWidth: 1
					},
					{
						type : "bar",
						label: 'ลา',
						data: [
							@foreach($leave as $rs)
								{{ $rs }},
							@endforeach
						],
						backgroundColor: [
							@foreach($leave as $rs)
								'rgba(153, 51, 0, 0.2)',
							@endforeach
						],
						borderColor: [
							@foreach($leave as $rs)
								'rgba(153, 51, 0, 1)',
							@endforeach
						],
						borderWidth: 1
				  	}
				]
			  },
			  options: {
				animation: {
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.textAlign = "center";
                        ctx.textBaseline = "middle";
                        var chart    = this;
                        var datasets = this.config.data.datasets;
                        datasets.forEach(function (dataset, i) {
                            ctx.font = "22px DBHelvethaica";
                            switch (dataset.type) {
                                case "line":
                                    ctx.fillStyle = "Black";
                                    chart.getDatasetMeta(i).data.forEach(function (p, j) {
                                        ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
                                    });
                                    break;
                                case "Lbar":
                                    ctx.fillStyle = "Black";
                                    chart.getDatasetMeta(i).data.forEach(function (p, j) {
										if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
                                    });
                                    break;
                            }
                        });
                    }
                },
				scales: {
				  responsive: true,
				  yAxes: [{
					stacked: true,
					ticks: {
					  	beginAtZero: true,
					  	steps: 10,
                        stepValue: 5,
                        max : {{ max($absence)+max($come)+max($late)+max($leave)+5 }}
					}
				  }],
				  xAxes: [{
					stacked: true,
					ticks: {
					  beginAtZero: true,
					  fontSize : 22,
					  fontFamily : 'DBHelvethaica',
					  fontStyle : 'normal',
					}
				  }]
				}
			  }
			});
	</script>
	<!----------------------------------------------------------  ChartYear  ---------------------------------------------------------------->
	<script>
		var ctx = document.getElementById("ChartYear");
				document.getElementById("ChartYear").onclick = function(evt)
				{
					var activePoints = year.getElementsAtEvent(evt);
					var modal = this;
					if(activePoints.length > 0)
					{
						var clickedElementindex = activePoints[0]["_index"];
						var id = year.data.id[clickedElementindex];
						window.location.href = "/work/year/"+id;
					}
				}
				var year = new Chart(ctx, {
				  type: 'bar',
				  data: {
					labels: [
						@foreach($yeary as $rs)
							'{{$rs}}',
        				@endforeach
					],
					id : [
						@foreach($yeary as $rs)
							'{{$rs}}',
        				@endforeach
					],
					datasets: [
						{
							type: "bar",
							label: 'ขาด',
							data: [
								@foreach($absencey as $rs)
									{{ $rs }},
								@endforeach
							],
							backgroundColor: [
								@foreach($absencey as $rs)
									'rgba(255, 99, 132, 0.2)',
								@endforeach
							],
							borderColor: [
								@foreach($absencey as $rs)
									'rgba(255, 99, 132, 1)',
								@endforeach
							],
							borderWidth: 1,
						  },
						  {
							type : "bar",
							label: 'มา',
							data: [
								@foreach($comey as $rs)
									{{ $rs }},
								@endforeach
							],
							backgroundColor: [
								@foreach($comey as $rs)
									'rgba(75, 192, 192, 0.2)',
								@endforeach
							],
							borderColor: [
								@foreach($comey as $rs)
									'rgba(75, 192, 192, 1)',
								@endforeach
							],
							borderWidth: 1
						},
						{
							type : "bar",
							label: 'สาย',
							data: [
								@foreach($latey as $rs)
									{{ $rs }},
								@endforeach
							],
							backgroundColor: [
								@foreach($latey as $rs)
									'rgba(255, 206, 86, 0.2)',
								@endforeach
							],
							borderColor: [
								@foreach($latey as $rs)
									'rgba(255, 206, 86, 1)',
								@endforeach
							],
							borderWidth: 1
						},
						{
							type : "bar",
							label: 'ลา',
							data: [
								@foreach($leavey as $rs)
									{{ $rs }},
								@endforeach
							],
							backgroundColor: [
								@foreach($leavey as $rs)
									'rgba(153, 51, 0, 0.2)',
								@endforeach
							],
							borderColor: [
								@foreach($leavey as $rs)
									'rgba(153, 51, 0, 1)',
								@endforeach
							],
							borderWidth: 1
						  }
					]
				  },
				  options: {
					animation: {
						onComplete: function () {
							var ctx = this.chart.ctx;
							ctx.textAlign = "center";
							ctx.textBaseline = "middle";
							var chart    = this;
							var datasets = this.config.data.datasets;
							datasets.forEach(function (dataset, i) {
								ctx.font = "22px DBHelvethaica";
								switch (dataset.type) {
									case "line":
										ctx.fillStyle = "Black";
										chart.getDatasetMeta(i).data.forEach(function (p, j) {
											ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
										});
										break;
									case "Lbar":
										ctx.fillStyle = "Black";
										chart.getDatasetMeta(i).data.forEach(function (p, j) {
											if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
										});
										break;
								}
							});
						}
					},
					scales: {
					  responsive: true,
					  yAxes: [{
						stacked: true,
						ticks: {
							  beginAtZero: true,
							  steps: 10,
							stepValue: 5,
							max : {{ max($absencey)+max($comey)+max($latey)+max($leave)+5 }}
						}
					  }],
					  xAxes: [{
						stacked: true,
						ticks: {
						  beginAtZero: true,
						  fontSize : 22,
						  fontFamily : 'DBHelvethaica',
						  fontStyle : 'normal',
						}
					  }]
					}
				  }
				});
	</script>
	<!----------------------------------------------------------  ChartEmployee  ---------------------------------------------------------------->
	<script>
			var ctx = document.getElementById("ChartEmployee");
					document.getElementById("ChartEmployee").onclick = function(evt)
					{
						var activePoints = employee.getElementsAtEvent(evt);
						var modal = this;
						if(activePoints.length > 0)
						{
							var clickedElementindex = activePoints[0]["_index"];
							var id = employee.data.id[clickedElementindex];
							window.location.href = "/work/"+id;
						}
					}
					var employee = new Chart(ctx, {
					type: 'horizontalBar',
					data: {
						labels: [
							@foreach($employee as $rs)
								"{{ $data['prename'][$rs->prename].' '.$rs->name.' '.$rs->surname}}",
							@endforeach
						],
						id: [
							@foreach($employee as $rs)
								'{{ $rs->code }}',
							@endforeach
						],
						datasets: [
							{
								type: "horizontalBar",
								label: 'ขาด',
								data: [
									@foreach($empabsence as $rs)
										{{ $rs }},
									@endforeach
								],
								backgroundColor: [
									@foreach($empabsence as $rs)
										'rgba(255, 99, 132, 0.2)',
									@endforeach
								],
								borderColor: [
									@foreach($empabsence as $rs)
										'rgba(255, 99, 132, 1)',
									@endforeach
								],
								borderWidth: 1,
							},
							{
								type : "horizontalBar",
								label: 'มา',
								data: [
									@foreach($empcome as $rs)
										{{ $rs }},
									@endforeach
								],
								backgroundColor: [
									@foreach($empcome as $rs)
										'rgba(75, 192, 192, 0.2)',
									@endforeach
								],
								borderColor: [
									@foreach($empcome as $rs)
										'rgba(75, 192, 192, 1)',
									@endforeach
								],
								borderWidth: 1
							},
							{
								type : "horizontalBar",
								label: 'สาย',
								data: [
									@foreach($emplate as $rs)
										{{ $rs }},
									@endforeach
								],
								backgroundColor: [
									@foreach($emplate as $rs)
										'rgba(255, 206, 86, 0.2)',
									@endforeach
								],
								borderColor: [
									@foreach($emplate as $rs)
										'rgba(255, 206, 86, 1)',
									@endforeach
								],
								borderWidth: 1
							},
							{
								type : "horizontalBar",
								label: 'ลา',
								data: [
									@foreach($empleave as $rs)
										{{ $rs }},
									@endforeach
								],
								backgroundColor: [
									@foreach($empleave as $rs)
										'rgba(153, 51, 0, 0.2)',
									@endforeach
								],
								borderColor: [
									@foreach($empleave as $rs)
										'rgba(153, 51, 0, 1)',
									@endforeach
								],
								borderWidth: 1
							}
						]
					},
					options: {
						animation: {
							onComplete: function () {
								var ctx = this.chart.ctx;
								ctx.textAlign = "center";
								ctx.textBaseline = "middle";
								var chart    = this;
								var datasets = this.config.data.datasets;
								datasets.forEach(function (dataset, i) {
									ctx.font = "22px DBHelvethaica";
									switch (dataset.type) {
										case "line":
											ctx.fillStyle = "Black";
											chart.getDatasetMeta(i).data.forEach(function (p, j) {
												ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 20);
											});
											break;
										case "Lbar":
											ctx.fillStyle = "Black";
											chart.getDatasetMeta(i).data.forEach(function (p, j) {
												if(datasets[i].data[j] != 0) { ctx.fillText(datasets[i].data[j], p._model.x, p._model.y + 18); }
											});
											break;
									}
								});
							}
						},
						scales: {
						responsive: true,
						yAxes: [{
							stacked: true,
							ticks: {
								beginAtZero: true,
								steps: 10,
								stepValue: 5,
								max : {{ max($absencey)+max($comey)+max($latey)+max($leave)+5 }}
							}
						}],
						xAxes: [{
							stacked: true,
							ticks: {
							beginAtZero: true,
							fontSize : 22,
							fontFamily : 'DBHelvethaica',
							fontStyle : 'normal',
							}
						}]
						}
					}
					});
	</script>

	@endpush
