@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Libraries\ClockLibrary\ClockLibrary;
@endphp
@extends('layouts.app') 
@section('title', 'บันทึกขาดลามาสาย') 
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }} 
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'บันทึกขาดลามาสาย'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                  {{ Form::open(['url' => 'clock_all', 'method'=>'DELETE', 'id'=>'clock_all']) }}
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >ตารางบันทึกการขาดลามาสาย</h2>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="report" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr>
                                        <th width="7%">#</th>
                                        <th width="25%">รายชื่อพนักงาน</th>
                                        <th width="12%">วันที่</th>
                                        <th width="10%">มา</th>
                                        <th width="10%">สาย</th>
                                        <th width="10%">ขาด</th>
                                        <th width="10%">ลา</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:20px">
                                @foreach($work as $k => $rs)
                                    <tr>                             
                                        <td class="text-center">{{ ++$k }}</td>
                                        <td>{{ $data['prename'][$rs->employee->prename] }} {{ $rs->employee->name }} {{ $rs->employee->surname }}</td>
                                        <td class="text-center">{!! DateThaiLibrary::ThaiDate($rs->date) !!}</td>
                                        <td class="text-center">{!! ($rs->status == 1) ? "<i class='ion ion-android-checkmark-circle text-green'></i>" : "<i class='ion ion-android-remove text-red'></i>" !!}</td>
                                        <td class="text-center">{!! ($rs->status == 2) ? "<i class='ion ion-android-checkmark-circle text-green'></i>" : "<i class='ion ion-android-remove text-red'></i>" !!}</td>  
                                        <td class="text-center">{!! ($rs->status == 0) ? "<i class='ion ion-android-checkmark-circle text-green'></i>" : "<i class='ion ion-android-remove text-red'></i>" !!}</td>  
                                        <td class="text-center">{!! ($rs->status == 3) ? "<i class='ion ion-android-checkmark-circle text-green'></i>" : "<i class='ion ion-android-remove text-red'></i>" !!}</td>    
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>                            
                                        <th colspan="3">
                                            รวมทั้งหมด
                                        </th>
                                        <th width="10%">{{ $come }}</th>
                                        <th width="10%">{{ $late }}</th>
                                        <th width="10%">{{ $absence }}</th>
                                        <th width="10%">{{ $leave }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                  {{ Form::close() }}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@include('clocktime.report.clockin')
@include('clocktime.report.clockout')
@include('leave.create', ['leavetype' => $leavetype])
@endsection 
@push('scripts')
    <!-- flatpickr -->
    {{ Html::script('plugins/flatpickr/flatpickr.min.js') }} 
    {{ Html::script('plugins/flatpickr/rtl/th.js') }} 
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }} 
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js') }}

     @if (session('update'))
     <script>
        swal("Success!", "ทำการแก้ไขเรียบร้อยแล้ว", "success");
     </script>
     @elseif (session('delete'))
    <script>
        swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
     @endif

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".check-table").click(function () {
            $(this).prop('checked') ? $(".btn-all").prop("disabled", false) : $(".btn-all").prop("disabled", true);
        });

        $(".checkbox-toggle").click(function () {
        $(".table-hover input[type='checkbox'], .check-all input[type='checkbox']").prop('checked', $(this).is(':checked'));
        });

        $( ".radio-half" ).click(function() {
            $( ".full" ).hide();
            $( ".half" ).show();
        });

        $( ".radio-full" ).click(function() {
            $( ".full" ).show();
            $( ".half" ).hide();
        });

        $('#create').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id     = button.data('id')
            var page   = "leave";
            var title  = "การขอลางาน";
            $(this).find('.modal-title').html(title)
            $(this).find('.modal-body .id').val(id)
            $(this).find('.modal-body .page').val(page)
        });

         var date = new Date();
    const c_clock = flatpickr(".c_clock", {
        locale : 'th',
        defaultDate	: date,
        disable: [
            @foreach($holiday as $rs)
                '{{ $rs->date }}',
             @endforeach
            ,
            function(date) {
                return (date.getDay() === 0)
            },
            {
                from: "2000-01-01",
                to:  date -1 
            }
        ]
    });

    var date = new Date();
    const c_clock_b = flatpickr(".c_clock_b", {
        locale : 'th',
        defaultDate	: date,
        disable: [
            @foreach($holiday as $rs)
                '{{ $rs->date }}',
             @endforeach
            ,
            function(date) {
                return (date.getDay() === 0)
            }
        ]
    });

    $('.c_clock_b').hide();
     $('#leavetype').on('change', function(event) {
         if($('#leavetype').val() == "ลาป่วย") {
            $('.c_clock').hide();
            $('.c_clock_b').show();
         } else {
            $('.c_clock').show();
            $('.c_clock_b').hide();
         }
     });

        $('#clock_in').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget) 
            var id      = button.data('id') 
            var date    = button.data('date') 
            var time    = button.data('time') 
            $(this).find('.modal-body #id').val(id)
            $(this).find('.modal-body #c_in').val(date)
            $(this).find('.modal-body #c_in_time').val(time)
        });

        $('#clock_out').on('show.bs.modal', function (event) {
            var button  = $(event.relatedTarget) 
            var id      = button.data('id') 
            var date    = button.data('date') 
            var time    = button.data('time') 
            $(this).find('.modal-body #id').val(id)
            $(this).find('.modal-body #c_in').val(date)
            $(this).find('.modal-body #c_in_time').val(time)
        });

        $('.btn-del').on('click',function(e){
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "ต้องการที่จะลบ ข้อมูลเวลางาน นี้ใช่หรือไม่ !!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then(willDelete => {
                if (willDelete) { 
                    $.ajax({
                        type: "DELETE",
                        url: "/clocktime",
                        data: {id : id},
                        success: function(msg){
                            swal("Success!", "ทำการลบข้อมูลเวลางานเรียบร้อยแล้ว", "success",{button:false});                       
                            setTimeout(function() {
                                location.reload(); 
                            }, 1000); 
                        }
                    });
                }
            });
        });

        $('.btn-all').on('click',function(e){
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "ต้องการที่จะลบ ข้อมูลที่เลือก ใช่หรือไม่ !!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then(willDelete => {
                if (willDelete) { 
                    $( "#clock_all" ).submit();
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip(); 

        $(function () {
                $("#example1").DataTable();
                $('#report').DataTable({ "dom": '<"left"l><"right"f> rt <"right"p> <"clear">'});
        });

    </script>
@endpush