@php
use Libraries\ClockLibrary\ClockLibrary;
@endphp
@extends('layouts.app')
@section('title', 'ลงเวลางาน')
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'ลงเวลาทำงานพนักงาน'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >ลงเวลาทำงาน {{ Cookie::get('cookie') }}</h2>
                            <div class="right time">
                               <span class="font" style="font-size:25px"> เวลา </span> <span id="h"></span> : <span id="m"></span> : <span id="s"></span>
                            </div>
                            <div class="right date">
                                <span class="font" style="font-size:25px"> {{ $datetime }}
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>

                    @isset($employee_month[0])
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" > พนักงานรายเดือน </h2>
                        </div>
                        <div class="row">
                        @foreach($employee_month as $v)
                             <div class="col-md-3 col-xs-12">
                                <div class="col-md-12 col-xs-12">
                                    <!-- Widget: user widget style 1 -->
                                    <div class="box box-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-aqua-active">
                                    <h5 class="widget-user-username font" style="font-size:12px">{{ $data['prename'][$v->prename] }} {{ $v->name }} {{ $v->surname }}
                                    @foreach($clock_in as $k => $c)
                                    @if($c['employee_id'] == $v->id && ($c['clock_status'] == 2))
                                        <span style="float:right"> ชม. {{ ClockLibrary::c_diff($c['clock_in'], $c['clock_out']) }}  </span> </h5>
                                    @endif
                                    @endforeach
                                    <h5 class="widget-user-desc" style="margin-top:5px">{{ $v->position }}</h5>
                                    </div>
                                    <div class="widget-user-image">
                                        {{ Html::image('images/employee/'.$v->image , $v->image , ['class'=>'img-responsive img-circle']) }}
                                    </div>
                                    <div class="box-footer">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="description-block" style="margin-top:19px">
                                                @foreach($clock_in as $k => $c)
                                                @if($c['employee_id'] == $v->id && ($c['clock_status'] == 1))
                                                    <span class="description-text text-success"> บันทึกเข้างานเรียบร้อย </span>
                                                    <span class="description-text text-back"> {{ date('H:i:s', strtotime($c->clock_in))  }}</span>
                                                @elseif($c['employee_id'] == $v->id && ($c['clock_status'] == 2))
                                                    <span class="description-text text-primary"> บันทึกออกงานเรียบร้อย </span>
                                                    <span class="description-text text-back"> {{ date('H:i:s', strtotime($c->clock_out))  }}</span>
                                                @endif
                                                @endforeach
                                                @if($v->clock_status == 0)
                                                  <span class="description-text text-primary"> กรุณาบันทึกเข้างาน </span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- /.col -->

                                        <!-- /.col -->
                                        <div class="col-sm-12 col-xs-12">
                                        <div class="description-block btn-clock">
                                            @if ((Carbon::now()->dayOfWeek === Carbon::SUNDAY) || !(empty($check_holiday)))
                                               <span class="text-red"> วันหยุด </span>
                                            @else
                                                @if($v->clock_status == 0)
                                                    <button class="btn btn-block btn-info clock_in" data-id="{{ $v->id }}"> <i class="ion ion-arrow-left-b" style="margin-right:5px"></i> บันทึกการเข้างาน </button>
                                                @elseif($v->clock_status == 1)
                                                    @if($time > $clock_out)
                                                        <button class="btn btn-block btn-primary clock_out" data-id="{{ $v->id }}"> บันทึกการออกงาน <i class="ion ion-arrow-right-b" style="margin-left:5px"></i> </button>
                                                    @else
                                                        <button class="btn btn-block btn-primary clock_out" data-id="{{ $v->id }}" disabled> บันทึกการออกงาน <i class="ion ion-arrow-right-b" style="margin-left:5px"></i> </button>
                                                    @endif
                                                @elseif($v->clock_status == 2)
                                                       <button class="btn btn-block btn-primary" disabled> กรุณารอบันทึกเข้างานวันถัดไป   </button>
                                                @else
                                                    <button class="btn btn-block btn-info clock_in" data-id="{{ $v->id }}"> <i class="ion ion-arrow-left-b" style="margin-right:5px"></i> บันทึกการเข้างาน </button>
                                                @endif
                                             @endif
                                        </div>
                                        <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                    </div>
                                    </div>
                                    <!-- /.widget-user -->
                                </div>
                                 <!-- /.col -->
                            </div>
                            <!-- /.col -->
                        @endforeach
                        </div>
                        <!-- /.row -->
                    </div>
                    @endisset

                    @isset($employee_day[0])
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" > พนักงานรายวัน </h2>
                        </div>
                        <div class="row">
                        @foreach($employee_day as $v)
                          <div class="col-md-3 col-xs-12">
                             <div class="col-md-12 col-xs-12">
                                 <!-- Widget: user widget style 1 -->
                                 <div class="box box-widget widget-user">
                                 <!-- Add the bg color to the header using any of the bg-* classes -->
                                 <div class="widget-user-header bg-aqua-active">
                                 <h5 class="widget-user-username font" style="font-size:12px">{{ $data['prename'][$v->prename] }} {{ $v->name }} {{ $v->surname }}
                                 @foreach($clock_in as $k => $c)
                                 @if($c['employee_id'] == $v->id && ($c['clock_status'] == 2))
                                     <span style="float:right"> ชม. {{ ClockLibrary::c_diff($c['clock_in'], $c['clock_out']) }}  </span> </h5>
                                 @endif
                                 @endforeach
                                 <h5 class="widget-user-desc" style="margin-top:5px">{{ $v->position }}</h5>
                                 </div>
                                 <div class="widget-user-image">
                                     {{ Html::image('images/employee/'.$v->image , $v->image , ['class'=>'img-responsive img-circle']) }}
                                 </div>
                                 <div class="box-footer">
                                 <div class="row">
                                     <div class="col-sm-12 col-xs-12">
                                         <div class="description-block" style="margin-top:19px">
                                             @foreach($clock_in as $k => $c)
                                             @if($c['employee_id'] == $v->id && ($c['clock_status'] == 1))
                                                 <span class="description-text text-success"> บันทึกเข้างานเรียบร้อย </span>
                                                 <span class="description-text text-back"> {{ date('H:i:s', strtotime($c->clock_in))  }}</span>
                                             @elseif($c['employee_id'] == $v->id && ($c['clock_status'] == 2))
                                                 <span class="description-text text-primary"> บันทึกออกงานเรียบร้อย </span>
                                                 <span class="description-text text-back"> {{ date('H:i:s', strtotime($c->clock_out))  }}</span>
                                             @endif
                                             @endforeach
                                             @if($v->clock_status == 0)
                                               <span class="description-text text-primary"> กรุณาบันทึกเข้างาน </span>
                                             @endif
                                         </div>
                                     </div>
                                     <!-- /.col -->

                                     <!-- /.col -->
                                     <div class="col-sm-12 col-xs-12">
                                     <div class="description-block btn-clock">
                                         @if (Carbon::now()->dayOfWeek === Carbon::SUNDAY)
                                            <span class="text-red"> วันหยุด </span>
                                         @else
                                             @if($v->clock_status == 0)
                                                 <button class="btn btn-block btn-info clock_in" data-id="{{ $v->id }}"> <i class="ion ion-arrow-left-b" style="margin-right:5px"></i> บันทึกการเข้างาน </button>
                                             @elseif($v->clock_status == 1)
                                                 @if($time > $clock_out)
                                                     <button class="btn btn-block btn-primary clock_out" data-id="{{ $v->id }}"> บันทึกการออกงาน <i class="ion ion-arrow-right-b" style="margin-left:5px"></i> </button>
                                                 @else
                                                     <button class="btn btn-block btn-primary clock_out" data-id="{{ $v->id }}" disabled> บันทึกการออกงาน <i class="ion ion-arrow-right-b" style="margin-left:5px"></i> </button>
                                                 @endif
                                             @elseif($v->clock_status == 2)
                                                     <button class="btn btn-block btn-primary" disabled> กรุณารอบันทึกเข้างานวันถัดไป  </button>
                                             @else
                                                 <button class="btn btn-block btn-info clock_in" data-id="{{ $v->id }}"> <i class="ion ion-arrow-left-b" style="margin-right:5px"></i> บันทึกการเข้างาน </button>
                                             @endif
                                          @endif
                                     </div>
                                     <!-- /.description-block -->
                                     </div>
                                     <!-- /.col -->
                                 </div>
                                 <!-- /.row -->
                                 </div>
                                 </div>
                                 <!-- /.widget-user -->
                             </div>
                              <!-- /.col -->
                         </div>
                         <!-- /.col -->
                        @endforeach
                        </div>
                        <!-- /.row -->
                    </div>
                    @endisset

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@include('leave.create', ['leavetype' => $leavetype])
@endsection
@push('scripts')
    <!-- flatpickr -->
    {{ Html::script('plugins/flatpickr/flatpickr.min.js') }}
    {{ Html::script('plugins/flatpickr/rtl/th.js') }}

    @if (session('leave'))
    <script>
       swal("Success!", "ทำการลางานเรียบร้อยแล้ว", "success");
    </script>
    @endif

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function restart() {
      location.reload();
    }

     $( ".radio-half" ).click(function() {
        $( ".full" ).hide();
        $( ".half" ).show();
    });

     $( ".radio-full" ).click(function() {
        $( ".full" ).show();
        $( ".half" ).hide();
    });

    var date = new Date();
    const c_clock = flatpickr(".c_clock", {
        locale : 'th',
        defaultDate	: date,
        disable: [
            @foreach($holiday as $rs)
                '{{ $rs->date }}',
             @endforeach
            ,
            function(date) {
                return (date.getDay() === 0)
            }
        ]
    });

    $('.clock_in').on('click',function(){
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: "/clocktime",
            data: {id : id},
            success: function(rs){
                swal("Success!", "บันทึกการเข้างานเรียบร้อยแล้ว" , "success", {button:false} );
                setTimeout(function() {
                    location.reload();
                }, 1200);
			}
		});
    });

    $('.clock_out').on('click',function(){
        var id     = $(this).data('id');
        var page   = "clocktime";
        var out_h  = '{{ $out->hour }}';
        var out_m  = '{{ $out->minute }}';
        var time   = new Date().getTime();
        var set    = new Date().setHours(out_h, out_m);
        if(time < set) {
            $('#create').modal('show').find('.id').val(id);
            $('#create').modal('show').find('.page').val(page);
        } else {
            swal({
                title: "Are you sure?",
                text: "ยืนยันการบันทึกเวลาออกงาน !!",
                icon: "warning",
                buttons:  ["ยกเลิก", "ตกลง"],
                dangerMode: true,
                })
                .then(willConfirm => {
                if (willConfirm) {
                    $.ajax({
                        type: "PUT",
                        url: "/clocktime",
                        data: {id : id},
                        success: function(msg){
                            swal("Success!", "บันทึกการออกงานเรียบร้อยแล้ว", "success",{button:false});
                            setTimeout(function() {
                                location.reload();
                            }, 1200);
                        }
                    });
                }
            });
        }
    });


    (function () {
        function checkTime(i) {
            return (i < 10) ? "0" + i : i;
        }
        function startTime() {
            var today = new Date(),
                h = checkTime(today.getHours()),
                m = checkTime(today.getMinutes()),
                s = checkTime(today.getSeconds());
           $('#h').html(h);
           $('#m').html(m);
           $('#s').html(s);
            t = setTimeout(function () {
                startTime()
            }, 500);
        }
        startTime();
    })();

    </script>

@endpush
{{--  toDateString() toTimeString() diffForHumans() format('H:i:s') --}}
