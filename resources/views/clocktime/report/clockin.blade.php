<!-- Modal -->
<div id="clock_in" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top:10%">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title font" style="font-size:22px"> แก้ไขเวลาเข้างาน </h4>
      </div>
      <div class="modal-body">
      {!! Form::open(['url' => '/clocktime/edit']) !!}
      {!! Form::hidden('clock', 'clock_in') !!}
      {!! Form::hidden('id', NULL, ['class'=>'id']) !!}
      {!! Form::date('c_clock', NULL , ['class'=>'form-control c_clock c_in']) !!}
      {!! Form::time('c_time',  NULL , ['class'=>'form-control c_time  c_in_time']) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
