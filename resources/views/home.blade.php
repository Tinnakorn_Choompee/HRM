@extends('layouts.app')
@section('title', 'หน้าหลัก')
@section('styles')
  <!-- Morris chart -->
  {{ Html::style('plugins/morris/morris.css') }}
  <!-- jvectormap -->
  {{ Html::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}
  <!-- Date Picker -->
  {{ Html::style('plugins/datepicker/datepicker3.css') }}
  <!-- Daterange picker -->
  {{ Html::style('plugins/daterangepicker/daterangepicker.css') }}
  <!-- bootstrap wysihtml5 - text editor -->
  {{ Html::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
  <!--  jquery-ui  -->
  {{ Html::style('css/Backend/dist/css/jquery-ui.min.css') }}
@endsection
@section('content')
@include('layouts.Backend.content')
@endsection
@push('scripts')
  <!-- jQuery UI 1.11.4  DeshBoard-->
  {{ Html::script('js/Backend/js/jquery-ui.min.js') }}
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  {{ Html::script('js/Backend/js/ripples.min.js') }}
  <!-- Morris.js charts -->
  {{ Html::script('js/Backend/js/raphael.min.js') }}
  {{ Html::script('plugins/morris/morris.min.js') }}
  <!-- Sparkline -->
  {{ Html::script('plugins/sparkline/jquery.sparkline.min.js') }}
  <!-- jvectormap -->
  {{ Html::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}
  {{ Html::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}
  <!-- jQuery Knob Chart -->
  {{ Html::script('plugins/knob/jquery.knob.js') }}
  <!-- daterangepicker -->
  {{ Html::script('js/Backend/js/moment.min.js') }}
  {{ Html::script('plugins/daterangepicker/daterangepicker.js') }}
  <!-- datepicker -->
  {{ Html::script('plugins/datepicker/bootstrap-datepicker.js') }}
  <!-- Bootstrap WYSIHTML5 -->
  {{ Html::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
@endpush



