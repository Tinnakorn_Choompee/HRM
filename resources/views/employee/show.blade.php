@extends('layouts.app')
@section('title', 'ข้อมูลพนักงาน')
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'ข้อมูลพนักงาน'])
        <!-- Main content -->
        <section class="content">

        <div class="row">
            <div class="col-md-3 col-xs-12">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                    {{ Html::image('images/employee/'.$employee->image , $employee->image , ['class'=>'profile-user-img img-responsive img-circle', 'style'=>"margin-top:15px"]) }}
                    <h3 class="profile-username text-center font" style="font-size:26px;margin-top:20px"> {{ $data['prename'][$employee->prename] }} {{ $employee->name }} {{ $employee->surname }} </h3>
                    <p class="text-muted text-center font" style="font-size:24px"> {{ $employee->position }} </p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item" style="margin-top:20px">
                            <b class="font" style="font-size:20px;font-weight:normal;">การมาทำงาน</b> <a class="pull-right"> {{ $come }}</a>
                        </li>
                        <li class="list-group-item" style="margin-top:20px">
                            <b class="font" style="font-size:20px;font-weight:normal;">การมาสาย</b> <a class="pull-right"> {{ $late }} </a>
                        </li>
                        <li class="list-group-item" style="margin-top:20px">
                            <b class="font" style="font-size:20px;font-weight:normal;">การขาด</b> <a class="pull-right"> {{ $absence }} </a>
                        </li>
                        <li class="list-group-item" style="margin-top:20px">
                            <b class="font" style="font-size:20px;font-weight:normal;">การลา</b> <a class="pull-right"> {{ $leave }} </a>
                        </li>
                    </ul>
                    <a href="/work/{{ $employee->code }}" class="btn btn-primary btn-raised btn-block font"><b style="font-size:22px;font-weight:normal;">ดูบันทึกการขาดลามาสาย</b></a>
                    {{--    --}}
                    </div>

                    <div style="margin-top:30px"></div>

                    <!-- /.box-body -->
                        </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title font"> ข้อมูลพนักงาน </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <br>
                        <strong style="margin-top:10px"><i class="fa fa-address-card-o"></i> เลขที่พนักงาน </strong> {{ sprintf('%04d', $employee->code) }}
                    <hr>
                    <br>
                        <strong style="margin-top:50px"><i class="fa fa-user-circle"></i> พนักงาน </strong> {{ $data['type'][$employee->type] }}
                    <hr>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
            <!-- /.col -->
            <div class="col-md-9 col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">ข้อมูลส่วนตัว</a></li>
                {{--  <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
                <li><a href="#settings" data-toggle="tab">Settings</a></li>  --}}
                </ul>
                <div class="tab-content">

                <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="user-block" style="margin-top:10px">
                                    <span class="font">
                                        <i class="fa fa-id-card fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> เลขบัตรประจำตัวประชาชน </a>
                                    </span>
                                </div>
                                <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $employee->card_id }}
                                </p>
                            </div>
                            <div class="col-md-6">
                                <div class="user-block" style="margin-top:10px">
                                    <span class="font">
                                        <i class="fa fa-phone fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> เบอร์โทรศัพท์ </a>
                                    </span>
                                </div>
                                <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $employee->tel }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- /.post -->
                    <div class="post">
                            <div class="user-block" style="margin-top:10px">
                                <span class="font">
                                    <i class="fa fa-book fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> ข้อมูลการศึกษา </a>
                                </span>
                            </div>
                            <!-- /.user-block -->
                            <p class="text-muted font" style="font-size:22px"> สถานที่จบการศึกษา <span style="margin-left:15px"> {{ $education->graduate }} </span></p>
                            <p class="text-muted font" style="font-size:22px"> สาขา <span style="margin-left:15px"> {{ $education->study }} </span> </p>
                            <p class="text-muted font" style="font-size:22px"> วุฒิการศึกษา  <span style="margin-left:15px"> {{ $data['education'][$education->education] }}</span> </p>
                            <p class="text-muted font" style="font-size:22px"> เกรดเฉลี่ย <span style="margin-left:15px"> {{ $education->grade }} </span> </p>
                    </div>
                    <!-- /.post -->
                    <div class="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="user-block" style="margin-top:10px">
                                        <span class="font">
                                            <i class="fa fa-calendar fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> วันเกิด </a>
                                        </span>
                                    </div>
                                    <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $employee->birthday }}  <span style="margin-left:10px;"> อายุ {{ $employee->age }} ปี</span>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <div class="user-block" style="margin-top:10px">
                                    <span class="font">
                                        <i class="fa fa-envelope-o fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> อีเมล์ </a>
                                    </span>
                                </div>
                                <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $user->email }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- /.post -->
                    <div class="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="user-block" style="margin-top:10px">
                                    <span class="font">
                                        <i class="fa fa-map-marker fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> ที่อยู่ </a>
                                    </span>
                                </div>
                                <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $employee->address }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- /.post -->
                    <div class="post">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="user-block" style="margin-top:10px">
                                    <span class="font">
                                        <i class="fa fa-snowflake-o fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> กรุ๊ปเลือด </a>
                                    </span>
                                    </div>
                                <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $data['blood'][$employee->blood] }}
                                </p>
                            </div>
                            <div class="col-md-3">
                                <div class="user-block" style="margin-top:10px">
                                    <span class="font">
                                        <i class="fa fa-flag fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> เชื่อชาติ </a>
                                    </span>
                                    </div>
                                     <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $data['race'][$employee->race] }}
                                </p>
                            </div>
                            <div class="col-md-3">
                                    <div class="user-block" style="margin-top:10px">
                                        <span class="font">
                                            <i class="fa fa-id-badge fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> สัญชาติ </a>
                                        </span>
                                        </div>
                                    <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $data['nationality'][$employee->nationality] }}
                                </p>
                            </div>
                            <div class="col-md-3">
                                    <div class="user-block" style="margin-top:10px">
                                        <span class="font">
                                            <i class="fa fa-university fa-2x"></i> <a style="font-size:25px;margin-left:10px;"> ศาสนา </a>
                                        </span>
                                        </div>
                                    <!-- /.user-block -->
                                <p style="font-size:16px">
                                    {{ $data['religion'][$employee->religion] }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
     
                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
@include('leave.create', ['leavetype' => $leavetype])
@endsection
@push('scripts')
  {{ Html::script('plugins/flatpickr/flatpickr.min.js') }}
  {{ Html::script('plugins/flatpickr/rtl/th.js') }}
    @if (session('leave'))
    <script>
     swal("Success!", "ทำการลางานเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('update'))
    <script>
       swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif

  <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $( ".radio-half" ).click(function() {
        $( ".full" ).hide();
        $( ".half" ).show();
    });

     $( ".radio-full" ).click(function() {
        $( ".full" ).show();
        $( ".half" ).hide();
    });

    $('#create').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id     = button.data('id')
        var page   = "leave";
        var title  = "การขอลางาน";
        $(this).find('.modal-title').html(title)
        $(this).find('.modal-body .id').val(id)
        $(this).find('.modal-body .page').val(page)
    });

    var date = new Date();
    const c_clock = flatpickr(".c_clock", {
        locale : 'th',
        defaultDate	: date,
        disable: [
            @foreach($holiday as $rs)
                '{{ $rs->date }}',
             @endforeach
            ,
            function(date) {
                return (date.getDay() === 0)
            }
        ]
    });
  </script>
@endpush
