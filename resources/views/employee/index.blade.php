@extends('layouts.app') 
@section('title', 'จัดการพนักงาน')
@section('styles')
    <!-- DataTables -->
    {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'จัดการพนักงาน'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >ตารางพนักงาน</h2>
                            <a href="/employee/create" class="btn btn-lg btn btn-info btn_employee font" style="font-size:22px">
                                <i class="ion ion-person-add" style="margin-right:10px;"></i> เพิ่มพนักงาน
                            </a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="employee" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="10%">รหัส</th>
                                        <th width="15%">พนักงาน</th>
                                        <th width="15%">ประเภท</th>
                                        <th width="15%">ตำแหน่ง</th>
                                        <th width="20%">รูปพนักงาน</th>
                                        <th width="25%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($employee as $k => $rs)
                                    <tr>
                                        <td class="text-center">{{ ++$k }}</td>
                                        <td class="text-center">{{ $rs->code }}</td>
                                        <td width="22%">{{ $data['prename'][$rs->prename] }} {{ $rs->name }} {{ $rs->surname }}</td>
                                        <td class="text-center">{{ $data['type'][$rs->type] }}</td>
                                        <td class="text-center">{{ $rs->position }}</td>
                                        <td width="20%" class="text-center">{{ Html::image('images/employee/'.$rs->image, $rs->image , ['width'=>'90','height'=>'80','class'=>'rounded']) }}</td>
                                        <td width="15.1%">
                                            @if(Auth::user()->hasRole('Manager'))
                                            <a href="/employee/{{$rs->id}}"  data-toggle="tooltip" data-placement="bottom" class="btn btn-block btn-primary left" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            @endif
                                            @if(Auth::user()->hasRole('Admin'))
                                            <a href="/employee/{{$rs->id}}" data-toggle="tooltip" data-placement="bottom" class="btn btn-primary left" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            <a href="/employee/{{$rs->id}}/edit" data-toggle="tooltip" data-placement="bottom" class="btn btn-warning left" title="แก้ไขข้อมูล"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
                                            {{ Form::open(['url' => ['employee', $rs->id], 'method'=>'DELETE', 'class' => 'left', 'id' => 'frm-delete'.$rs->id]) }}
                                                {{ Form::button('<i class="fa fa-trash"></i>',
                                                    ['type' => 'button', 'class'=>'btn btn-danger btn-del',
                                                    'data-id' => $rs->id,
                                                    'data-toggle' => 'tooltip',
                                                    'data-placement' => 'bottom',
                                                    'title'=>'ลบข้อมูล'])
                                                }}
                                            {{ Form::close() }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="10%">รหัส</th>
                                        <th width="15%">พนักงาน</th>
                                        <th width="15%">ประเภท</th>
                                        <th width="15%">ตำแหน่ง</th>
                                        <th width="20%">รูปพนักงาน</th>
                                        <th width="25%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')

    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
        <script>
            swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif
    <script>

    $('.btn-del').on('click',function(e){
       e.preventDefault();
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะ ลบ พนักงานนี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) {
                 $("#frm-delete"+id).submit()
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();
    $(function () {
        $('#employee').DataTable({ "dom": '<"right"f> rt <"right"p> <"clear">'});
    });
    </script>
@endpush

    {{--  $(".checkbox-toggle").click(function () {
      $(".table-hover input[type='checkbox'], .check-all input[type='checkbox']").prop('checked', $(this).is(':checked'));
    });  --}}
