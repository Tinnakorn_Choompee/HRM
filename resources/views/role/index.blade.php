@extends('layouts.app')
@section('title', 'ผู้ใช้งานระบบ')
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'ผู้ใช้งานระบบ'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >ตารางผู้ใช้งานระบบ</h2>
                            <a href="/user/create" class="btn btn-lg btn-info btn_employee font" style="font-size:22px">
                                <i class="ion ion-android-person-add" style="margin-right:10px;"></i> เพิ่มผู้ใช้งานระบบ
                            </a>
                        </div>
                        <!-- /.box-header -->
                        {{--  @if ($errors->any())
                            @foreach ($errors->all() as $error)
                            <div class="box-body">
                                <div class="callout callout-danger">
                                    <p class="font text-center" style="font-size:25px;font-weight: normal;"> {{ $error }} </p>
                                </div>
                            </div>
                            @endforeach
                        @endif  --}}
                        <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="20%">ชื่อ</th>
                                        <th width="15%">ชื่อเช้าใช้ระบบ</th>
                                        <th width="20%">อีเมล์</th>
                                        <th width="15%">ประเภทผู้ใช้ระบบ</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:20px">
                                @foreach($user as $k => $rs)
                                    <tr class="text-center">
                                        <td>{{ ++$k }}</td>
                                        <td>{{ $rs->name }}</td>
                                        <td>{{ $rs->username }}</td>
                                        <td>{{ $rs->email }}</td>
                                        {{--  <td>
                                            <input type="checkbox" {{ $rs->hasRole('User') ? 'checked' : '' }} name="role_user">
                                            <input type="checkbox" {{ $rs->hasRole('Admin') ? 'checked' : '' }} name="role_author">
                                            <input type="checkbox" {{ $rs->hasRole('Manager') ? 'checked' : '' }} name="role_admin">
                                        </td>  --}}
                                        <td class="text-center">
                                            <button class="btn btn-warning btn-edit" data-id="{{ $rs->id }}" data-date="{{ $rs->date }}" data-name="{{ $rs->name }}" data-toggle="modal" data-target="#edit" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></button>
                                            <button class="btn btn-danger  btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="20%">ชื่อ</th>
                                        <th width="15%">ชื่อเช้าใช้ระบบ</th>
                                        <th width="20%">อีเมล์</th>
                                        <th width="15%">ประเภทผู้ใช้ระบบ</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#edit').on('show.bs.modal', function (event) {
        var button  = $(event.relatedTarget)
        var id      = button.data('id')
        var date    = button.data('date')
        var name    = button.data('name')
        $(this).find('.modal-body #id').val(id)
        $(this).find('.modal-body #date').val(date)
        $(this).find('.modal-body #name').val(name)
    });

    $('.btn-del').on('click',function(){
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ วันหยุด นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) {
                $.ajax({
                    type: "DELETE",
                    url: "/holiday",
                    data: {id : id},
                    success: function(msg){
                        swal("Success!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success",{button:false});
                        setTimeout(function() {
                            location.reload();
                        }, 1000);
        				    }
        			    });
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
