@extends('layouts.app')
@section('title','Login');
@section('login')
<div class="login-box">
  <div class="login-logo">
      <p>ระบบบริหารจัดการ</p>
      <p>พนักงานร้านรุ่งเรืองทรัพย์</p>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">เข้าสู่ระบบ</p>
	{!! Form::open(['url' => 'login']) !!}
    {!! csrf_field() !!}
        <div class="form-group has-feedback {{ $errors->has('username') ? ' has-error' : '' }}">
            {{ Form::text('username', old('username') , ['id'=>'username', 'class'=>'form-control', 'placeholder'=>'Username', 'required', 'autofocus','title'=> 'กรอก Username เข้าใช้งาน', 'oninvalid'=>"this.setCustomValidity('กรุณากรอก Username เข้าใช้งาน')",'oninput'=>"setCustomValidity('')"] ) }}
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        {{--  custom match error  C:\xampp\htdocs\hrm\resources\lang\en\auth.php  --}}
        @if ($errors->has('username'))
            <span class="help-block text-danger">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif  
        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password', ['id'=>'password','class' => 'form-control', 'placeholder'=>'Password', 'required', 'title'=> 'กรอก Password เข้าใช้งาน', 'oninvalid'=>"this.setCustomValidity('กรุณากรอก Password เข้าใช้งาน')",'oninput'=>"setCustomValidity('')"]) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        @if ($errors->has('password'))
            <span class="help-block text-danger">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      <div class="row">
   
        <div class="col-xs-12">
          {{ Form::submit('LOGIN', ['class'=>'btn btn-primary btn-raised btn-block btn-flat']) }}
        </div>
      </div>
   	{!! Form::close() !!}
  </div>
</div>
@endsection
