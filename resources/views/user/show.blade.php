@extends('layouts.app') 
@section('title', 'ข้อมูลผู้ใช้ระบบ') 
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'ข้อมูลผู้ใช้ระบบ'])
        <!-- Main content -->
        <section class="content">

                <div class="row">
                  <div class="col-md-3">
          
                    <!-- Profile Image -->
                    <div class="box box-primary">
                      <div class="box-body box-profile">
                        {{ Html::image('images/user/'.$user->image , $user->image , ['class'=>'profile-user-img img-responsive img-circle', 'style'=>"margin-top:15px"]) }}
                        <h3 class="profile-username text-center font" style="font-size:26px;margin-top:20px"> {{ $user->name }} </h3>
                        <p class="text-muted text-center font" style="font-size:24px;padding-bottom:12px;"> {{ $user->hasRole('Admin') ?  "ผู้ดูแลระบบ" : "ผู้บริหาร" }} </p>
          
                        {{--  <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <b>Followers</b> <a class="pull-right">1,322</a>
                          </li>
                          <li class="list-group-item">
                            <b>Following</b> <a class="pull-right">543</a>
                          </li>
                          <li class="list-group-item">
                            <b>Friends</b> <a class="pull-right">13,287</a>
                          </li>
                        </ul>
          
                        <a href="#" class="btn btn-primary btn-raised btn-block"><b>Follow</b></a>  --}}
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <!-- About Me Box -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                       <h3 class="box-title font">ข้อมูลส่วนตัว</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <strong><i class="fa fa-envelope-o margin-r-5"></i> Email</strong>
          
                        <p class="text-muted">
                          {{ $user->email }}
                        </p>
          
                        <hr>
          
                        <strong><i class="fa fa-user-o margin-r-5"></i> Username </strong>
          
                        <p class="text-muted"> {{ $user->username }}</p>
          
                        <hr>
                       
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
             
                </div>
                <!-- /.row -->
          
            </section>
    </div>
@endsection 
   
   
