@extends('layouts.app') 
@section('title', 'แก้ไขผู้ใช้งานระบบ') 
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	@include('layouts.Backend.breadcrumb', ['title'=>'แก้ไขผู้ใช้งานระบบ'])
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12 col-xs-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
                        {{ Html::image('images/user/'.$user->image, $user->image , ['width'=>'90','height'=>'80','class'=>'rounded']) }}
						<h3 class="box-title font" style="margin-left:20px">ข้อมูลผู้ใช้งานระบบ</h3>
					</div>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<!-- /.box-header -->
					<!-- form start -->
                    {!! Form::model($user, ['url' => ['user', $user->id], 'method' => 'PUT', 'files'=> TRUE])  !!}
                    {!! Form::hidden('id', $user->id) !!}
					<div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('name', 'ชื่อ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('name', NULL, ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('email', 'อีเมล์', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::email('email', NULL, ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('username', 'ชื่อเข้าใช้ระบบ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('username', NULL, ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('password', 'พาสเวิร์ด', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::password('password', ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group is-fileinput">
                                {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::hidden('edit_image', $user->image) !!}
                                    {!! Form::text( NULL,  NULL, ['class'=>'form-control','readonly', 'placeholder'=>'เลือกรูปภาพ']) !!}
                                    {!! Form::file('image', ['class'=>'form-control']); !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('role', 'ประเภทผู้ใช้ระบบ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    <div class="radio">
                                        {{--  <label> {!! Form::radio('type', '1', true) !!} ผู้ใช้งาน </label>  --}}
                                        <label style="margin-right:30px"> 
                                            {!! Form::radio('role', '2', $user->hasRole('Admin')   ? TRUE : FALSE) !!} ผู้ดูแลระบบ 
                                        </label>
                                        <label style="margin-right:30px"> 
                                            {!! Form::radio('role', '3', $user->hasRole('Manager') ? TRUE : FALSE) !!} ผู้บริหาร
                                         </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary label_font"> บันทึก </button>
					</div>
					{!! Form::close() !!}
				</div>
				<!-- /.box -->
			</div>
			<!-- /.row -->
	</section>
    <!-- /.content -->
</div>
@endsection 