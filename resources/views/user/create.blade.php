@extends('layouts.app') 
@section('title', 'เพิ่มผู้ใช้งานระบบ') 
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	@include('layouts.Backend.breadcrumb', ['title'=>'เพิ่มผู้ใช้งานระบบ'])
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12 col-xs-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title font">ข้อมูลผู้ใช้งานระบบ</h3>
					</div>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<!-- /.box-header -->
					<!-- form start -->
					{!! Form::open(['url' => 'user', 'files'=>TRUE]) !!}
					<div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('name', 'ชื่อ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('name', NULL, ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('email', 'อีเมล์', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::email('email', NULL, ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('username', 'ชื่อเข้าใช้ระบบ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('username', NULL, ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('password', 'พาสเวิร์ด', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::password('password', ['class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group is-fileinput">
                                {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text( NULL,  NULL, ['class'=>'form-control','readonly', 'placeholder'=>'เลือกรูปภาพ']) !!}
                                    {!! Form::file('image', ['class'=>'form-control']); !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('radio_role', 'ประเภทผู้ใช้ระบบ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    <div class="radio">
                                        {{--  <label> {!! Form::radio('type', '1', true) !!} ผู้ใช้งาน </label>  --}}
                                        <label style="margin-right:30px"> 
                                            {!! Form::radio('role', '2', true) !!} ผู้ดูแลระบบ 
                                        </label>
                                        <label style="margin-right:30px"> 
                                            {!! Form::radio('role', '3', false) !!} ผู้บริหาร
                                         </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary label_font"> บันทึก </button>
					</div>
					{!! Form::close() !!}
				</div>
				<!-- /.box -->
			</div>
			<!-- /.row -->
	</section>
    <!-- /.content -->
</div>
@endsection 