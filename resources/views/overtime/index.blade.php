@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app') 
@section('title', 'การทำงานล่วงเวลา') 
@section('styles')
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'การทำงานล่วงเวลา'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" > การทำงานล่วงเวลา </h2>
                            <a href="/overtime/create" class="btn btn-lg btn-add btn btn-info btn_employee font" style="font-size:22px"> 
                                <i class="ion ion-android-add-circle" style="margin-right:10px;"></i> เพิ่มการทำงานล่วงเวลา
                            </a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="15%">วันที่</th>
                                        <th width="25%">ชม.การทำงานล่วงเวลา</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:20px">
                                @foreach($overtime as $k => $rs)
                                    <tr>
                                        <td class="text-center">{{ ++$k }}</td>
                                        <td class="text-center">{!! DateThaiLibrary::ThaiDate($rs->date) !!}</td>
                                        <td class="text-center">{{ date('H:i', strtotime($rs->time)) }}</td>
                                        <td class="text-center">
                                            <a href="/overtime/{{ $rs->id }}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="ดูข้อมูล"><i class="fa fa-eye"></i></a>  
                                            <a href="/overtime/{{ $rs->id }}/edit" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>  
                                            <button class="btn btn-danger  btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button> 
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="15%">วันที่</th>
                                        <th width="25%">ชม.การทำงานล่วงเวลา</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <p id="message"></p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }} 
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}
     <!-- page script -->
     @if (session('success'))
     <script>
         swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
     </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
        <script>
            swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif
    <script>
        
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-del').on('click',function(){
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ การทำงานล่วงเวลา นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) { 
                $.ajax({
                    type: "DELETE",
                    url: "/overtime/"+id,
                    success: function(msg){
                        swal("Success!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success",{button:false});                       
                        setTimeout(function() {
                            location.reload(); 
                        }, 1000); 
				    }
			    });
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip(); 

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>

@endpush