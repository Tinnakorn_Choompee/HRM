@extends('layouts.app') 
@section('title', 'เพิ่มการทำงานล่วงเวลา') 
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }} 
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'เพิ่มการทำงานล่วงเวลา'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        <h3 class="box-title font">ข้อมูลการทำงานล่วงเวลา</h3>
                        </div>  
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li class="font" style="font-size:20px">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['url' => 'overtime']) !!}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('date', 'วันที่', ['class'=>'label_font col-sm-4 text-right']) !!}
                                            <div class="col-sm-4">
                                                {!! Form::date('date', NULL , ['class'=>'form-control c_clock c_in' , 'required']) !!}                                     
                                            </div>
                                        </div>  
                                    </div>  
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('time', 'ชม.การทำงานล่วงเวลา', ['class'=>'label_font col-sm-4 text-right']) !!}
                                            <div class="col-sm-4">
                                                {!! Form::time('time',  NULL , ['class'=>'form-control c_time  c_in_time', 'required']) !!}                             
                                            </div>
                                        </div> 
                                    </div> 
                                </div>   

                                <div class="row">
                                    <div class="col-md-offset-3 col-md-6">
                                        <hr>
                                        <span class="text-blue label_font"> <i class="ion ion-person-stalker"></i> รายชื่อพนักงาน </span>
                                        <span class="text-blue label_font pull-right" style="margin-right:8px"> ตำแหน่ง </span>
                                    </div> 
                                </div> 

                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($employee as $em)
                                            <div class="col-md-6 col-md-offset-3">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="id[]" value="{{ $em->id }}">
                                                            {{ $data['prename'][$em->prename] }} {{ $em->name }} {{ $em->surname }}
                                                        </label>
                                                        <span class="pull-right"> {{ $em->position }} </span>
                                                    </div> 
                                                </div> 
                                            </div>
                                         @endforeach
                                    </div>                       
                                </div> 
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary label_font"> บันทึก </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
            </div><!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')
    <!-- flatpickr -->
    {{ Html::script('plugins/flatpickr/flatpickr.min.js') }}
    {{ Html::script('plugins/flatpickr/rtl/th.js') }}
    
    <script>

    const c_clock = flatpickr(".c_clock", {
        locale : 'th',
        defaultDate: ["{{ $date }}"],
        disable: [
            @foreach($holiday as $rs)
                '{{ $rs->date }}',
             @endforeach
            ,
            function(date) {
                return (date.getDay() === 0)
            }
        ],
    });

    const c_time  = flatpickr(".c_time", {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        defaultDate: "00:00",
        minDate: "00:00",
        time_24hr: true
    });

    </script>
@endpush