@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app') 
@section('title', 'การทำงานล่วงเวลา') 
@section('styles')

@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'การทำงานล่วงเวลา'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        <h3 class="box-title font">ข้อมูลการทำงานล่วงเวลา</h3>
                        </div>  
                        <!-- /.box-header -->
                   
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-1 col-md-offset-3">
                                      <span class="font" style="font-size:25px"> วันที่ </span>     
                                    </div> 
                                    <div class="col-md-2">
                                        <span class="font" style="font-size:25px"> {!! DateThaiLibrary::ThaiDate($overtime->date) !!} </span> 
                                    </div>  
                                    <div class="col-md-2">
                                        <span class="font pull-right" style="font-size:25px"> ชม.การทำงานล่วงเวลา </span>     
                                    </div> 
                                    <div class="col-md-1">
                                        <span class="font pull-right" style="font-size:25px"> {{ date('H:i', strtotime($overtime->time)) }} </span> 
                                    </div>
                                </div>   

                                <div class="row">
                                    <div class="col-md-offset-3 col-md-6">
                                        <hr>
                                        <span class="text-blue label_font"> <i class="ion ion-person-stalker"></i> รายชื่อพนักงาน </span>
                                        <span class="text-blue label_font pull-right" style="margin-right:8px"> ตำแหน่ง </span>
                                    </div> 
                                </div> 

                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($employee as $em)
                                            <div class="col-md-6 col-md-offset-3">
                                            @if (in_array($em->id, $detail))
                                                <p class="font" style="font-size:22px"> {{ $data['prename'][$em->prename] }} {{ $em->name }} {{ $em->surname    }}<span class="pull-right"> {{ $em->position }}</span></p>
                                            @endif
                                            </div>
                                         @endforeach
                                    </div>                       
                                </div> 
                            </div>
 
                            <!-- /.box-body -->
                            <hr>
                            <br>
                    </div>
                    <!-- /.box -->
            </div><!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')

@endpush