<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ร้านรุ่งเรืองทรัพย์</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{ asset('images/human.ico') }}" type="image/x-icon" />
        <!-- Bootstrap CDN -->
        {{ Html::style('css/Frontend/bootstrap.min.css') }}
        {{ Html::style('css/Frontend/animate.css') }}
        {{ Html::style('css/Frontend/style.css') }}
    </head>
    <body>
        @yield('content')
        @include('layouts.Frontend.header')
        @include('layouts.Frontend.employee')
        @include('layouts.Frontend.about')
        @include('layouts.Frontend.footer')
        <!-- Js -->
        {{ Html::script('js/jquery-3.2.1.min.js') }}
        {{ Html::script('js/Frontend/popper.js') }}
        {{ Html::script('js/Frontend/bootstrap.min.js') }}
        {{ Html::script('js/Frontend/main.js') }}
    </body>
</html>