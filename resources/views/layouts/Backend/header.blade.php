<header class="main-header">
	<!-- Logo -->
	<a href="/home" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini">R<b>R</b>S</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg">Roong<b>Ruang</b>Sarp</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="/images/user/{{Auth::user()->image}}" class="user-image" alt="User Image">
						<span class="hidden-xs">{{ Auth::user()->name }}</span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="/images/user/{{Auth::user()->image}}" class="img-circle" alt="User Image">
							<p class="font" style="font-size:25px">
								{{ Auth::user()->name }}
								<br>
								<small style="font-size:20px;"> {{ Auth::user()->email }}</small>
							</p>
						</li>
						<!-- Menu Body -->
						<li class="user-body">
							<button data-id="{{ Auth::user()->id }}" data-toggle="modal" data-target="#change_password" class="btn btn-block btn-info font" style="font-size:20px"> เปลี่ยนรหัสผ่าน </button>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								@if(Auth::user()->hasRole('User'))
									<a href="/employee/profile/{{ Auth::user()->username }}" class="btn btn-default btn-flat">Profile</a>
								@endif
								@if(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Manager'))
									<a href="/user/profile/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Profile</a>
								@endif
							</div>
							<div class="pull-right">
								<a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
								{!! Form::open(['url' => 'logout', 'id'=>'logout-form']) !!} {!! Form::close() !!}
							</div>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button -->
				{{--  <li>
					<a href="#" data-toggle="control-sidebar">
						<i class="fa fa-gears"></i>
					</a>
				</li>  --}}
			</ul>
		</div>
	</nav>
</header>

<!-- Modal -->
<div id="change_password" class="modal fade" role="dialog">
	<div class="modal-dialog" style="margin-top:10%">
		  <!-- Modal content-->
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title font" style="font-size:22px"> เปลี่ยนรหัสผ่าน
			<span class="pull-right" id="alert" style="margin-right:20px">
				@if ($errors->has('password_user'))
				<span class="help-block text-danger" style="font-size:22px">
					{{ $errors->first('password_user') }}
				</span>
				@endif
			</span>
			</h4>
		</div>
		<div class="modal-body">
			{!! Form::open(['url' => '/user/password', 'method' => 'PUT', 'class'=>'form-horizontal']) !!}
			{!! Form::hidden('id', NULL, ['id'=>'id']) !!}
			<div class="form-group">
				{!! Form::label('password_user', 'รหัสผ่านใหม่', ['class'=>'col-sm-3 control-label label_font', 'style'=> 'top:-15px']) !!}
				<div class="col-sm-8">
					{!! Form::password('password_user', ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password_confirmation', 'ยืนยันรหัสผ่าน', ['class'=>'col-sm-3 control-label label_font', 'style'=> 'top:-15px']) !!}
				<div class="col-sm-8">
					{!! Form::password('password_confirmation', ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>
@push('scripts')
{{-- @if ($errors->any())
	@foreach ($errors->all() as $error)
		<script>
			$('#password').modal('show');
		</script>
    @endforeach
@endif --}}
@if ($errors->has('password_user'))
	<script>
		$('#change_password').modal('show');
	</script>
@endif

@if (session('password'))
<script>
	swal("Changed!", "ทำการเปลี่ยนแปลงพาสเวิร์ดเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
	$('#change_password').on('show.bs.modal', function (event) {
        var button  = $(event.relatedTarget)
        var id      = button.data('id')
        $(this).find('.modal-body #id').val(id)
	});
</script>
@endpush
