@php
use Libraries\MenuLibrary\MenuLibrary;
@endphp
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="/images/user/{{ Auth::user()->image }}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p class="font" style="font-size:20px">{{ Auth::user()->name }}</p>
			    <i class="fa fa-circle text-success"></i> Online
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search Menu">
				<span class="input-group-btn">
					<button type="submit" id="search-btn" class="btn btn-flat">
						<i style="padding:10px 10px;" class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		{{--  class = active  --}}
		<ul class="sidebar-menu">
			@if(Auth::user()->hasRole('Admin'))
			<li class="header">MENU</li>
				@foreach(MenuLibrary::Menu() as $v)
					@if($v['menu']['submenu'] == TRUE)
						<li class="treeview" class="active">
							<a href="#">
								<i class="ion {{ $v['menu']['icon'] }}"></i>
								<span> {{ $v['menu']['title'] }} </span>
							</a>
							<ul class="treeview-menu">
								@foreach($v['menu']['list'] as $k => $v)
								<li>
									<a href="{{ $v['url'] }}">
										<i class="fa fa-circle-o"></i> {{ $v['name'] }}</a>
								</li>
								@endforeach
							</ul>
						</li>
					@else
			
				    @if (str_contains($v['menu']['list']['name'], isset($_GET['q']) ? $_GET['q'] : NULL) === TRUE)
					<li class="active">
					@else
					<li>
					@endif					
					<a href="{{ $v['menu']['list']['url'] }}">
						<i class="ion {{ $v['menu']['list']['icon'] }}"></i>
						<span>{{ $v['menu']['list']['name'] }}</span>
						@if($v['menu']['list']['url'] == "/leave")
							@if($v['menu']['list']['alert'] != 0)
							<span class="pull-right-container">
								<span class="label label-primary pull-right">{{ $v['menu']['list']['alert'] }}</span>
							</span>
							@endif
						@endif
					</a>
					</li>
				{{--  {{ $v['menu']['b']['c'] }}  --}}
					@endif
				@endforeach
			@endif
			

			 @if(Auth::user()->hasRole('User'))
			 <li class="header">MENU</li>
			 <li>
				<a href="/employee/edit/{{ Auth::user()->username }}">
					<i class="fa fa-pencil"></i>
					<span>  แก้ไขข้อมูลพนักงาน  </span>
				</a>
			</li>

			 <li>
				<a href="/report/clocktime/{{ Auth::user()->username }}">
					<i class="ion ion-clipboard"></i>
					<span>  ประวัติการบันทึกเวลางาน  </span>
				</a>
			</li>
			<li>
				<a href="/work/{{ Auth::user()->username }}">
					<i class="ion ion-pricetags"></i>
					<span>  บันทึกขาดลามาสาย  </span>
				</a>
			</li>
			
			 <li class="header">LEAVE</li>
				 <li>
					 <a href="#" data-toggle="modal" data-target="#create" data-id="{{ Auth::user()->username }}">
						 <i class="ion ion-ios-compose-outline"></i>
						 <span>  การขอลางาน  </span>
					 </a>
				 </li>
				 <li>
					<a href="/leave/profile/{{ Auth::user()->username }}">
							<i class="ion ion-clipboard"></i>
							<span>  ประวัติการลางาน  </span>
						</a>
					</li>
			 @endif

			 @if(Auth::user()->hasRole('Manager'))
			 <li class="header">MENU</li>
			 <li>
				<a href="/employee">
					<i class="ion ion-person-stalker"></i>
					<span>  ข้อมูลพนักงาน  </span>
				</a>
			</li>
			 @foreach(MenuLibrary::Menu() as $v)
			 	@if($v['menu']['list']['url'] == "/leave")
				<li>
					<a href="{{ $v['menu']['list']['url'] }}">
						<i class="ion {{ $v['menu']['list']['icon'] }}"></i>
						<span>อนุมัติการลา</span>
						@if($v['menu']['list']['alert'] != 0)
							<span class="pull-right-container">
								<span class="label label-primary pull-right">{{ $v['menu']['list']['alert'] }}</span>
							</span>
						@endif
					</a>
				</li>
				@endif
			 @endforeach
				<li class="header">REPORT</li>
				@foreach(MenuLibrary::Report() as $v)
				<li>
					<a href="{{ $v['menu']['list']['url'] }}">
						<i class="ion {{ $v['menu']['list']['icon'] }}"></i>
						<span>{{  $v['menu']['list']['name'] }}</span>
					</a>
				</li>
				@endforeach
			 @endif

			@if(Auth::user()->hasRole('Admin'))
				<li class="header">SETTING</li>
				@foreach(MenuLibrary::Setting() as $v)
				@if (str_contains($v['menu']['list']['name'], isset($_GET['q']) ? $_GET['q'] : NULL) === TRUE)
				<li class="active">
				@else
				<li>
				@endif	
					<a href="{{ $v['menu']['list']['url'] }}">
						<i class="fa fa-circle-o text-aqua"></i>
						<span>{{  $v['menu']['list']['name'] }}</span>
					</a>
				</li>
				@endforeach
			@endif
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
