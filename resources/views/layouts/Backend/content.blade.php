@php
use Libraries\MenuLibrary\MenuLibrary;
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	@include('layouts.Backend.breadcrumb', ['title'=>'Dashboard'])
	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>{{ $work }}</h3>
						<p>การมาทำงาน</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-people"></i>
					</div>
					<a href="work/status/1" class="small-box-footer">More info
						<i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>{{ $leave }}</h3>
						<p>การลา</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-people"></i>
					</div>
					<a href="work/status/3" class="small-box-footer">More info
						<i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>{{ $late }}</h3>
						<p>การมาสาย</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-people"></i>
					</div>
					<a href="work/status/2" class="small-box-footer">More info
						<i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3>{{ $absence }}</h3>
						<p>การขาดงาน</p>
					</div>
					<div class="icon">
						<i class="ion ion-ios-people"></i>
					</div>
					<a href="work/status/0" class="small-box-footer">More info
						<i class="fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
			<!-- ./col -->
		</div>
		<!-- /.row -->
			<!-- Main row -->
			@if(Auth::user()->hasRole('Admin'))
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">MENU</h3>
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse">
									<i class="fa fa-minus"></i>
								</button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
							  @foreach(MenuLibrary::Menu() as $v)
								<div class="col-sm-6 col-md-3 animated fadeInRight employee">
									<div class="block text-center">
									    <a href="{{ $v['menu']['list']['url'] }}" class="hvr-pop">
										<div class="icon">
											<br>
											<i class="ion {{ $v['menu']['list']['icon'] }}" style="font-size:30px"></i> 
											<br> 
											<span class="font" style="font-size:20px"> {{  $v['menu']['list']['name'] }} </span>
											<br>	
											<br>											
										</div>
										</a>
									</div>
								</div>
							   @endforeach
								<!-- /.col -->
							</div>
							<!-- /.row -->
						</div>
						<!-- ./box-body -->
				
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			@endif

			@if(Auth::user()->hasRole('Admin'))
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">SETTING</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
								  @foreach(MenuLibrary::SETTING() as $v)
									<div class="col-sm-6 col-md-3 animated fadeInRight employee">
										<div class="block text-center">
											<a href="{{ $v['menu']['list']['url'] }}" class="hvr-pop">
											<div class="icon">
												<br>
												<i class="fa fa-circle-o text-aqua" style="fonxt-size:30px;"></i> 
												<br> 
												<span class="font" style="font-size:20px"> {{  $v['menu']['list']['name'] }} </span>
												<br>	
												<br>											
											</div>
											</a>
										</div>
									</div>
								   @endforeach
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- ./box-body -->
					
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
			<!-- /.row -->
			@endif

			@if(Auth::user()->hasRole('Manager'))
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">MENU</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
								@foreach(MenuLibrary::Menu() as $v)
									@if($v['menu']['list']['url'] == "/leave")
										<div class="col-sm-6 col-md-3 animated fadeInRight employee">
											<div class="block text-center">
												<a href="{{ $v['menu']['list']['url'] }}" class="hvr-pop">
												<div class="icon">
													<br>
													<i class="ion {{ $v['menu']['list']['icon'] }}" style="font-size:30px"></i> 
													<br> 
													<span class="font" style="font-size:20px"> อนุมัติการลา </span>
													<br>	
													<br>											
												</div>
												</a>
											</div>
										</div>
									@endif
								@endforeach
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- ./box-body -->
					
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
			@endif

			@if(Auth::user()->hasRole('Manager'))
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">REPORT</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
								  @foreach(MenuLibrary::REPORT() as $v)
									<div class="col-sm-6 col-md-3 animated fadeInRight employee">
										<div class="block text-center">
											<a href="{{ $v['menu']['list']['url'] }}" class="hvr-pop">
											<div class="icon">
												<br>
												<i class="ion {{ $v['menu']['list']['icon'] }} text-aqua" style="font-size:30px;"></i> 
												<br> 
												<span class="font" style="font-size:20px"> {{  $v['menu']['list']['name'] }} </span>
												<br>	
												<br>											
											</div>
											</a>
										</div>
									</div>
								   @endforeach
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- ./box-body -->
					
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
			<!-- /.row -->
			@endif

	</section>
	<!-- /.content -->
</div>
