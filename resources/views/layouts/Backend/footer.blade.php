<!-- /.content-wrapper -->
<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>RoongRuangSarp</b>
	</div>
	<strong>Copyright &copy; {{ date('Y') }} @ {{ Auth::user()->name }}</a>.</strong> All rights reserved.
</footer>