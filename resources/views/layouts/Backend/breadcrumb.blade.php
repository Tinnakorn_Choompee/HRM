<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:10px">
	<h1 class="font" style="font-size:35px">
		{{ $title }}
		<small style="font-size:30px"> ร้านรุ่งเรืองทรัพย์</small>
		@if($title != "Dashboard")
		<small class="font Breadcrumbs pull-right">
				{{ Breadcrumbs::render() }}
		</small>
		@endif
	</h1>
</section>
<div style=" clear: both;"></div>