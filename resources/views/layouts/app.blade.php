<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/human.ico') }}" type="image/x-icon" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Bootstrap 3.3.6 -->
    {{ Html::style('css/Backend/css/bootstrap.min.css') }}
    <!-- Font Awesome -->
    {{ Html::style('css/Backend/font-awesome/css/font-awesome.min.css') }}
    <!-- Ionicons -->
    {{ Html::style('css/Backend/ionicons/css/ionicons.min.css') }}
    <!-- AdminLTE style -->
    {{ Html::style('css/Backend/dist/css/AdminLTE.min.css') }}
    <!-- Material Design -->
    {{ Html::style('css/Backend/dist/css/bootstrap-material-design.min.css') }}
    <!-- Ripples -->
    {{ Html::style('css/Backend/dist/css/ripples.min.css') }}
    <!-- MaterialAdminLTE -->
    {{ Html::style('css/Backend/dist/css/MaterialAdminLTE.min.css') }}
    <!-- MaterialAdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    {{ Html::style('css/Backend/dist/css/skins/all-md-skins.min.css') }}
    <!-- Style -->
    {{ Html::style('css/Backend/css/style.css') }}
    <!-- Style -->
    {{ Html::style('plugins/hover-master/css/hover-min.css') }}
    <!-- Animate -->
    {{ Html::style('css/Frontend/animate.css') }}
    @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    @auth
        @include('layouts.Backend.header')
        @include('layouts.Backend.sidebarL')
        @yield('content')
        @include('layouts.Backend.sidebarR')
        @include('layouts.Backend.footer')
    @endauth
        @yield('login')
    </div>
    <!-- jQuery 3.2.1 -->
    {{ Html::script('js/jquery-3.2.1.min.js') }}
    <!-- Bootstrap 3.3.6 -->
    {{ Html::script('js/Backend/js/bootstrap.min.js') }}
    <!-- Material Design -->
    {{ Html::script('js/Backend/js/material.min.js') }}
    {{ Html::script('js/Backend/js/ripples.min.js') }}
    <!-- Sweetalert -->
    {{ Html::script('plugins/sweetalert/sweetalert.min.js') }}
    <script>
        $.material.init();
    </script>
    <!-- Slimscroll -->
    {{ Html::script('plugins/slimScroll/jquery.slimscroll.min.js') }}
    <!-- FastClick -->
    {{ Html::script('plugins/fastclick/fastclick.js') }}
    <!-- AdminLTE App -->
    {{ Html::script('js/Backend/dist/js/app.min.js') }}
    <!-- AdminLTE for demo purposes -->
    {{ Html::script('js/Backend/dist/js/demo.js') }}
    @stack('scripts')
</body>
</html>
