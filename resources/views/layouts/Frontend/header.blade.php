<!--
        Header start
        ==================== -->
<div class="container">
	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
		<a class="navbar-brand animated fadeInLeft header-name" href="#" id="logo">
			<div class="font_header"> RoongRuangSarp </div>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
		 aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav ml-auto navbar-header">
				<li class="nav-item active hvr-underline-from-center">
					<a class="nav-link" href="#" id="home">Home
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item hvr-underline-from-center">
					<a class="nav-link" href="#Time" id="time">Employee</a>
				</li>
				<li class="nav-item hvr-underline-from-center">
					<a class="nav-link" href="#About" id="btn-About">About</a>
				</li>
			</ul>
		</div>
	</nav>
</div>
{{-- infinite --}}
<div class="col-ms-9">
<br><br>
</div>


<section class="hero-area bg-1">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="block animated fadeIn" id="1">
					<div class="font">ระบบลงเวลาทำงาน</div>
					<div class="font_datail">ร้านรุ่งเรืองทรัพย์</div>
					<div class="animated fadeInDown" id="2">
						<a class="btn btn-home font_btn" href="#Time" role="button" id="btn-Time">พนักงาน</a>
					</div>
				</div>
			</div>
			<div class="col-md-5 animated zoomIn" id="3">
				<div class="block">
					<div class="counter text-center">
						<ul>
							<li>
								<div class="dash hours_dash">
									<div class="digit" id="h">0</div>
									<span class="dash_title">Hours</span>
								</div>
							</li>
							<li>
								<div class="dash minutes_dash">
									<div class="digit" id="m">0</div>
									<span class="dash_title">Minutes</span>
								</div>
							</li>

							<li>
								<div class="dash minutes_dash">
									<div class="digit" id="s">0</div>
									<span class="dash_title">Seconds</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- .row close -->
	</div>
	<!-- .container close -->
</section>
<!-- header close -->
