<!--
        Employee start
        ==================== -->
<div id="Time"></div><br>

<section class="service section">
	<div class="container">
		<div class="heading animated fadeInUp" id="employee_time">
			<span id="staff_header"> พนักงานร้านรุ่งเรืองทรัพย์ </span>
		</div>
		<div class="row">
			@foreach($employee as $rs)
			<div class="col-sm-6 col-md-3 animated fadeInRight employee">
				<div class="block">
					{{ Html::image('images/employee/'.$rs->image, $rs->image , ['width'=>'150','height'=>'150','class'=>'rounded']) }}
					<h3 style="margin-top:10px"> {{ $data['prename'][$rs->prename] }} {{ $rs->name }} {{ $rs->surname }} </h3>
					<h4> ตำแหน่ง </h4>
					<h5>{{ $rs->position }}</h5>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	<!-- .container close -->
</section>
<!-- #service close -->
