<!-- Modal -->
<div id="create" class="modal fade" role="dialog">
	<div class="modal-dialog" style="margin-top:10%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title font" style="font-size:22px"> เพิ่มวันหยุด </h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['url' => '/holiday', 'class'=>'form-horizontal']) !!}
					<div class="form-group">
						{!! Form::label('date', 'วันที่', ['class'=>'col-sm-3 control-label label_font']) !!}
						<div class="col-sm-9">
							{!! Form::date('date', "", ['class'=>'form-control date', 'required']) !!}
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('name', 'รายละเอียด', ['class'=>'col-sm-3 control-label label_font']) !!}
						<div class="col-sm-9">
							{!! Form::text('name', "", ['class'=>'form-control', 'required']) !!}
						</div>
					</div>
				<!-- /.box-body -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
