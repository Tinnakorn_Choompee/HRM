@extends('layouts.app')
@section('title', 'ใบสลิปเงินเดือน')
@section('styles')
    {{--  <!-- DataTables -->
    {{ Html::style('plugins/datatables/dataTables.bootstrap.css') }}   --}}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'ใบสลิปเงินเดือน'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;">ใบสลิปเงินเดือน ปี {{ $currentyear }} </h2>
                            <span class="btn_employee" style="margin-top:-20px;">
                              {!! Form::open(['url' => 'salary', 'method'=> 'GET', 'id' => 'years']) !!}
                              <div class="form-control">
                                {!! Form::select('year', $year , NULL, ['class'=>'form-control select_year', 'id'=> 'year', 'required']) !!}
                              </div>
                              {!! Form::close() !!}
                            </span>
                            <span class="pull-right" style="padding:25px 10px 0px 0px">
                              ปี   {{ Session::get('success') }}
                            </span>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                          <!-- START ACCORDION & CAROUSEL-->
                          <div class="row">
                            <div class="col-md-12">
                              <div class="box box-solid">
                                <div class="box-body">
                                  <div class="box-group" id="accordion">

                                    @foreach($month as $k => $m)

                                      @if($currentyear == $now->year + 543)
                                        @if($now->month >= $k)
                                          <div class="panel box box-primary">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$k}}" id="m_{{$k}}">
                                              <div class="box-header with-border">
                                                <h3 class="box-title font">
                                                  <span style="color:#0073b7"> เดือน {{ $m }} </span>
                                                </h3>
                                              </div>
                                            </a>
                                            <div id="collapse_{{$k}}" class="panel-collapse collapse">
                                              <div class="box-body">
                                                <ul class="list-unstyled" style="margin-left:15px">
                                                  @foreach($employee as $rs)
                                                    <li>
                                                      @switch($rs->type)
                                                          @case(1)
                                                          <span class="badge bg-blue" style="margin-right:20px;"> รายเดือน </span>
                                                          @break
                                                          @case(2)
                                                          <span class="badge bg-yellow" style="margin-right:20px;"> &nbsp;&nbsp;&nbsp;รายวัน&nbsp;&nbsp;&nbsp; </span>
                                                          @break
                                                      @endswitch
                                                      {{ $data['prename'][$rs->prename] }} {{ $rs->name }} {{ $rs->surname }} </li>
                                                    <ul>
                                                      @foreach ($week as $key => $value)
                                                        @if($key == $rs->id)
                                                            @if($value[$k][1] != 0)
                                                              <li>
                                                                <a href="/salary/slip/{{ $value[$k][1] }}" class="btn btn-default"> Week 1 </a>
                                                                <span class="text-green">
                                                                  <i class="ion ion-android-checkmark-circle text-green" style="font-size:18px"></i>
                                                                  กรอกข้อมูลเรียบร้อย
                                                                </span>

                                                                <span class="pull-right">
                                                                  <a class="btn btn-warning" href="/salary/{{$value[$k][1]}}/edit">
                                                                    <i class="ion ion-android-create"></i> แก้ไขข้อมูล
                                                                  </a>

                                                                  <a target="_blank" href="/salary/slip/{{ $value[$k][1] }}" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                    <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                  </a>
                                                                </span>
                                                              </li>
                                                            @else
                                                              <li>
                                                                <button class="btn btn-default" disabled> Week 1 </button>
                                                                <span class="text-red">
                                                                  <i class="ion ion-android-cancel text-red" style="font-size:18px"></i>
                                                                  กรอกข้อมูลให้ครบถ้วน
                                                                </span>
                                                                <span class="pull-right">
                                                                  <a class="btn btn-success" href="/salary/create?id={{ $rs->id }}&week=1&date={{ ($currentyear - 543)."-".sprintf('%02d', $k)."-01" }}">
                                                                    <i class="ion ion-android-add-circle"></i> เพิ่มข้อมูล&nbsp;&nbsp;
                                                                  </a>
                                                                  <button type="button" href="/saraly/slip" class="btn btn-default"  disabled data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                    <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                  </button>
                                                                </span>
                                                              </li>
                                                            @endif

                                                            <br>

                                                            @if($value[$k][2] != 0)
                                                              <li>
                                                                <a href="/salary/slip/{{ $value[$k][2] }}" class="btn btn-default"> Week 2 </a>
                                                                <span class="text-green">
                                                                  <i class="ion ion-android-checkmark-circle text-green" style="font-size:18px"></i>
                                                                  กรอกข้อมูลเรียบร้อย
                                                                </span>
                                                                <span class="pull-right">
                                                                  <a class="btn btn-warning" href="/salary/{{$value[$k][2]}}/edit">
                                                                    <i class="ion ion-android-create"></i> แก้ไขข้อมูล
                                                                  </a>
  
                                                                  <a target="_blank" href="/salary/slip/{{ $value[$k][2] }}" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                    <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                  </a>
                                                                </span>
                                                              </li>
                                                            @else
                                                              <li>
                                                                <button class="btn btn-default" disabled> Week 2 </button>
                                                                <span class="text-red">
                                                                  <i class="ion ion-android-cancel text-red" style="font-size:18px"></i>
                                                                  กรอกข้อมูลให้ครบถ้วน
                                                                </span>
                                                                <span class="pull-right">
                                                                  <a class="btn btn-success" href="/salary/create?id={{ $rs->id }}&week=2&date={{ ($currentyear - 543)."-".sprintf('%02d', $k)."-16" }}">
                                                                    <i class="ion ion-android-add-circle"></i> เพิ่มข้อมูล&nbsp;&nbsp;
                                                                  </a>
                                                                  <button type="button" class="btn btn-default"  disabled data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                    <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                  </button>
                                                                </span>
                                                              </li>
                                                            @endif
                                                        @endif
                                                      @endforeach
                                                    </ul>
                                                    <hr>
                                                  @endforeach
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                        @endif

                                      @else
                                        <div class="panel box box-primary">
                                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$k}}" id="m_{{$k}}">
                                            <div class="box-header with-border">
                                              <h3 class="box-title font">
                                                <span style="color:#0073b7"> เดือน {{ $m }} </span>
                                              </h3>
                                            </div>
                                          </a>
                                          <div id="collapse_{{$k}}" class="panel-collapse collapse">
                                            <div class="box-body">
                                              <ul class="list-unstyled" style="margin-left:15px">
                                                @foreach($employee as $rs)
                                                  <li>
                                                    @switch($rs->type)
                                                        @case(1)
                                                        <span class="badge bg-blue" style="margin-right:20px;"> รายเดือน </span>
                                                        @break
                                                        @case(2)
                                                        <span class="badge bg-yellow" style="margin-right:20px;"> &nbsp;&nbsp;&nbsp;รายวัน&nbsp;&nbsp;&nbsp; </span>
                                                        @break
                                                    @endswitch
                                                    {{ $data['prename'][$rs->prename] }} {{ $rs->name }} {{ $rs->surname }} </li>
                                                  <ul>
                                                    @foreach ($week as $key => $value)
                                                      @if($key == $rs->id)
                                                          @if($value[$k][1] != 0)
                                                            <li>
                                                              <a href="/salary/slip/{{ $value[$k][1] }}" class="btn btn-default"> Week 1 </a>
                                                              <span class="text-green">
                                                                <i class="ion ion-android-checkmark-circle text-green" style="font-size:18px"></i>
                                                                กรอกข้อมูลเรียบร้อย
                                                              </span>

                                                              <span class="pull-right">
                                                                <a class="btn btn-warning" href="/salary/{{$value[$k][1]}}/edit">
                                                                  <i class="ion ion-android-create"></i> แก้ไขข้อมูล
                                                                </a>

                                                                <a target="_blank" href="/salary/slip/{{ $value[$k][1] }}" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                  <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                </a>
                                                              </span>
                                                            </li>
                                                          @else
                                                            <li>
                                                              <button class="btn btn-default" disabled> Week 1 </button>
                                                              <span class="text-red">
                                                                <i class="ion ion-android-cancel text-red" style="font-size:18px"></i>
                                                                กรอกข้อมูลให้ครบถ้วน
                                                              </span>
                                                              <span class="pull-right">
                                                                <a class="btn btn-success" href="/salary/create?id={{ $rs->id }}&week=1&date={{ ($currentyear - 543)."-".sprintf('%02d', $k)."-01" }}">
                                                                  <i class="ion ion-android-add-circle"></i> เพิ่มข้อมูล&nbsp;&nbsp;
                                                                </a>
                                                                <button type="button" href="/saraly/slip" class="btn btn-default"  disabled data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                  <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                </button>
                                                              </span>
                                                            </li>
                                                          @endif

                                                          <br>

                                                          @if($value[$k][2] != 0)
                                                            <li>
                                                              <a href="/salary/slip/{{ $value[$k][2] }}" class="btn btn-default"> Week 2 </a>
                                                              <span class="text-green">
                                                                <i class="ion ion-android-checkmark-circle text-green" style="font-size:18px"></i>
                                                                กรอกข้อมูลเรียบร้อย
                                                              </span>
                                                              <span class="pull-right">
                                                                <a class="btn btn-warning" href="/salary/{{$value[$k][2]}}/edit">
                                                                  <i class="ion ion-android-create"></i> แก้ไขข้อมูล
                                                                </a>

                                                                <a target="_blank" href="/salary/slip/{{ $value[$k][2] }}" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                  <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                </a>
                                                              </span>
                                                            </li>
                                                          @else
                                                            <li>
                                                              <button class="btn btn-default" disabled> Week 2 </button>
                                                              <span class="text-red">
                                                                <i class="ion ion-android-cancel text-red" style="font-size:18px"></i>
                                                                กรอกข้อมูลให้ครบถ้วน
                                                              </span>
                                                              <span class="pull-right">
                                                                <a class="btn btn-success" href="/salary/create?id={{ $rs->id }}&week=2&date={{ ($currentyear - 543)."-".sprintf('%02d', $k)."-01" }}">
                                                                  <i class="ion ion-android-add-circle"></i> เพิ่มข้อมูล&nbsp;&nbsp;
                                                                </a>
                                                                <button type="button" class="btn btn-default"  disabled data-toggle="tooltip" data-placement="bottom" title="Print Slip">
                                                                  <i class="ion ion-printer" style="font-size:20px;"></i>
                                                                </button>
                                                              </span>
                                                            </li>
                                                          @endif
                                                      @endif
                                                    @endforeach
                                                  </ul>
                                                  <hr>
                                                @endforeach
                                              </ul>
                                            </div>
                                          </div>
                                        </div>
                                      @endif

                                    @endforeach
                                  </div>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->
                          <!-- END ACCORDION & CAROUSEL-->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@include('salary.salary')

@endsection

@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}

    @if (session('success'))
        <script>
            var id = {{Session::get('success')}};
            $("#m_"+id).trigger('click');
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            var id = {{Session::get('success')}};
            $("#m_"+id).trigger('click');
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  
    $('#salary').on('show.bs.modal', function (event) {
        var button     = $(event.relatedTarget)
        var daybetween = button.data('daybetween')
        var action     = button.data('action')
        var id         = button.data('id')
        var week       = button.data('week')
        var date       = button.data('date')
        var type       = button.data('type')
        var salary     = button.data('salary')
        var s_id       = button.data('s_id')
        $(this).find('.modal-header .daybetween').html(daybetween)
        $(this).find('.modal-body .s_id').val(s_id)
        $(this).find('.modal-body .id').val(id)
        $(this).find('.modal-body .week').val(week)
        $(this).find('.modal-body .date').val(date)
        $(this).find('.modal-body .type').html(type)
        $('#salary').find('.modal-body .salary').val(salary)
        switch (type) {
          case "รายเดือน" :  var insurance = ((salary * 2.5) / 100); break;
          case "รายวัน"   :  var insurance = ((salary * 2.5) / 100); break;
        }
        switch (action) {
          case "add":
                $('#salary').find('.modal-body .overtime').attr("placeholder", "คำนวนจากโปรแกรม");
                $('#salary').find('.modal-body .ar').val("")
                $('#salary').find('.modal-body .commission').val("")
                $('#salary').find('.modal-body .insurance').val(insurance)
                $('#salary').find('.modal-body .other').val("")
                $('#salary').find('.modal-body .amount_other').val("")
            break;
            case "edit":
                $.ajax({
                  type: "GET",
                  url: "/salary/show/"+s_id,
                  success: function(data) {
                    console.log(data);
                    $('#salary').find('.modal-body .salary').val(data.salary)
                    $('#salary').find('.modal-body .overtime').val(data.overtime)
                    $('#salary').find('.modal-body .ar').val(data.ar)
                    $('#salary').find('.modal-body .commission').val(data.commission)
                    $('#salary').find('.modal-body .insurance').val(data.insurance)
                    $('#salary').find('.modal-body .other').val(data.other)
                    $('#salary').find('.modal-body .amount_other').val(data.amount_other)
                  }
                });
            break;
        }
    }); 

    $('.select_year').on('change',function(){
        $( "#years" ).submit();
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(function () {
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
