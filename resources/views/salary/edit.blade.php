@extends('layouts.app')
@section('title', 'แก้ไขใบสลิปเงินเดือน')
@section('styles')

@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'แก้ไขใบสลิปเงินเดือน'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        <h3 class="box-title font"> ข้อมูลใบสลิปเงินเดือน {{ $daybetween }} </h3>
                        <h3 class="box-title font pull-right"> {{ $name }} </h3>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <!-- form start -->
                        <hr>   
                            {!! Form::model($salary , ['url' => ['salary', $salary->id] , 'method' => 'PATCH']  ) !!}
                            <div class="box-body">
                                <div class="row">
                                        <div class="row">
                                            <div class="col-md-6 col-md-offset-1">
                                                <span class="text-green label_font" style="font-size:22px;">  <i class="ion ion-android-star"></i> รายได้ (Income)  </span>
                                            </div>   
                                        </div>   
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-1">
                                              <div class="form-group">
                                                    @switch($employee->type)
                                                    @case(1)
                                                    {!! Form::label('Salary', "เงินเดือน", ['class'=>'label_font type']) !!}
                                                    @break
                                                    @case(2)
                                                    {!! Form::label('Salary', "รายวัน", ['class'=>'label_font type']) !!}
                                                    @break
                                                    @endswitch
                                                  {!! Form::number('salary', NULL, ['class'=>'form-control salary', 'readonly']) !!}
                                              </div>
                                              <div class="form-group">
                                                  {!! Form::label('Overtime', "ค่าล่วงเวลา", ['class'=>'label_font']) !!}
                                                  {!! Form::number('overtime', NULL, ['class'=>'form-control overtime', 'readonly']) !!}
                                              </div>
                                            </div>
                                            <div class="col-md-5">
                                            <div class="form-group">
                                                  {!! Form::label('ar', "ค่า A.R. (เบี้ยขยัน)", ['class'=>'label_font']) !!}
                                                  {!! Form::number('ar', NULL, ['class'=>'form-control ar']) !!}
                                            </div>
                                            <div class="form-group">
                                                  {!! Form::label('commission', "ค่า Commission", ['class'=>'label_font']) !!}
                                                  {!! Form::number('commission', NULL, ['class'=>'form-control commission']) !!}
                                            </div>
                                        </div>
                                        </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-5 col-md-offset-1">
                                            <span class="text-red label_font"> <i class="ion ion-android-star"></i> รายการหัก (Deduction) </span>
                                        </div>   
                                    </div>   
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="form-group">
                                                {!! Form::label('insurance', "ประกันสังคม", ['class'=>'label_font']) !!}
                                                {!! Form::number('insurance', NULL, ['class'=>'form-control insurance', 'readonly']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-md-offset-1">
                                            <div class="form-group">
                                                {!! Form::label('other', "อื่น ๆ", ['class'=>'label_font']) !!}
                                                {!! Form::text('other', NULL, ['class'=>'form-control other', 'placeholder' => 'รายละเอียด']) !!}
                                            </div>
                                            <span class="font text-red" style="font-size:16px"> * กรณีที่มีการหักรายการอื่น ๆ ให้กรอกในช่องอื่น ๆ  </span>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                {!! Form::label('amount_other', "จำนวนเงิน", ['class'=>'label_font']) !!}
                                                {!! Form::number('amount_other', NULL, ['class'=>'form-control amount_other']) !!}
                                            </div>
                                            <span class="font text-red" style="font-size:16px"> ระบุจำนวนเงินรายการอื่นๆที่หัก  </span>
                                        </div>
                                    </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary label_font"> บันทึก </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
            </div><!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
   <script>
       
   </script>
@endpush
