<!-- Modal -->
<div id="salary" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top:5%">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title font" style="font-size:22px"> ข้อมูลเงินเดือน งวดวันที่ <span class="daybetween"></span> </h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['url' => '/salary']) !!}
        {!! Form::hidden('employee_id', NULL, ['class'=>'id']) !!}
        {!! Form::hidden('id', NULL, ['class'=>'s_id']) !!}
        {!! Form::hidden('week', NULL, ['class'=>'week']) !!}
        {!! Form::hidden('date', NULL , ['class'=>'date']) !!}
        <span class="text-green label_font" style="font-size:22px">  <i class="ion ion-android-star"></i> รายได้ (Income)  </span>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('Salary', NULL, ['class'=>'label_font type']) !!}
                {!! Form::number('salary', NULL, ['class'=>'form-control salary', 'readonly']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('Overtime', "ค่าล่วงเวลา", ['class'=>'label_font']) !!}
                {!! Form::number('overtime', NULL, ['class'=>'form-control overtime', 'readonly']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('ar', "ค่า A.R. (เบี้ยขยัน)", ['class'=>'label_font']) !!}
                {!! Form::number('ar', 0, ['class'=>'form-control ar']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('commission', "ค่า Commission", ['class'=>'label_font']) !!}
                {!! Form::number('commission', 0, ['class'=>'form-control commission']) !!}
            </div>
          </div>
        </div>
        <hr>
        <span class="text-red label_font"> <i class="ion ion-android-star"></i> รายการหัก (Deduction) </span>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('insurance', "ประกันสังคม", ['class'=>'label_font']) !!}
                {!! Form::number('insurance', 0, ['class'=>'form-control insurance', 'readonly']) !!}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('other', "อื่น ๆ", ['class'=>'label_font']) !!}
                {!! Form::text('other', 0, ['class'=>'form-control other', 'placeholder' => 'รายละเอียด']) !!}
            </div>
              <span class="font text-red" style="font-size:16px"> * กรณีที่มีการหักรายการอื่น ๆ ให้กรอกในช่องอื่น ๆ  </span>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('amount_other', "จำนวนเงิน", ['class'=>'label_font']) !!}
                {!! Form::number('amount_other', 0, ['class'=>'form-control amount_other']) !!}
            </div>
              <span class="font text-red" style="font-size:16px"> ระบุจำนวนเงินรายการอื่นๆที่หัก  </span>
          </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
