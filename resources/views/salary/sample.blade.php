<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      {{ Html::style('fonts/style.css') }}
      <style media="screen">
        .absolute {
          position: absolute;
          text-align: center;
          vertical-align: middle;
          border:thin solid blue;
        }

        table.separate {
          border-collapse: separate;
          /* border-spacing: 5pt;
          border: 3pt solid #33d; */
          width: 100%;
        }

        table.collapse {
          border-collapse: collapse;
          border: 1px solid black;
        }

        .full-width {
          width: 100%;
        }

        th {
          text-align: center;
          vertical-align: middle;
        }

        td,th {
          vertical-align: middle;
          border: 1px solid black
        }

        .right {
          float:right;
        }
      </style>
  </head>
  <body marginwidth="0" marginheight="0">
    <h1>ใบแจ้งเงินเดือน (PAY SLIP) <span class="right"> {{ $company }} </span> </h1>

    <h3 style="margin-right:75%;text-align: left;  border: 1px solid black"> ประเภทพนักงาน <span class="right" style="font-weight:normal;text-align: left;"> รายเดือน </span> </h3>
    <h3 style="margin-right:75%;text-align: left;  border: 1px solid black"> ชื่อ - นามสกุล <span class="right" style="font-weight:normal"> นาย พารกร จุมปี </span> </h3>

    <table class="separate">
      <thead>
        <tr>
          <th>คำแหน่ง</th>
          <th>head 2</th>
          <th>head 3</th>
          <th>head 4</th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <td rowspan="2">cell 1</td>
        <td>cell 2</td>
        <td colspan="2">cell 3</td>
      </tr>
      <tr>
        <td colspan="2">cell 4</td>
        <td rowspan="2">cell 5</td>
      </tr>
      <tr>
        <td colspan="3">cell 6</td>
      </tr>
      <tr>
        <td colspan="4">cell 7</td>
      </tr>
      </tbody>
    </table>

    <hr>
    <br>
    <hr>

    <table class="collapse full-width">
      <thead>
        <tr>
          <th>head 1</th>
          <th>head 2</th>
          <th>head 3</th>
          <th>head 4</th>
        </tr>
      </thead>
      <tbody>
      <tr>
        <td>cell 1</td>
        <td>cell 2</td>
        <td>cell 3</td>
        <td>cell 4</td>
      </tr>
      <tr>
        <td colspan="2">cell 5</td>
        <td>cell 6</td>
        <td>cell 7</td>
      </tr>
      <tr>
        <td>cell 8</td>
        <td>cell 9</td>
        <td colspan="2">cell 10</td>
      </tr>
      </tbody>
      <tbody><tr>
        <td colspan="4">cell 11</td>
      </tr>
      </tbody>
    </table>

    <hr>
    <br>
    <hr>

    <table align="center" class="collapse full-width">
      <thead>
        <tr>
          <th>เลขที่</th>
          <th>สินค้า</th>
          <th>ราคา</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>ปากกา</td>
          <td>10</td>
        </tr>
        <tr>
          <td>2</td>
          <td>ดินสอ</td>
          <td>5</td>
        </tr>
        <tr>
          <td>3</td>
          <td>สมุด</td>
          <td>15</td>
        </tr>
      </tbody>
    </table>

    <hr>
    <br>
    <hr>

    <div style="margin-right:50%;border:thin solid red;text-align: left;">Left</div>
    <div style="margin-left:50%;border:thin solid red;text-align: right;">Right</div>

    {{-- <div class="absolute" style="top: 350px; left: 0px;">
      top/left
    </div>

    <div class="absolute" style="top: 350px; right: 0px;">
      top/right
    </div>

    <div class="absolute" style="top: 400px; left: 40px; right: 40px;">
      top/left/right
    </div>

    <div class="absolute" style="bottom: 40px; left: 40px;">
        bottom/left
    </div>

    <div class="absolute" style="bottom: 40px; right: 40px;">
      bottom/right
    </div>

    <div class="absolute" style="left: 160px; right: 160px; bottom: 0px ">
      top/left/right/bottom
    </div> --}}

    {{-- <div class="absolute" style="bottom: 0px;font-size: 18px">
        <p>(ลงชื่อ)......................................................พนักงาน </p>
        ( นาย ทินกร จุมปี )
    </div> --}}

    {{-- <table align="center" class="collapse full-width">
      <thead>
        <tr>
          <th>เลขที่</th>
          <th>สินค้า</th>
          <th>ราคา</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>ปากกา</td>
          <td>10</td>
        </tr>
        <tr>
          <td>2</td>
          <td>ดินสอ</td>
          <td>5</td>
        </tr>
        <tr>
          <td>3</td>
          <td>สมุด</td>
          <td>15</td>
        </tr>
      </tbody>
    </table>

    <hr>
    <br>
    <hr> --}}

    {{-- <div style="margin-right:50%;border:thin solid red;text-align: left;">Left</div>
    <div style="margin-left:50%;border:thin solid red;text-align: right;">Right</div> --}}

    {{-- <div class="absolute" style="top: 350px; left: 0px;">
      top/left
    </div>

    <div class="absolute" style="top: 350px; right: 0px;">
      top/right
    </div>

    <div class="absolute" style="top: 400px; left: 40px; right: 40px;">
      top/left/right
    </div>

    <div class="absolute" style="bottom: 40px; left: 40px;">
        bottom/left
    </div>

    <div class="absolute" style="bottom: 40px; right: 40px;">
      bottom/right
    </div>

    <div class="absolute" style="left: 160px; right: 160px; bottom: 0px ">
      top/left/right/bottom
    </div> --}

  </body>
</html>


<style> .page-break { page-break-after: always } </style>
