@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <title> {{ $title }} </title>
      {{ Html::style('fonts/style.css') }}
  </head>
  <body marginwidth="0" marginheight="0">

    <h1>ใบแจ้งเงินเดือน (PAY SLIP) <span class="f_right"> {{ $company }} </span> </h1>

    <table class="separate">
      <tbody>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ประเภทพนักงาน </span> </td>
          <td> <span style="font-size:20px;"> {{ ($salary->employee->type == 1 ) ? "รายเดือน" : "รายวัน" }} </span> </td>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ตำแหน่ง </span> </td>
          <td> <span style="font-size:20px;"> {{ $salary->employee->position }} </span> </td>
        </tr>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ชื่อ - นามสกุล </span> </td>
          <td> <span style="font-size:20px;"> {{ $data['prename'][$salary->employee->prename] }} {{ $salary->employee->name }} {{ $salary->employee->surname }} </span> </td>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ประจำวันที่ </span> </td>

          <td>
            <span style="font-size:20px;">
              @if(Carbon::parse($salary->date)->day < 15)
                {{ DateThaiLibrary::ThaiDate($salary->date)  ." - ".  DateThaiLibrary::ThaiDate(Carbon::parse($salary->date)->day(15)) }}
              @else
                {{ DateThaiLibrary::ThaiDate($salary->date)  ." - ".  DateThaiLibrary::ThaiDate(Carbon::parse($salary->date)->day(Carbon::parse($salary->date)->daysInMonth)) }}
              @endif
            </span>
          </td>

        </tr>
      </tbody>
    </table>

    <hr>
    <hr>

    <table class="separate">
      <tbody>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> จำนวนวันทำงาน </span> </td>
          <td> <span style="font-size:20px;"> {{ $come != 0 ? $come : "-"  }}  </span> </td>.
          <td> <span style="font-size:20px;"> วัน  </span> </td>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ขาด </span> </td>
          <td> <span style="font-size:20px;"> {{ $absence != 0 ? $absence : "-" }}  </span> </td>
          <td> <span style="font-size:20px;"> วัน  </span> </td>
        </tr>
        <tr>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> ลา </span> </td>
          <td> <span style="font-size:20px;"> {{ $leave != 0 ? $leave : "-" }}  </span> </td>
          <td> <span style="font-size:20px;"> วัน </span> </td>
          <td width="15%"> <span style="font-weight:bold;font-size:20px"> มาสาย </span> </td>
          <td> <span style="font-size:20px;"> {{ $late != 0 ? $late : "-" }}  </span> </td>
          <td> <span style="font-size:20px;"> วัน  </span> </td>
        </tr>
      </tbody>
    </table>

    <hr>
    <hr>
    <br>

    <table class="collapse full-width">
      <thead>
        <tr>
          <th colspan="2" width="35%"> รายได้ (Income) </th>
          <th colspan="2" width="35%"> รายการหัก (Deduction) </th>
          <th colspan="2" width="30%"> รวมเงิน (Net Income) </th>
        </tr>
      </thead>
      <tbody>
      <tr>

        <td class="left">
          <span class="m_left"> เงินเดือนต่องวด </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($salary->salary,2,'.',',') }} </span>
        </td>

        <td class="left border">
          <span class="m_left"> ประกันสังคม </span>
        </td>
        <td class="right">
            {{-- {{ number_format($salary->insurance,2,'.',',') }} --}}
          <span class="m_right">{{ number_format(($salary->salary * 5) / 100, 2 ,'.',',') }}  </span>
        </td>

        <td class="left border">
          <span class="m_left bold"> รวมเงินได้ </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($sum,2,'.',',') }} </span>
        </td>

      </tr>

      <tr>
        <td class="left">
          <span class="m_left"> ค่าล่วงเวลา ({{ $ot_time }} ชม.)</span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($ot_amount,2,'.',',') }} </span>
        </td>

        <td class="left border">
          <span class="m_left"> อื่น ๆ {{ "( ".$salary->other." )" }} </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($salary->amount_other,2,'.',',') }} </span>
        </td>

        <td class="left border">
          <span class="m_left bold"> รวมเงินหัก </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($deduction,2,'.',',') }} </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <span class="m_left"> ค่า A.R. </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($salary->ar,2,'.',',') }} </span>
        </td>

        <td class="left border">
          <span class="m_left">  </span>
        </td>
        <td class="right">
          <span class="m_right">  </span>
        </td>

        <td class="left border">
          <span class="m_left bold">  เงินได้สุทธิ </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($total,2,'.',',') }} </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <span class="m_left"> ค่า Commission </span>
        </td>
        <td class="right">
          <span class="m_right"> {{ number_format($salary->commission,2,'.',',') }} </span>
        </td>

        <td class="left border">
          <span class="m_left">  </span>
        </td>
        <td class="right">
          <span class="m_right">  </span>
        </td>

        <td class="left border">
          <span class="m_left">  </span>
        </td>
        <td class="right">
          <span class="m_right">  </span>
        </td>
      </tr>
      </tbody>
    </table>

    <br>
    <hr>
    <hr>

    <p class="f_right bold" style="font-size: 20px"> รวมเงินได้สุทธิ {{ number_format($total, 2,'.', ',') }} บาท ({{ $thai }}) </p>

    <br>
    <br>
    <br>
    <hr>
    <hr>

    <table class="full-width" style="margin-top:185px">
      <tbody>
      <tr>
        <td></td>
        <td></td>
        <td class="center" width="20%">
          <span style="font-size:18px"> (ลงชื่อ)...................................................พนักงาน </span>
        </td>
      </tr>

      <tr>
        <td></td>
        <td></td>
        <td class="center"  width="20%">
          <span style="font-size:18px">   ( {{ $data['prename'][$salary->employee->prename] }} {{ $salary->employee->name }} {{ $salary->employee->surname }} )  </span>
        </td>
      </tr>

      </tbody>
    </table>

  </body>
</html>
