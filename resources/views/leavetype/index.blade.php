@extends('layouts.app') 
@section('title', 'ประเภทการลา') 
@section('styles')

@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        @include('layouts.Backend.breadcrumb', ['title'=>'ประเภทการลา'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title font" style="margin-top:19px;" >ตารางประเภทการลา </h2>
                            <button class="btn btn-lg btn-add btn btn-info btn_employee font" style="font-size:22px"> 
                                <i class="ion ion-android-add-circle" style="margin-right:10px;"></i> เพิ่มประเภทการลา
                            </button>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover font" style="font-size:19px">
                                <thead>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="15%">ลำดับ</th>
                                        <th width="45%">ปรเภท</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:20px">
                                @foreach($position as $k => $rs)
                                    <tr>
                                        <td class="text-center">{{ ++$k }}</td>
                                        <td class="text-center">
                                        @if($min == $rs->type_no)
                                            <button class="down btn-link" data-no="{{ $rs->type_no }}" data-toggle="tooltip" data-placement="bottom" title="เลื่อนประเภทการลาลง"><i class="ion ion-arrow-down-c"></i></button>
                                        @elseif($min < $rs->type_no && $rs->type_no < $max)
                                            <button class="down btn-link" data-no="{{ $rs->type_no }}" data-toggle="tooltip" data-placement="bottom" title="เลื่อนประเภทการลาลง"><i class="ion ion-arrow-down-c"></i></button>
                                            <button class="up btn-link" data-no="{{ $rs->type_no }}" data-toggle="tooltip" title="เลื่อนประเภทการลาขึ้น"><i class="ion ion-arrow-up-c"></i></button>
                                        @else($max == $rs->type_no)
                                            <button class="up btn-link" data-no="{{ $rs->type_no }}" data-toggle="tooltip" title="เลื่อนประเภทการลาขึ้น"><i class="ion ion-arrow-up-c"></i></button>
                                        @endif
                                        </td>
                                        <td>{{ $rs->type_name }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-warning btn-edit" data-id="{{ $rs->id }}" data-no="{{ $rs->type_no }}" data-type="{{ $rs->type_name }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></button>  
                                            <button class="btn btn-danger  btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button> 
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="15%">ลำดับ</th>
                                        <th width="45%">ปรเภท</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <p id="message"></p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugins/datatables/jquery.dataTables.js') }} 
    {{ Html::script('plugins/datatables/dataTables.bootstrap.min.js')}}
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.down').on('click',function(){
        var no = $(this).data('no');
        $.ajax({
            type: "PUT",
            url: "/leavetype/up",
            data: {no : no},
            success: function(){
                swal("Success!", "ทำการเลื่อนประเภทการลาเรียบร้อยแล้ว", "success",{button:false}); 
                setTimeout(function() {
                    location.reload(); 
                }, 1200); 
			}
		});
    });

    $('.up').on('click',function(){
        var no = $(this).data('no');
        $.ajax({
            type: "PUT",
            url: "/leavetype/down",
            data: {no : no},
            success: function(){
                swal("Success!", "ทำการเลื่อนประเภทการลาเรียบร้อยแล้ว", "success",{button:false}); 
                setTimeout(function() {
                    location.reload(); 
                }, 1200); 
			}
		});
    });

    $('.btn-add').on('click',function(){
        swal(" เพิ่มประเภทการลา ", {
            content: "input",
            buttons: {
                cancel: true,
                confirm: true,
            }
        })
        .then((value) => {
            if(value.length > 1) {
                $.ajax({
                    type: "POST",
                    url: "/leavetype",
                    data: { type_name : value },
                    success: function(msg){
                        swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success",{button:false}); 
                        setTimeout(function() {
                            location.reload(); 
                        }, 1000); 
				    }
			    });
            } else {
                return false;
            }
        });
    });

    $('.btn-edit').on('click',function(){
        var id = $(this).data('id');
        var no = $(this).data('no');
        var type = $(this).data('type');
        swal(" แก้ไขประเภทการลา ", {
        content: {
            element: "input",
            attributes: {
                value : type
            },
        },
        })
         .then((value) => {
            if(value.length > 1) {
                $.ajax({
                    type: "PUT",
                    url: "/leavetype",
                    data: { type_name : value, id : id, no : no},
                    success: function(msg){
                        swal("Success!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success",{button:false}); 
                        setTimeout(function() {
                            location.reload(); 
                        }, 1000); 
				    }
			    });
            } else {
                return false;
            }
        }); 
    });

    $('.btn-del').on('click',function(){
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ ประเภทการลา นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) { 
                $.ajax({
                    type: "DELETE",
                    url: "/leavetype",
                    data: {id : id},
                    success: function(msg){
                        swal("Success!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success",{button:false});                       
                        setTimeout(function() {
                            location.reload(); 
                        }, 1000); 
				    }
			    });
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip(); 

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush